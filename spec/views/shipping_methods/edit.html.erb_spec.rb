require 'spec_helper'

describe "shipping_methods/edit" do
  before(:each) do
    @shipping_method = assign(:shipping_method, stub_model(ShippingMethod))
  end

  it "renders the edit shipping_method form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", shipping_method_path(@shipping_method), "post" do
    end
  end
end
