require 'spec_helper'

describe "shipping_methods/new" do
  before(:each) do
    assign(:shipping_method, stub_model(ShippingMethod).as_new_record)
  end

  it "renders new shipping_method form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", shipping_methods_path, "post" do
    end
  end
end
