require 'spec_helper'

describe "orders/show" do
  before(:each) do
    @order = assign(:order, stub_model(Order,
      :payment_type => "Payment Type",
      :shipping_method => "",
      :is_confirmed => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Payment Type/)
    rendered.should match(//)
    rendered.should match(/false/)
  end
end
