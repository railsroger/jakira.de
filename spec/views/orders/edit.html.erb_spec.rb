require 'spec_helper'

describe "orders/edit" do
  before(:each) do
    @order = assign(:order, stub_model(Order,
      :payment_type => "MyString",
      :shipping_method => "",
      :is_confirmed => false
    ))
  end

  it "renders the edit order form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", order_path(@order), "post" do
      assert_select "input#order_payment_type[name=?]", "order[payment_type]"
      assert_select "input#order_shipping_method[name=?]", "order[shipping_method]"
      assert_select "input#order_is_confirmed[name=?]", "order[is_confirmed]"
    end
  end
end
