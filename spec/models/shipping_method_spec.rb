require 'spec_helper'

describe ShippingMethod do

  describe 'Other' do
    let :shipping_method do
      create :shipping_method
    end

    it 'should be creatable' do
      expect(shipping_method).to be_valid
    end

    it 'name must not be out of range' do
      shipping_method.name = Faker::Lorem.characters(rand(10))
      expect(shipping_method).not_to be_valid
    end

    it 'name can be the same in different shops' do
      second_shipping_method = build(:shipping_method)
      second_shipping_method.name = shipping_method.name
      expect(second_shipping_method).to be_valid
    end
 
    it 'name must not be the same in the same shop' do
      second_shipping_method = build(:shipping_method)
      shipping_method.shipping_methodable.shipping_methods << second_shipping_method
      second_shipping_method.name = shipping_method.name
      expect(second_shipping_method).not_to be_valid
    end

  end

  describe 'Pickup' do
    it 'must be creatable' do
      pickup_shipping = create(:pickup_shipping)
      expect(pickup_shipping).to be_valid
    end
  end

  describe 'Free within Germany' do
    it 'must be creatable' do
      free_within_germany_shipping = create(:free_within_germany_shipping)
      expect(free_within_germany_shipping).to be_valid
    end
  end

end
