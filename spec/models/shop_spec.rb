require 'spec_helper'

describe Shop do

  let :shop do
    create :shop
  end

  it 'must be valid' do
    expect(shop).to be_valid
  end

  it 'must belong to an user' do
    expect(shop.user).to be_truthy
  end

  it 'must belong to an address' do
    expect(shop.address).to be_truthy
  end

  it 'is invalid if name attribute is not set' do
    expect(build(:shop, name: '')).to be_invalid
  end

  it 'must have subdomain' do
    expect(shop.subdomain).to be_truthy
  end

  it 'should be updatable' do 
    existing_shop = Shop.find(shop.id)
    existing_shop.imprint = Faker::Lorem.paragraph(40)
    expect(existing_shop.save).to be_truthy
  end

  context 'when payment_authorization' do
    it 'is not present should be invalid' do
      expect(build(:shop, payment_authorization: nil)).to be_invalid
      expect(build(:shop, payment_authorization: false)).to be_invalid
      expect(build(:shop, payment_authorization: '0')).to be_invalid
    end

    it 'is not present while updating with personal bank data and package' do
      existing_shop = Shop.find(shop.id)
      existing_shop.package = (['smart', 'basic', 'professional'] - [shop.package]).sample
      expect(existing_shop.save).to be_falsey
      existing_shop.personal_bank_data.account_holder = Faker::Name.first_name
      expect(existing_shop.save).to be_falsey
    end

  end

  it 'must be valid with terms' do
    shop.terms_and_revocation_are_chosen = '1'
    expect(shop).not_to be_valid
    shop.terms = Faker::Lorem.paragraph(40)
    expect(shop).to be_valid
  end

  it 'must be valid with payment types' do
    shop.set_payment_types = '1'
    expect(shop).not_to be_valid
    shop.payment_types = PaymentType.all.sample(1 + rand(PaymentType.all.count))
    expect(shop).to be_valid
  end

  # it 'must be valid with shipping methods ' do
  #   shop.set_shipping = '1'
  #   expect(shop).to be_valid
  #   # shop.shipping_methods
  #   # expect(shop).to be_valid

  # end

end
