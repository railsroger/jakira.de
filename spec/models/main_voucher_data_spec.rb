require 'spec_helper'

describe MainVoucherData do

  before do
    @main_voucher_data = MainVoucherData.first
  end

  it 'should be one' do
    expect(MainVoucherData.count).to eq 1
  end

  it 'should be present' do
    expect(@main_voucher_data).to be_truthy
  end

  it 'should not have payment_types' do
    expect(@main_voucher_data.have_payment_types?).to be_falsey
  end

  context 'when bank_transfer then' do 

    it 'should not be valid' do
      @main_voucher_data.update(bank_transfer: true)
      expect(@main_voucher_data).not_to be_valid
    end

    it 'should be half valid and not valid at all' do
      @main_voucher_data.update(bank_transfer: true,
        bank_transfer_data_attributes: {account_holder: 'badgs',
                                                  iban: 'fsdg'})
      expect(@main_voucher_data).not_to be_valid
    end
    it 'should be valid' do
      @main_voucher_data.update(bank_transfer: true,
        bank_transfer_data_attributes: {account_holder: 'badgs',
                                                  iban: 'fsdg',
                                                  swft_bic: 'fsdfs'})
      expect(@main_voucher_data).to be_valid
    end

    it 'should have payment_types' do
      @main_voucher_data.update(bank_transfer: true,
        bank_transfer_data_attributes: {account_holder: 'badgs',
                                                  iban: 'fsdg',
                                                  swft_bic: 'fsdfs'})
      expect(@main_voucher_data.reload.have_payment_types?).to be_truthy
    end
  end

  context 'when paypal then' do 

    it 'should not be valid' do
      @main_voucher_data.update(paypal: true)
      expect(@main_voucher_data).not_to be_valid
    end

    it 'should be half valid and not valid at all' do
      @main_voucher_data.update(paypal: true,
        paypal_data_attributes: {
          client_id: '98798'})
      expect(@main_voucher_data).not_to be_valid
    end

    it 'should be valid' do
      @main_voucher_data.update(paypal: true,
        paypal_data_attributes: {
          client_id: '98798',
          client_secret: '7987'})
      expect(@main_voucher_data).to be_valid
    end

    it 'should have payment_types' do
      @main_voucher_data.update(paypal: true,
        paypal_data_attributes: {
          client_id: '98798',
          client_secret: '898090'})
      expect(@main_voucher_data.reload.have_payment_types?).to be_truthy
    end

  end 

  it 'should have paydirekt that is not valid' do
  #   @main_voucher_data.update(paydirekt: true)
  #   expect(@main_voucher_data).not_to be_valid
  end

end
