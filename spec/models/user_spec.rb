require 'spec_helper'

describe User do

  describe "Guest" do
    let :guest do
      build :guest_user
    end

    it 'should be creatable' do
      expect(guest.save).to be_truthy
    end

    it 'should have role "guest"' do
      expect(guest.role).to eq 'guest'
    end
  end

  describe "Regular" do
    let :normal_user do
      create :normal_user
    end
    it 'should not have type' do
      expect(normal_user.type).to eq nil
    end
    it 'should have role "regular"' do
      expect(normal_user.role).to eq "regular"
    end
    it 'should not be registrable without an address' do
      normal_user.address.first_name = ''
      normal_user.address.last_name = ''
      expect(normal_user).not_to be_valid
    end
    it 'should not be valid without an address ' do
      normal_user.address = nil
      expect(normal_user).not_to be_valid
    end
    it 'should have a valid full_name' do
      expect(normal_user.full_name).to be_a(String)
      expect(normal_user.full_name).to be_truthy
      expect(normal_user.full_name).to eq(normal_user.address.full_name)
    end
    it 'should have a membership number' do
      expect(normal_user.membership_number).to be_truthy
    end

    it 'second user should not be valid with the same nickname' do
      normal_user
      second_user = build(:normal_user, nickname: normal_user.nickname)
      expect(second_user).not_to be_valid
    end

    it 'second user must have a correct membership number' do
      normal_user
      second_user = create(:normal_user)
      expect(second_user.membership_number).to eq(1000000002)
    end

    it 'should not have an email in nickname field' do
      normal_user.nickname = Faker::Internet.email
      expect(normal_user).not_to be_valid
    end

    it "should be valid if some address's fields are empty" do
      normal_user.address.house_number = ''
      normal_user.address.phone = ''
      normal_user.address.street = ''
      normal_user.address.place = ''
      normal_user.address.company_name = ''
      expect(normal_user).to be_valid
    end

    it 'can belong to an art association' do
      expect(normal_user.art_association).to be_falsey
      art_association = create :art_association
      normal_user.art_association_id = art_association.id
      normal_user.art_association_membership_number = rand(10)
      normal_user.save
      expect(normal_user.art_association).to be_truthy
    end

    it 'must have an art association membership number' do
      art_association = create :art_association
      normal_user.art_association_id = art_association.id
      normal_user.art_association_membership_number = rand(10)
      normal_user.save
      expect(normal_user.art_association_membership_number).to be_truthy
    end

  end

  describe "Admin" do
    it 'must be created' do
      admin = User.where(role: User.roles[:admin]).first
      expect(admin).to be_truthy
    end
    it 'must be one' do
      admins = User.where(role: User.roles[:admin])
      expect(admins.count).to eq 1
    end
  end

end