require 'spec_helper'

describe PaymentType do
  it 'must not be empty' do
    expect(PaymentType.all).not_to be_empty
  end
end
