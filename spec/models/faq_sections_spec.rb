require 'spec_helper'

describe FaqSection do
  let :faq_section do
    build :faq_section
  end

  it 'must be valid' do
    expect(faq_section).to be_valid
  end
  context 'must not be valid' do 
    it 'if name is blank' do 
      faq_section.name = ''
      expect(faq_section).not_to be_valid
    end
    it "if name's length is more than 255 characters" do
      faq_section.name = Faker::Lorem.characters(256)
      expect(faq_section).not_to be_valid
    end
  end
end
