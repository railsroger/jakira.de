require 'spec_helper'

describe Address do

  let :address do
    build :full_address
  end

  it 'is invalid if the field is not filled' do
    address.street = ''
    expect(address).to be_valid

    address.house_number = ''
    expect(address).to be_valid

    address.country = ''
    expect(address).to be_valid

    address.place = ''
    expect(address).to be_valid

    address.postal_code = ''
    expect(address).to be_valid
  end

  it 'must be valid' do
    expect(address).to be_valid
  end

  it 'full name must be valid' do
    expect(address.full_name).to be_a(String)
    expect(address.full_name).to be_truthy
    expect(address.full_name).to eq(address.first_name + " " + address.last_name)
  end

end
