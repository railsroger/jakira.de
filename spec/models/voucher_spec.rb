require 'spec_helper'

describe "Voucher" do

  let :voucher do
    build :voucher
  end

  before do
    MainVoucherData.first.update(bank_transfer: true,
      bank_transfer_data_attributes: {account_holder: 'badgs',
                                                iban: 'gsdgsg',
                                            swft_bic: 'fsdfs'},
      paypal: true, paypal_data_attributes: { client_id: '98798',
                                          client_secret: 'kjhkj'},
      paydirekt: true)
    voucher.address = build :voucher_bank_transfer_address_blank
  end

  it 'must be valid' do
    expect(voucher).to be_valid
  end

  it 'must have an address' do
    expect(voucher.address).to be_truthy
  end

  context 'must not be valid if' do

    after 'must not be valid if' do
      voucher.save
      expect(voucher).not_to be_valid
    end

    it "value is nil" do
      voucher.value = nil
    end

    it "value is not in array" do
      voucher.value = 68
    end

    it "from field is nil " do
      voucher.from = nil
    end

    it "for field is nil " do
      voucher.for = nil
    end

    it 'email is not valid' do
      voucher.email = 'not_email'
    end
    
    it 'confirmation email is not the same' do
      voucher.email = Faker::Internet.email
    end

    it 'agreement is not checked' do
      voucher.agreement = '0'
    end

  end

  it 'must have a valid number' do
    voucher.save
    expect(voucher.number).to eq "JKG0000001"
    second_voucher = create :voucher
    expect(second_voucher.number).to eq "JKG0000002"
  end


  context 'when payment type is bank transfer' do

    before do
      voucher.payment_type = "bank_transfer"
      voucher.save
    end

    it "and address is not valid" do
      expect(voucher).not_to be_valid
    end

    it "and address is valid" do
      voucher.address = build :voucher_bank_transfer_address
      expect(voucher).to be_valid
    end

  end

  context 'before the payment will be confirmed' do
    before do
      voucher.save
    end
    after do
      expect(@result).to be_falsey
    end
    it 'should not have a voucher code' do
      @result = voucher.code
    end
    it 'should not have paid on date' do
      @result = voucher.paid_on
    end
    it "status must be nil" do 
      @result = voucher.status
    end
    it "should not be paid" do 
      @result = voucher.paid
    end
    it 'should not have sent on date' do
      @result = voucher.sent_on
    end
  end

  context 'after the payment was confirmed' do
    before do
      voucher.save
      voucher.confirm_payment
    end

    it "should be paid" do 
      expect(voucher.paid).to be_truthy
    end

    it 'should have paid on date' do
      expect(voucher.paid_on).to be_truthy
    end

    it 'should have the same paid on date after second payment confirmation' do
      first_paid_on = voucher.paid_on
      voucher.confirm_payment
      expect(voucher.paid_on).to eq first_paid_on
    end

    it 'should have a code' do
      expect(voucher.code).to be_truthy
    end

    it 'should have the same code after second payment confirmation' do
      first_coupon_code = voucher.code
      voucher.confirm_payment
      expect(voucher.code).to eq first_coupon_code
    end

    it "status must be sent" do 
      expect(voucher.status).to eq 'sent'
    end

    it 'should have sent on date' do
      expect(voucher.sent_on).to be_truthy
    end

    it 'should have one sent on date' do
      expect(voucher.sent_on.count).to eq 1
    end

    it 'should have two sent on dates after second payment confirmation' do
      voucher.confirm_payment
      expect(voucher.sent_on.count).to eq 2
    end

  end

end
