# require 'spec_helper'

# describe Article do
#   it 'has a SEO slug' do
#     article = build(:stable_article)
#     article.title = 'Abcd Defg'
#     article.save
#     expect(article.slug).to be == 'abcd-defg'
#   end

#   it 'is visible if finished' do
#     article = create(:stable_article)
#     Article.visible.first.should == article
#   end

#   it 'is invisible if cancelled' do
#     article = create(:stable_article)
#     article.state = nil
#     article.save
#     Article.visible.count.should == 0
#   end

#   it 'only appears in latest articles if new' do
#     20.times do
#       create(:stable_article)
#     end
#     article = create(:stable_article)
#     Article.latest_articles.first.should == article
#     Article.latest_articles.count.should be == 10
#     10.times do
#       create(:stable_article)
#     end
#     Article.latest_articles.count.should be == 10
#     Article.latest_articles.include?(article).should be_false
#   end

#   it 'allows only a limited length for title and description' do
#     article = build(:stable_article)
#     article.current_step = :general
#     article.title = 'a'*1000
#     article.should_not be_valid
#     article.title = 'a'
#     article.should_not be_valid
#     article.title = 'a'*100
#     article.should be_valid
#     article.description = 'a'*10000
#     article.should_not be_valid
#     article.description = 'a'
#     article.should_not be_valid
#     article.description = 'a'*100
#     article.should be_valid
#   end

#   it 'is invalid without an owner' do
#     article = build(:stable_article)
#     article.user = nil
#     article.should_not be_valid
#   end

#   it 'is invalid without a shipping method' do
#     article = create(:stable_article)
#     article.current_step = :details
#     article.should be_valid
#     article.shipping_methods.destroy_all
#     article.should_not be_valid
#   end

#   it 'is invalid without a payment method' do
#     article = create(:stable_article)
#     article.payment_selection = nil
#     article.should_not be_valid
#   end

#   it 'is sold if it belongs to an accepted order' do
#     article  = create(:stable_article)
#     article.sold?.should be_false

#     # Create article_order
#     article_order             = build(:stable_articles_order)
#     article_order.article     = article
#     article_order.is_approved = 'approved'
#     article_order.save

#     article.sold?.should be_true
#   end

#   it 'can only be edited if no accepted or pending orders' do
#     article = create(:stable_article)
#     article.title = "new title"
#     article.description = "abc abc abc abc abc abc abc "
#     article.save
#     article.should have(0).errors

#     # Create article_order
#     article_order             = build(:stable_articles_order)
#     article_order.article     = article
#     article_order.is_approved = 'approved'
#     article_order.save!

#     article.title = "new title 2"
#     article.save
#     article.should have_at_least(1).error

#     purchase = build(:purchase)
#     purchase.state = 'finished'
#     purchase.user = create(:user)
#     purchase.save!

#     order = create(:order)
#     order.purchase = purchase
#     order.save!

#     article_order.is_approved = ''
#     article_order.order = order
#     article_order.save!

#     article.title = "new title 3"
#     article.save
#     article.should have_at_least(1).error

#   end

#   it 'can be build step-by-step' do
#     # Step 1
#     user = create(:user)
#     category = create(:category)
#     article = Article.new
#     article.current_step = :general
#     article.should_not be_valid
#     article.user = user
#     article.title = 'abcd'
#     article.description = 'asdfadsfdasfsf'
#     article.category = category
#     article.price = Money.new(12412, 'USD')
#     article.should be_valid

#     # Step 2
#     article.current_step = :details
#     article.should_not be_valid
#     article.shipping_methods << create(:shipping_method)
#     article.payment_selection = create(:payment_selection)
#     article.should be_valid

#     # Step 3
#     article.current_step = :review
#     article.should_not be_valid
#     article.state = 'finished'
#     article.should be_valid
#   end

#   it 'is not visible if sold' do
#     article = create(:stable_article)

#     article.sold?.should be_false
#     Article.visible.count.should be == 1

#     # Create article_order
#     article_order             = build(:article_order_without_article)
#     article_order.article     = article
#     article_order.is_approved = 'approved'
#     article_order.save

#     article.sold?.should be_true
#     Article.visible.count.should be == 0
#   end


# end
