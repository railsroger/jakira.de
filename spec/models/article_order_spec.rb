# require 'spec_helper'

# describe ArticlesOrder do
#   it 'is invalid if article or order is unset' do
#     articles_order = build(:stable_articles_order)
#     articles_order.article = nil
#     articles_order.should_not be_valid

#     articles_order = build(:stable_articles_order)
#     articles_order.order = nil
#     articles_order.should_not be_valid

#     articles_order = build(:stable_articles_order)
#     articles_order.shipping_method = nil
#     articles_order.should_not be_valid

#     articles_order = build(:stable_articles_order)
#     articles_order.should be_valid
#   end

#   it 'can store a note' do
#     articles_order = build(:stable_articles_order)
#     articles_order.note = 'abcdefg'
#     articles_order.should be_valid
#   end

#   it 'can be accepted or declined before payment' do
#     articles_order = build(:stable_articles_order)
#     articles_order.order.is_paid = false
#     articles_order.save
#     articles_order.is_approved = 'approved'
#     articles_order.should be_valid
#     articles_order.is_approved = 'declined'
#     articles_order.should be_valid
#   end

#   it 'cannot be declined after payment mark' do
#     articles_order = build(:stable_articles_order)
#     articles_order.order.is_paid = true
#     articles_order.save
#     articles_order.is_approved = 'approved'
#     articles_order.should_not be_valid
#     articles_order.is_approved = 'declined'
#     articles_order.should_not be_valid
#   end

#   it 'cannot be edited after acceptance' do
#     order = create(:order)

#     purchase = create(:purchase)
#     purchase.orders << order

#     articles_order = build(:stable_articles_order)
#     articles_order.order = order
#     articles_order.payment_type = 'paypal'
#     articles_order.save!

#     articles_order.should be_valid

#     articles_order.is_approved = 'approved'
#     articles_order.save!

#     articles_order.payment_type = 'advance'
#     articles_order.should_not be_valid

#   end

# end
