require 'spec_helper'

describe ArtAssociation do
  let :art_association do
    build :art_association
  end

  it 'should be valid' do
    expect(art_association).to be_valid
  end

  it 'should have role "art_association"' do
    art_association.save
    expect(art_association.role).to eq "art_association"
  end

  it 'should have type "ArtAssociation"' do
    expect(art_association.type).to eq "ArtAssociation"
  end

  it 'should have a valid membership number' do
    art_association.save
    expect(art_association.membership_number).to eq 1
    expect(art_association.membership_number_with_formatting).to eq "VM00001"
    second_art_association = create :art_association
    expect(second_art_association.membership_number).to eq 2
    expect(second_art_association.membership_number_with_formatting).to eq "VM00002"
  end

  it 'should not have members' do
    expect(art_association.members.count).to eq 0
  end

  it 'should have a member' do
    art_association.members << create(:normal_user,
             art_association_id: art_association.id,
             art_association_membership_number: 1)
    art_association.save
    expect(art_association.members.count).to eq 1
  end

  it 'should have two members' do
    art_association.members << create(:normal_user,
             art_association_id: art_association.id,
             art_association_membership_number: 1)
    art_association.members << create(:normal_user,
             art_association_id: art_association.id,
             art_association_membership_number: 2)
    art_association.save
    expect(art_association.members.count).to eq 2
    expect(art_association.members.last.art_association_membership_number).to eq 2
  end

  describe 'should not be valid if 'do
    after do
      expect(art_association).not_to be_valid
    end
    it 'address is nil' do
      art_association.address = nil
    end
    it 'email is empty' do
      art_association.email = ''
    end
    it "address's feild is empty" do
      art_association.address.street = ''
    end
  end


end