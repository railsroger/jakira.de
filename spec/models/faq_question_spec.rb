require 'spec_helper'

describe FaqQuestion do
  let :faq_question do
    build :faq_question
  end

  it 'must be valid' do
    expect(faq_question).to be_valid
  end

  it 'must have faq section' do
    expect(faq_question.faq_section).to be_truthy
  end

  it 'can have an answer' do
    faq_question.answer = '111'
    expect(faq_question.answer).to eq '111'
  end  

  context 'must not be valid' do 
    it 'if question is blank' do 
      faq_question.question = ''
      expect(faq_question).not_to be_valid
    end
    it "if question's length is more than 255 characters" do
      faq_question.question = Faker::Lorem.characters(256)
      expect(faq_question).not_to be_valid
    end
  end
end