require 'spec_helper'

describe Category do
  let :root do
    Category.root
  end

  it 'should have a root category' do
    expect(root.name).to eq 'root'
  end
  
  it 'should not have a parent' do
    expect(root.parent).to be_nil
  end

  it 'should have children' do
    expect(root.children).to be_present
  end
end