# # This file should be invoced before every test. It is a small version of the /db/seeds.rb file.

# # Data will be created by FactoryGirl
# require 'factory_girl_rails'

# # Create the user test@test.com
# FactoryGirl.create(:test_user)

# # Create a huge amount of random users
# 10.times do
#   FactoryGirl.create(:user)
# end

# # Create a huge amount of random sellers
# # A seller is a user with a shop
# 10.times do
#   user = FactoryGirl.create(:user)
#   shop = FactoryGirl.build(:shop)
#   shop.user_id = user.id
#   shop.save
#   # Create some articles in the shop
#   Random.rand(10).times do
#     article = FactoryGirl.build(:article)
#     article.user_id = user.id # Article belongs to user
#     article.save
#   end
# end

# # Create base categories
# root_category = Category.create name: 'root'
# category_schmuck = Category.create name: 'Schmuck', parent: root_category
# Category.create name: 'Handschmuck', parent: category_schmuck
# category_halsschmuck = Category.create name: 'Halsschmuck', parent: category_schmuck
# Category.create name: 'Kette', parent: category_schmuck
# Category.create name: 'Anhänger', parent: category_schmuck
# Category.create name: 'Kette & Anhänger', parent: category_schmuck
# category_kopfschmuck = Category.create name: 'Kopfschmuck', parent: category_schmuck
# Category.create name: 'Ohrringe', parent: category_kopfschmuck
# Category.create name: 'Fußschmuck', parent: category_schmuck
# Category.create name: 'Bauchschmuck', parent: category_schmuck
# Category.create name: 'Broschen', parent: category_schmuck
# Category.create name: 'Uhren', parent: root_category
# category_bilder = Category.create name: 'Bilder', parent: root_category
# Category.create name: 'Aquarelle', parent: category_bilder
# Category.create name: 'Öl', parent: category_bilder
# Category.create name: 'Skulpturen', parent: root_category
# Category.create name: 'Vasen', parent: root_category
# Category.create name: 'Antiques', parent: root_category
# Category.create name: 'Raritäten', parent: root_category

