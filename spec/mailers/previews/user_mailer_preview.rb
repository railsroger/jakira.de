class UserMailerPreview < ActionMailer::Preview
  # default from: "no-reply@jakira.de"
  
  # skip_filter :authenticate, only: [:send_invoice_to_user]

  def purchase_notification
    order = Order.first
    @buyer = order.purchase.user
    @order = order 
    # attachments['terms.txt'] = {mime_type: 'application/x-gzip',
                                    # content: @order.seller.shop.terms}
    # attachments.inline['logo2.png'] = File.read('app/assets/images/logo2.png')
    order.articles_orders.each do |articles_order|
      if articles_order.article.pictures.present?
        picture = articles_order.article.pictures.main
        attachments.inline[picture.image.file.filename] = File.read('public' + picture.image.url)
      else
        attachments.inline['placeholder.gif'] = File.read('app/assets/images/placeholder.gif')
      end
    end
    mail to: @buyer.email, subject: "Purchase Notification"
  end

  # def password_changed(user)
  #   @user = user
  #   mail to: @user.email, subject: "Your password was changed"
  # end

  # def email_changed(user)
  #   @user = user
  #   mail to: @user.email, subject: "Your email was changed"
  # end

  def articles_order_confirmation(articles_order)
    @article = articles_order.article
    @user = articles_order.buyer
    mail to: @user.email, subject: "Your order was confirmed"
  end

  def articles_order_dispatched(articles_order)
    @article = articles_order.article
    @user = articles_order.buyer
    mail to: @user.email, subject: "Your order was dispatched"
  end

  def monthly_invoices_sending
    mail to: "shamardin.k@gmail.com", subject: "Mounthly invoices sending"
  end

  def send_invoice_to_user(invoice)
    @invoice = invoice
    PDFKit.configure do |config|
      config.default_options = {
        page_size:      'Letter',
        margin_top:     '0.3in',
        margin_right:   '0.3in',
        margin_bottom:  '0.3in',
        margin_left:    '0.3in'
      }
    end
    kit = PDFKit.new(render_to_string(template: 'admin/invoices/pdf_version', layout: "pdf_layout"))
    file = kit.to_file('public/uploads/pdfs/' + @invoice.number + '.pdf')

    attachments.inline["Invoice " + @invoice.number + ".pdf"] = File.read('public/uploads/pdfs/' + @invoice.number + '.pdf')
    mail to: @invoice.user.email, subject: "Invoice " + @invoice.number 
  end

  def send_newsletter(newsletter, email)
    @text = newsletter.text
    mail to: email, subject: newsletter.subject, from: "newsletter@jakira.de"
  end

end