# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :purchase do
    association :shipping_address, :factory => :address
    association :payment_address, :factory => :address
    ip  "127.0.0.1"
    confirmed_at  '2013-11-08 23:59:33'
    state 'finished'
    user
  end
end
