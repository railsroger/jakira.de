FactoryGirl.define do
  factory :voucher do
    value               Voucher.values.sample
    from                Faker::Name.first_name
    recipient           Faker::Name.first_name
    message_text        Faker::Lorem.paragraph(40)
    email = Faker::Internet.email
    email               email
    email_confirmation  email
    payment_type        ['paypal', 'paydirekt'].sample
    agreement           '1'
  end
end
