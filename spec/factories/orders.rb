# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
    shipping_method
    is_approved ''

    factory :confirmed_order do
      is_approved 'approved'
    end

  end
end
