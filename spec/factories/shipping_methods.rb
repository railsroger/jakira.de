# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :shipping_method do
    association   :shipping_methodable, factory: :shop

    checked       true
    name          %w{within_germany within_eu worldwide}.sample
    sequence(:price) {rand(50)/0.9+3}
    duration_from {rand(1..15)}
    duration_to   {rand(16..30)}

    factory :pickup_shipping do
      name "pickup"
      price 0
      duration_from nil
      duration_to nil
    end

    factory :free_within_germany_shipping do
      name "free_within_germany"
      price 0
    end

  end
end
