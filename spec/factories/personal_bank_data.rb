# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :personal_bank_datum, class: 'PersonalBankData' do
    account_holder Faker::Lorem.characters(10)
    iban           Faker::Lorem.characters(10)
    swft_bic       Faker::Lorem.characters(10)
  end
end
