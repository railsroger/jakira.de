FactoryGirl.define do
  factory :article do
    title            {Faker::Commerce.product_name}
    category_id      {2+Random.rand(18)} # 2..20, 1=root
    price_cents      {(10 + Random.rand(500)*0.96)*100} # Produce some decimal numbers
    vat              '19'
    description {
      '<p>'+(Faker::Lorem.paragraphs 1+Random.rand(10), true).inject {|a,b| a+'</p><p>'+b}+'</p>'
    }
    state            ''

    factory :finished_article do
      user
      payment_selection
      after(:create) do |instance|
        create_list(:shipping_method, 3, article: instance)
        instance.state = 'finished'
        instance.save
      end

    end

  end

end
