FactoryGirl.define do
  factory :articles_order do
    note  'sample note'
    payment_type 'paypal'

    factory :article_order_without_article do
      order
      shipping_method
    end

    factory :stable_articles_order do
      association :article, factory: :stable_article
      order
      shipping_method
    end

    factory :approved_order do
      is_approved 'approved'
    end

  end

end
