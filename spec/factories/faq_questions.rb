FactoryGirl.define do
  factory :faq_question do
    association :faq_section
    question Faker::Lorem.characters(10)
  end
end