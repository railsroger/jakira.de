FactoryGirl.define do

  factory :user_address, class: "Address" do
    first_name   Faker::Name.first_name
    last_name    Faker::Name.last_name
  end

  factory :full_address, class: "Address" do 
    first_name   Faker::Name.first_name
    last_name    Faker::Name.last_name
    street       Faker::Address.street_name
    house_number Faker::Number.number(2)
    country      Faker::Address.country
    place        Faker::Address.state
    postal_code  Faker::Address.zip_code
    phone        Faker::PhoneNumber.phone_number
    mobile       Faker::PhoneNumber.cell_phone
  end
  factory :voucher_bank_transfer_address, class: "Address" do 
    first_name   Faker::Name.first_name
    last_name    Faker::Name.last_name
    street       Faker::Address.street_name
    place        Faker::Address.state
    postal_code  Faker::Address.zip_code
  end
  factory :voucher_bank_transfer_address_blank, class: "Address" do 
    first_name   ""
    last_name    ""
    street       ""
    place        ""
    postal_code  ""
  end

  factory :art_association_address, class: "Address" do
    company_name  Faker::Company.name
    first_name    Faker::Name.first_name
    last_name     Faker::Name.last_name
    street        Faker::Address.street_name
    house_number  Faker::Number.number(2)
    city          Faker::Address.city
    postal_code   Faker::Address.postcode
    phone         Faker::PhoneNumber.phone_number
  end
  
end
