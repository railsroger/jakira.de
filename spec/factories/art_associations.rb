FactoryGirl.define do
  factory :art_association do
    sequence(:email)    {Faker::Internet.email}
    password            'secret123'
    sequence(:nickname) {Faker::Internet.user_name}

    confirmation_sent_at  Time.now
    confirmed_at          Time.now + 2

    factory :unconfirmed_art_association do
      confirmed_at nil
    end

    association :address, factory: :art_association_address
  end

end
