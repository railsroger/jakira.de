FactoryGirl.define do
  factory :faq_section do
    name Faker::Lorem.characters(10)
  end
end