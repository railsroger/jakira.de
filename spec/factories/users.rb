FactoryGirl.define do
  factory :user do
    sequence(:email) {Faker::Internet.email}
    password   'secret123'
    sequence(:nickname) {Faker::Internet.user_name}
    # This will make the user confirmed
    confirmation_sent_at  '2013-11-08 23:59:33'
    confirmed_at          '2013-11-08 23:59:55'

    # address factory: :address # Every user has an address

    # This user has not confirmed his email

    factory :unconfirmed_user do
      confirmed_at nil
    end

    factory :guest_user do
      role "guest"
    end

    factory :normal_user do

      association :address, factory: :user_address

      factory :second_normal_user do
      end
    end

  end

end
