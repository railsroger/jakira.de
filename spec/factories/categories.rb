FactoryGirl.define do
  factory :category do
    name {Faker::Commerce.department}

    factory :root_category do
      name 'root'
    end
  end
end
