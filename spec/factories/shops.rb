FactoryGirl.define do
  factory :shop do
    association                  :user, factory: :normal_user
    association                  :address, factory: :full_address
    association                  :personal_bank_data, factory: :personal_bank_datum
    sequence(:name)              {Faker::Company.name}
    package                      ['smart', 'basic', 'professional'].sample
    imprint                      Faker::Lorem.paragraph(40)
    online                       true
    professional                 '1'
    payment_authorization        '1'
    subscription                 '1'
    accept_terms                 '1'
  end
end
