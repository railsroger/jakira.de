# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bank_transfer_data do
    account_holder '#{Faker::Name.first_name} #{Faker::Name.last_name}'
    iban           'fdsfsdg'
    swft_bic       'fdsfsdg'
  end
end
