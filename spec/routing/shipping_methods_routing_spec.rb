require "spec_helper"

describe ShippingMethodsController do
  describe "routing" do

    it "routes to #index" do
      get("/shipping_methods").should route_to("shipping_methods#index")
    end

    it "routes to #new" do
      get("/shipping_methods/new").should route_to("shipping_methods#new")
    end

    it "routes to #show" do
      get("/shipping_methods/1").should route_to("shipping_methods#show", :id => "1")
    end

    it "routes to #edit" do
      get("/shipping_methods/1/edit").should route_to("shipping_methods#edit", :id => "1")
    end

    it "routes to #create" do
      post("/shipping_methods").should route_to("shipping_methods#create")
    end

    it "routes to #update" do
      put("/shipping_methods/1").should route_to("shipping_methods#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/shipping_methods/1").should route_to("shipping_methods#destroy", :id => "1")
    end

  end
end
