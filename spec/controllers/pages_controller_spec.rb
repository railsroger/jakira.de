require 'spec_helper'

describe PagesController, type: :controller do
  it 'home page must work' do
    get :home
    # expect(response).to render_template("pages/home")
    expect(response).to have_http_status(:success)
  end

end