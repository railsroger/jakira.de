require 'spec_helper'

describe PurchasesController do

  # This should return the minimal set of attributes required to create a valid
  # Purchase. As you add validations to Purchase, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { {  } }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PurchasesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all purchases as @purchases" do
      combined_order = Purchase.create! valid_attributes
      get :index, {}, valid_session
      assigns(:purchases).should eq([combined_order])
    end
  end

  describe "GET show" do
    it "assigns the requested combined_order as @combined_order" do
      combined_order = Purchase.create! valid_attributes
      get :show, {:id => combined_order.to_param}, valid_session
      assigns(:combined_order).should eq(combined_order)
    end
  end

  describe "GET new" do
    it "assigns a new combined_order as @combined_order" do
      get :new, {}, valid_session
      assigns(:combined_order).should be_a_new(Purchase)
    end
  end

  describe "GET edit" do
    it "assigns the requested combined_order as @combined_order" do
      combined_order = Purchase.create! valid_attributes
      get :edit, {:id => combined_order.to_param}, valid_session
      assigns(:combined_order).should eq(combined_order)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Purchase" do
        expect {
          post :create, {:combined_order => valid_attributes}, valid_session
        }.to change(Purchase, :count).by(1)
      end

      it "assigns a newly created combined_order as @combined_order" do
        post :create, {:combined_order => valid_attributes}, valid_session
        assigns(:combined_order).should be_a(Purchase)
        assigns(:combined_order).should be_persisted
      end

      it "redirects to the created combined_order" do
        post :create, {:combined_order => valid_attributes}, valid_session
        response.should redirect_to(Purchase.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved combined_order as @combined_order" do
        # Trigger the behavior that occurs when invalid params are submitted
        Purchase.any_instance.stub(:save).and_return(false)
        post :create, {:combined_order => {  }}, valid_session
        assigns(:combined_order).should be_a_new(Purchase)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Purchase.any_instance.stub(:save).and_return(false)
        post :create, {:combined_order => {  }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested combined_order" do
        combined_order = Purchase.create! valid_attributes
        # Assuming there are no other purchases in the database, this
        # specifies that the Purchase created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Purchase.any_instance.should_receive(:update).with({ "these" => "params" })
        put :update, {:id => combined_order.to_param, :combined_order => { "these" => "params" }}, valid_session
      end

      it "assigns the requested combined_order as @combined_order" do
        combined_order = Purchase.create! valid_attributes
        put :update, {:id => combined_order.to_param, :combined_order => valid_attributes}, valid_session
        assigns(:combined_order).should eq(combined_order)
      end

      it "redirects to the combined_order" do
        combined_order = Purchase.create! valid_attributes
        put :update, {:id => combined_order.to_param, :combined_order => valid_attributes}, valid_session
        response.should redirect_to(combined_order)
      end
    end

    describe "with invalid params" do
      it "assigns the combined_order as @combined_order" do
        combined_order = Purchase.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Purchase.any_instance.stub(:save).and_return(false)
        put :update, {:id => combined_order.to_param, :combined_order => {  }}, valid_session
        assigns(:combined_order).should eq(combined_order)
      end

      it "re-renders the 'edit' template" do
        combined_order = Purchase.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Purchase.any_instance.stub(:save).and_return(false)
        put :update, {:id => combined_order.to_param, :combined_order => {  }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested combined_order" do
      combined_order = Purchase.create! valid_attributes
      expect {
        delete :destroy, {:id => combined_order.to_param}, valid_session
      }.to change(Purchase, :count).by(-1)
    end

    it "redirects to the purchases list" do
      combined_order = Purchase.create! valid_attributes
      delete :destroy, {:id => combined_order.to_param}, valid_session
      response.should redirect_to(combined_orders_url)
    end
  end

end
