require 'spec_helper'

describe ArticlesOrdersHelper, type: :helper do

  describe 'is_the_articles_order_status_can_be_updatable' do

    context 'should work properly when status is' do
      
      it 'awaiting_confirmation' do
        articles_order = ArticlesOrder.new(status: "awaiting_confirmation")

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "awaiting_confirmation")
        expect(result).not_to be_truthy

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "confirmed")
        expect(result).to be_truthy

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "paid")
        expect(result).not_to be_truthy

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "sent")
        expect(result).not_to be_truthy
      end

      it 'confirmed' do
        articles_order = ArticlesOrder.new(status: "confirmed")

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "paid")
        expect(result).to be_truthy

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "sent")
        expect(result).to be_truthy

      end

      it 'paid' do
        articles_order = ArticlesOrder.new(status: "paid")

        result = is_the_articles_order_status_can_be_updatable?(articles_order, "sent")
        expect(result).to be_truthy
      end
    end
  end

  describe 'articles_order_disabled_statuses' do

    before do
      @states = ArticlesOrder.statuses.keys + ['review', 'finished']
    end

    it "should contain only confirmed and finished if status is awaiting_confirmation" do
      disabled_statuses = articles_order_disabled_statuses(@states, 'awaiting_confirmation')
      expect(disabled_statuses).to be == ['paid', 'sent', 'review']
    end

    context 'should be properly if status is' do

      it "confirmed" do
        disabled_statuses = articles_order_disabled_statuses(@states, 'confirmed')
        expect(disabled_statuses).to be == ['awaiting_confirmation', 'confirmed']
      end

      it "paid" do
        disabled_statuses = articles_order_disabled_statuses(@states, 'paid')
        expect(disabled_statuses).to be == ['awaiting_confirmation', 'confirmed', 'paid']
      end

      it "sent" do
        disabled_statuses = articles_order_disabled_statuses(@states, 'sent')
        expect(disabled_statuses).to be == ['awaiting_confirmation', 'confirmed', 'paid', 'sent']
      end

    end
  end
end