class MailInterceptor
  def self.delivering_email(mail)
    if mail.from.to_s.include?('@jakira.de')
      email = mail.to.to_s.split('"')[1]
      user = User.find_by(email: email) || User.find_by(unconfirmed_email: email)
      if user.present? && user.deleted?
        mail.perform_deliveries = false
      end
    end
  end
end