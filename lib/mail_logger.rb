class MailLogger

  def self.delivered_email(mail)
    if mail.from.to_s.include?('@jakira.de')
      email = mail.to.to_s.split('"')[1]
      user = User.find_by(email: email) || User.find_by(unconfirmed_email: email)
      if user.present?
        Maillog.create(user_id: user.id,
                         email: email,
                       subject: mail.subject.to_s,
                       sent_at: mail.date)
      else
        Maillog.create(email: email,
                     subject: mail.subject.to_s,
                     sent_at: mail.date)
      end
    end
  end
end