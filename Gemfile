source 'http://rubygems.org/'  # https://rubygems.org/ was replaced because of certificates problems of rubygems.org

ruby '2.1.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.14'

gem 'pg'

gem 'bootstrap-sass'
gem 'sass-rails', '~> 4.0.2'  # Use SCSS for stylesheets
gem 'uglifier', '>= 1.3.0'    # Use Uglifier as compressor for JavaScript assets
gem 'coffee-rails', '>= 4.0.0'# Use CoffeeScript for .js.coffee assets and views

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails', '3.1.0'
gem 'jquery-ui-rails', '~> 5.0'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '>= 1.2'

gem 'devise'   # Authorization library for user management
gem 'devise-encryptable' # State of the art encryption for passwords
gem 'ancestry' # ActiveRecord hierarchy. Used for Categories
gem 'faker'    # Generator for random data. Used for demonstration purposes
gem 'wicked'   # Multi-Form-Wizards
gem 'kaminari' # Implements pagination of Objects like Articles.
gem 'money-rails'#, '0.9.0' # Money handler
gem "monetize"
# gem 'money'#, '~> 5.1'
gem 'friendly_id' # Creates SEO friendly URLs
gem 'carrierwave' # File-Uploader
gem 'mini_magick' # Image manipulator for carrierwave
gem 'remotipart'  # Remote file upload
gem 'tinymce-rails'
# gem 'tinymce-rails', :git => 'git://github.com/spohlenz/tinymce-rails.git'
gem 'select2-rails'
gem 'country_select' # Country selection
gem 'bxslider-rails' # Image slider assets
gem 'jquery-turbolinks'
gem 'paypal-sdk-rest'
gem 'credit_card_validations'
gem 'http_accept_language'
gem 'globalize', '~> 4.0.3'
gem 'rails_autolink'
gem 'slim'
gem 'whenever', require: false
gem 'pdfkit'
gem 'wkhtmltopdf-binary', '0.9.9'
gem 'ckeditor', github: 'galetahub/ckeditor'
gem 'date_validator'
gem 'premailer-rails'
gem 'nokogiri'
gem 'spinjs-rails'
gem 'rack-mini-profiler'
gem 'touchpunch-rails'
gem 'httparty'
gem 'elevatezoom-rails'
gem 'fastimage'
gem 'jquery-cropper'
gem 'jquery-minicolors-rails'
gem 'valid_email'
gem 'coupon_code'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem "omniauth-google-oauth2"
gem "omniauth-paypal"
gem 'bootstrap-select-rails'
gem 'roo'
gem 'roo-xls'
gem 'pdf-forms'
gem 'grim'
gem 'cancancan', '~> 1.10'

# This project rely on RSpec for testing. Don't use TestUnit here!
group :test, :development do
  gem 'factory_girl_rails'
  gem 'rspec-rails', '~> 3.0'
  # gem 'pry'
end

# Autodocumentation of the project by sdoc.
group :doc do
  gem 'sdoc', require: false # bundle exec rake doc:rails generates the API under doc/api.
end

# Use ActiveModel has_secure_password
# This is actually not necessary!
gem 'bcrypt', '>= 3.1.2'

group :development do
  gem 'quiet_assets'

  # Use Capistrano for deployment
  gem "capistrano", "~> 3.4"
  gem 'capistrano-ssh-doctor', '~> 1.0'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rvm'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-faster-assets', '~> 1.0'
  gem 'capistrano-passenger'
  gem 'puma'

  gem 'rubocop', '~> 0.46.0', require: false
  gem 'slim_lint', require: false
end