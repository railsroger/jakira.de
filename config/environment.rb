# Load the Rails application.
require File.expand_path('../application', __FILE__)
require './lib/mail_logger'
require './lib/mail_interceptor'
require 'coupon_code'
# Initialize the Rails application.
WeltDerUnikate::Application.initialize!

