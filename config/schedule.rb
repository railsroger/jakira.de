# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

# env :PATH, ENV['PATH']
# set :env_path, '/home/holy-roman/.rvm/rubies/ruby-2.1.0/bin/ruby'

every 1.day do
  runner "Newsletter.send_newsletters"
end

every 1.month do
  runner "User.monthly_invoices_generating"
end

every 3.month do
  runner "User.quarter_invoices_generating"
end