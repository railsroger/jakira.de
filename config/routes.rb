WeltDerUnikate::Application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  constraints subdomain: 'www' do
    get ':any', to: redirect(subdomain: nil, path: '/%{any}'), any: /.*/
  end

  constraints subdomain: 'administration' do
    namespace(:admin, path: '/') do
      get '/' => :show
      get :maillog
      resources :art_associations, except: [:destroy, :show]
      resources :members do
        get :update_monthly_charge_for_all_users
        member do
          get :purchases_and_sales
          get :user_profile
          get :user_settings
          get "shop_profile/about_me"           => :shop_profile_about_me
          get "shop_profile/settings"           => :shop_profile_settings
          get "shop_profile/terms_and_imprint"  => :shop_profile_terms_and_imprint
          get "shop_profile/revocation"         => :shop_profile_revocation
          post :update_user_data
          get :update_monthly_charge_for_the_user
          get :restore_deleted_account
        end
      end
      resources :invoices, only: [] do
        collection do
          get :created
          get :open
          get :paid
          get :cancellations
          get :credits
          get :check_boxes_action
          get :send_monthly_invoice
          get :send_commission_invoice
        end
      end
      resources :correspondence, only: [:index] do
        collection do
          get :registration
          get :new_shop
          get :to_old_email_about_email_changing
          get :confirmation_email
          get :password_changed
          get :articles_order_dispatched
          get :article_is_already_sold
          get :new_order_message_from_buyer
          get :new_order_message_from_seller
          get :buyer_cancellation
          get :seller_cancellation
          get :new_message
          get :purchase_notification_confirmed_to_buyer
          get :purchase_notification_confirmed_to_seller
          get :purchase_notification_unconfirmed_to_buyer
          get :purchase_notification_unconfirmed_to_seller
          get :share_article
          get :new_voucher
        end
      end
      resources :newsletters, only: [:new, :create]
      resources :business_cards, only: [:index] do
        post :change_details, on: :member
      end
      resources :vouchers, only: :index do
        get :change, on: :collection
        member do
          post :change_status
          post :change_paid_on
        end
      end
      get :text_content
      scope :text_content do
        resources :faq_sections,
            param: :faq_section_id do
          member do
            resources :faq_questions,
                param: :faq_question_id,
                except: :index
          end
        end
        get :discover_the
        get :find_yours
      end
    end
  end

  get 'admin', to: redirect(subdomain: 'administration', path: '/')
  get 'admin/:any', to: redirect(subdomain: 'administration', path: '/%{any}')

  constraints subdomain: false do
    resources :invoices, only: [:index, :show]
  end
  resources :invoices, only: [] do
    get :pdf_version, on: :member
  end

  resources :favorites, only: [:index, :create, :destroy]
  resources :favorite_shops, only: [:index, :create, :destroy]
  resources :articles_view_types, only: [] do
    get :change, on: :collection
  end

  resources :messages, except: [:index] do
    member do
      get :reply
      get :to_trash
    end
    collection do
      get  :inbox
      get  :sent
      get  :drafts
      get  :bin
      get  :archive
      post :check_boxes_action
      get  :empty_trash
    end
  end

  resources :orders do
    collection do
      get :open
      get :completed
      get :canceled
    end
  end

  resources :purchases do
    member do 
      get  :addresses_and_payment_types
      get  :check_order
      get  :complete
    end
    collection do
      get :open
      get :completed
    end
  end

  resources :articles, except: :index do
    post :change, on: :member
    collection do
      get :sold
      get :deleted
      get :delete_all
      get :online
      get :offline
      get :templates
    end
  end


  resources :documents

  scope :search do
    get '/:search', to: 'articles#search'
    get '/', to: 'articles#search'
  end

  resources :payment_infos
  resources :payment_selections
  resources :shipping_methods
  resources :pictures, only: [:create, :destroy, :edit] do
    post :crop, on: :member
    post :create_shop_main, on: :collection
  end

  resources :withdrawal_forms, only: [:create, :destroy] do
    get :preview, on: :member
  end

  resources :shopping_baskets, only: [:create,
                                      :index,
                                      :update,
                                      :destroy]

  resources :articles_orders, only: [:destroy] do
    member do
      get :paypal_payment
      get :bank_details
      get :credit_card_payment
      post :credit_card_payment
      get :write_a_review
      get :write_a_review_for_buyer
      get :mark_as_sold
      get :mark_order_messages_as_read
      patch :use_voucher
      patch :set_payment_type
      patch :set_pickup_as_shipping_method
      patch :use_shipping_address
      get :closure_confirmation
      get :close
      get :cancel
      patch :update_status
      get :send_payment_reminder
    end
  end

  resources :credit_card_data, only: [:create]

  get '/' => 'shops#show', constraints: lambda { |r| r.subdomain.present? && r.subdomain != "www"}

  controller :pages do
    root to: :home
    get :imprint
    get :privacy
    get :about_jakira
    get :terms
  end

  resources :categories, only: :show

  devise_for :users, controllers: {registrations: 'users/registrations',
                                   sessions: 'users/sessions',
                                   passwords: 'users/passwords',
                                   confirmations: 'users/confirmations',
                                   omniauth_callbacks: "users/omniauth_callbacks"}
  # devise_scope :user do
  #   get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
  #   get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
  # end

  resources :ratings, only: [:update, :index]

  resources :shops, except: [:edit, :index] do
    collection do
      get :about_me
      get :settings
      get :terms_and_imprint
      get :revocation
      get :open
    end
    member do
      get :show_terms
      get :show_revocation
      get :ratings
      get :download_withdrawal_form
    end
  end

  resources :order_messages, only: [:create, :show]
  resources :share, only: [:new, :create, :index]

  resources :business_cards, only: [:index, :create]

  resources :vouchers, only: [:show, :new, :create] do
    get :after_payment, on: :collection
    get :pdf_version, on: :member
  end

  resources :main_voucher_data, only: :update

  resources :addresses, only: [] do
    get :load_country_code, on: :collection
    member do
      post :update_purchase_address
    end
  end

  resources :art_associations,
      only: [:create, :index] do
    collection do
      get :register
      patch :become_a_member
      scope :admin_panel, as: 'admin_panel' do
        get :events
        get :settings
        scope as: 'art_associations' do
          resources :members,
              controller: 'art_associations/members',
              except: [:show, :destroy] do
            post :import_xls, on: :collection
          end
        end
      end
    end
  end

  resources :faq, only: [:index, :show]

  get '/contact', to: 'contacts#new'
  resources :contacts, only: [:create]
end