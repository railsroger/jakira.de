# Be sure to restart your server when you modify this file.

WeltDerUnikate::Application.config.session_store :cookie_store,
key: '_WeltDerUnikate_session',
domain: {
  production:  ['.jakira.de', '79.143.181.137'],
  development: ['.lvh.me', '192.168.1.4']
}.fetch(Rails.env.to_sym, :all)
