class AddVoucherIdToArticlesOrders < ActiveRecord::Migration
  def change
    add_column :articles_orders, :voucher_id, :integer
    add_money :articles_orders, :voucher_discount
  end
end