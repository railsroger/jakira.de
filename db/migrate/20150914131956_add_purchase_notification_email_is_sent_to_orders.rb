class AddPurchaseNotificationEmailIsSentToOrders < ActiveRecord::Migration
  def change
	  add_column :orders,
               :purchase_notification_email_is_sent,
               :boolean,
               default: :false
  end
end
