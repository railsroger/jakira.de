class RemovePaymentTypeStringFromArticlesOrders < ActiveRecord::Migration
  def change
    remove_column :articles_orders, :payment_type, :string
  end
end
