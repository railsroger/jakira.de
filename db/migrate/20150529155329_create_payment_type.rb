class CreatePaymentType < ActiveRecord::Migration
  def change
    create_table :payment_types do |t|
      t.string :name
    end
    create_table :articles_payment_types, id: false do |t|
      t.belongs_to :article, index: true
      t.belongs_to :payment_type, index: true
    end
  end
end
