class CreateShoppingBaskets < ActiveRecord::Migration
  def change
    create_table :shopping_baskets do |t|
      t.integer :article_id
      t.integer :user_id
  	  t.integer :shipping_method_id
  	  t.integer :payment_type_id
      t.integer :article_amount, default: 1
      t.timestamps
    end
  end
end
