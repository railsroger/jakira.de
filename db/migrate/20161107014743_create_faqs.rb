class CreateFaqs < ActiveRecord::Migration
  def up
    create_table :faq_sections do |t|
      t.string  :name
    end
    create_table :faq_questions do |t|
      t.string  :question
      t.text    :answer
      t.integer :faq_section_id
    end
    
    FaqSection.create_translation_table!({ 
      name: :string
    }, {
      migrate_data: true
    })

    FaqQuestion.create_translation_table!({ 
      question: :string,
      answer: :text
    }, {
      migrate_data: true
    })
  end
  def down
    FaqSection.drop_translation_table! migrate_data: true
    FaqQuestion.drop_translation_table! migrate_data: true
    drop_table :faq_sections
    drop_table :faq_questions
  end
end
