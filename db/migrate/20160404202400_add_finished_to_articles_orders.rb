class AddFinishedToArticlesOrders < ActiveRecord::Migration
  def change
    add_column :articles_orders, :finished, :boolean, default: false
  end
end
