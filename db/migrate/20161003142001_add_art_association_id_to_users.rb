class AddArtAssociationIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :art_association_id, :integer
  end
end
