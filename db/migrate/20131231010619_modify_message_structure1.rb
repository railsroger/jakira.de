class ModifyMessageStructure1 < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer  :sender_id
      t.integer  :recipient_id
      t.boolean  :sender_deleted, default: false
      t.string   :subject
      t.text     :body
      t.datetime :read_at
      t.string   :container, default: 'draft'
      t.timestamps
    end
  end
end
