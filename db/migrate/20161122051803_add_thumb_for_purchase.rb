class AddThumbForPurchase < ActiveRecord::Migration
  def change
    Purchase.delete_all
    Order.delete_all
    ArticlesOrder.delete_all
    Article.ordered.delete_all
    Rating.delete_all
    Picture.all.each { |p| p.destroy if p.article.nil? && p.shop.nil? }
    Picture.all.each do |picture|
      picture.image.recreate_versions!
    end
  end
end
