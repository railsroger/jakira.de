class ChangeAssignmentApprovement < ActiveRecord::Migration
  def change
    rename_column :assignments, :is_approved, :is_approved_seller
    add_column    :assignments, :is_approved_buyer, :integer
  end
end
