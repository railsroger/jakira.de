class ChangeAssignmentType < ActiveRecord::Migration
  def change
    remove_column :assignments, :is_approved_buyer
    add_column :assignments, :is_approved_buyer, :string
  end
end
