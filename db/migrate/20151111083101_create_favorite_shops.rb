class CreateFavoriteShops < ActiveRecord::Migration
  def change
    create_table :favorite_shops do |t|
      t.integer  :shop_id
      t.integer  :user_id
    end
  end
end
