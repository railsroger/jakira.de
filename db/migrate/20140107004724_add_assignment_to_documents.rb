class AddAssignmentToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :assignment_id, :integer
  end
end
