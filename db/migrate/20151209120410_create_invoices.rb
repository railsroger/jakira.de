class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string   :status
      t.string 	 :number
      t.money 	 :price
      t.integer  :user_id
      t.integer  :status
      t.timestamps
    end
  end
end
