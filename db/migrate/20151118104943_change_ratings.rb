class ChangeRatings < ActiveRecord::Migration
  def self.up
    change_column :ratings, :text, :text
    add_column :ratings, :articles_order_id, :integer
    add_column :ratings, :to, :string
    add_column :ratings, :is_finished, :boolean, default: false
  end

  def self.down
    change_column :ratings, :text, :string
    remove_column :ratings, :articles_order_id, :integer
    remove_column :ratings, :to, :string
    remove_column :ratings, :is_finished, :boolean, default: false
  end
end
