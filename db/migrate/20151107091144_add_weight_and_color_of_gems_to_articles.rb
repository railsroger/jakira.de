class AddWeightAndColorOfGemsToArticles < ActiveRecord::Migration
  def change
    create_table :gem_colors do |t|
      t.string  :name
    end

    create_table :articles_gem_colors, id: false do |t|
      t.belongs_to :article, index: true
      t.belongs_to :gem_color, index: true
    end
  end
end
