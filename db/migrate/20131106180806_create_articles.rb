class CreateArticles < ActiveRecord::Migration
  def change

    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.string :ancestry
    end

    add_index :categories, :ancestry

    create_table :articles do |t|
      t.belongs_to  :user
      t.string      :title
      t.text        :description
      t.belongs_to  :category
      t.money       :price
      t.integer     :vat
      t.string      :artist
      t.string      :motive
      t.string      :dispatch_delay
      t.integer     :status, default: 0
      t.string      :year
      t.string      :made_in
      t.boolean     :completely_deleted
      t.integer     :article_type
      t.string      :gems
      t.string      :width
      t.string      :length
      t.string      :height
      t.string      :weight
      t.string      :weight_of_gem
      t.string      :sku
      t.boolean     :customizable, default: false
      t.string      :state
      t.string      :slug
      t.boolean     :deleted, default: false
      t.text        :customizable_comment
      t.integer     :order_confirmation, default: 0
      t.integer     :kind, default: 0
      t.string      :template_name

      t.timestamps
    end

  end
end
