class RemaneColumnsInBankTransferData < ActiveRecord::Migration
  def change
  	remove_column :bank_transfer_data, :account_number
  	remove_column :bank_transfer_data, :bank_code
  	remove_column :bank_transfer_data, :iban
  	remove_column :bank_transfer_data, :swft_bic
  	add_column :bank_transfer_data, :account_number, :string
  	add_column :bank_transfer_data, :bank_code, :string
  	add_column :bank_transfer_data, :iban, :string
  	add_column :bank_transfer_data, :swft_bic, :string
  end
end
