class MigratePicturesToImages < ActiveRecord::Migration
  def up
    Article.ordered.each { |a| a.destroy if a.articles_order.nil? }

    Picture.all.each { |p| p.destroy if p.article.nil? && p.shop.nil? }

    Picture.all.where.not(article_id: nil).each do |picture|
      image = Image.create(kind: :article_pictures,
                           parent_id: picture.article_id)
      image.image = File.new(File.join(Rails.root, '/public' + picture.image.url))
      image.save!
      image.image.recreate_versions!
      if picture.id == picture.article.main_picture_id
        picture.article.update(main_picture_id: image.id)
      end
    end
    Picture.all.where.not(shop_id: nil).each do |picture|
      image = Image.create(kind: :shop_pictures,
                           parent_id: picture.shop_id)
      image.image = File.new(File.join(Rails.root, '/public' + picture.image.url))
      image.save!
      image.image.recreate_versions!
    end
    Shop.where.not(main_picture_id: nil).each do |shop|
      image = Image.create(kind: :shop_main_pictures,
                           parent_id: shop.id)
      image.image = File.new(File.join(Rails.root, '/public' + Picture.find(shop.main_picture_id).image.url))
      image.save!
      image.image.recreate_versions!
    end
    Picture.delete_all
    remove_column :shops, :main_picture_id, :integer
  end
  def down
    add_column :shops, :main_picture_id, :integer
  end
end
