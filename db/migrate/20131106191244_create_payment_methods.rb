class CreatePaymentMethods < ActiveRecord::Migration
  def change
    create_table :payment_selections do |t|
      t.belongs_to :article
      t.string     :type
      t.boolean    :payment_wire
      t.boolean    :payment_paypal
      t.boolean    :payment_advance
      t.boolean    :payment_cash
      t.timestamps
    end
  end
end
