class ChangeStringToTextInSomeFields < ActiveRecord::Migration
  def self.up
  	change_column :shopping_baskets, :message_text, :text
  end
  def self.down
  	change_column :shopping_baskets, :message_text, :string
  end
end
