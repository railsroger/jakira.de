class AddAddressesToPurchase < ActiveRecord::Migration
  def change
    Purchase.all.each do |purchase|
      user_address = purchase.user.address.attributes.except("id")
      purchase.build_shipping_address user_address
      purchase.build_payment_address user_address
      purchase.save!(validate: false)
    end
  end
end
