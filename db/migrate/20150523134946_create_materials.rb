class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :name
    end
    create_table :articles_materials, id: false do |t|
      t.belongs_to :article, index: true
      t.belongs_to :material, index: true
    end
  end
end
