class ChangeWorthInVouchers < ActiveRecord::Migration
  def change
    remove_column :vouchers, :worth, :integer
    add_money :vouchers, :value
  end
end
