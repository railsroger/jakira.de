class CreateTranslationsForBusinessCardDetails < ActiveRecord::Migration
  def up
    BusinessCardDetails.create_translation_table! price:            :string,
                                                  quantity:         :string,
                                                  format:           :string,
                                                  extent:           :string,
                                                  dyeing:           :string,
                                                  paper:            :string,
                                                  gram_matur:       :string,
                                                  printing_process: :string,
                                                  paintwork:        :string
  end
  def down
    BusinessCardDetails.drop_translation_table!
  end
end
