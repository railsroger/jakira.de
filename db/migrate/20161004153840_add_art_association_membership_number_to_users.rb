class AddArtAssociationMembershipNumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :art_association_membership_number, :integer
  end
end
