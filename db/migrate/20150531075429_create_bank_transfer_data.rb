class CreateBankTransferData < ActiveRecord::Migration
  def change
    create_table :bank_transfer_data do |t|
      t.boolean :bank_transfer, default: false
      t.string  :account_holder
      t.integer :account_number
      t.integer :bank_code
      t.string  :bank_name
      t.integer :iban
      t.integer :swft_bic
      t.integer :article_id
      t.timestamps
    end
  end
end
