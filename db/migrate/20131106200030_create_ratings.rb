class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer    :score
      t.belongs_to :order
      t.string     :text
      t.belongs_to :user

      t.timestamps
    end
  end
end
