class AddTranslatesToGemColor < ActiveRecord::Migration
  def self.up
    GemColor.create_translation_table!({
      name: :string
    }, {
      :migrate_data => true
    })
  end

  def self.down
    GemColor.drop_translation_table! migrate_data: true
  end
end