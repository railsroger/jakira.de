class ChangeShoppingBasketsColumns < ActiveRecord::Migration
  def change
    add_column :shopping_baskets, :message_sent_at, :datetime
  end
end
