class AddCanceledToArticlesOrder < ActiveRecord::Migration
  def up
    add_column :articles_orders, :canceled, :boolean, default: false
  end

  def down
    remove_column :articles_orders, :canceled
  end
end
