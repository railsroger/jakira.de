class AddSubscription < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer   :user_id
      t.string    :name
      t.timestamp :activated_at
      t.integer   :duration
      t.string    :state
      t.money :price

      t.timestamps
    end

  end
end
