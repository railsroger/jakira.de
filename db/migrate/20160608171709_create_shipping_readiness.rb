class CreateShippingReadiness < ActiveRecord::Migration
  def change
    create_table :shipping_readinesses do |t|
      t.integer :readiness
      t.integer :from
      t.integer :to
      t.belongs_to :article
      t.belongs_to :shop
    end
  end
end
