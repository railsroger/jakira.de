class CreateOrders < ActiveRecord::Migration
  def change

    create_table :purchases do |t|
      t.belongs_to :user
      t.integer    :shipping_address_id
      t.integer    :payment_address_id
      t.string     :state
      t.datetime   :confirmed_at
      t.boolean    :confirmed, default: false
      t.timestamps
    end

    create_table :orders do |t|
      t.boolean     :allow_partial_approval
      t.boolean     :is_paid
      t.boolean     :is_sent
      t.boolean     :is_received
      t.belongs_to  :purchase
      t.belongs_to  :shipping_method
      t.integer     :seller_address_id
      t.integer     :seller_id
      t.timestamps
    end
  end
end
