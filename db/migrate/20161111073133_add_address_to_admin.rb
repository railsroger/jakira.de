class AddAddressToAdmin < ActiveRecord::Migration
  def up
    admin = User.admin
                .first_or_initialize(role: :admin,
                                     nickname: "admin",
                                     email: "admin@jakira.de",
                                     password: "wdu2014",
                                     password_confirmation: "wdu2014")
    admin.build_address(first_name: "Main", last_name: "Admin")
    admin.save
  end
  def down
    User.admin.first.address.destroy
  end
end
