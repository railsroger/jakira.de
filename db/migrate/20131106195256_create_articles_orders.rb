class CreateArticlesOrders < ActiveRecord::Migration
  def change
    create_table :articles_orders do |t|
      t.belongs_to  :article
      t.belongs_to  :order
      t.integer		  :shipping_method_id
      t.integer		  :payment_type_id
      t.integer     :shipping_address_id
      t.integer     :payment_address_id
      t.integer     :status
      t.integer     :invoice_id
      t.datetime    :message_sent_at
      t.datetime    :sent_at
      t.datetime    :paid_at
      t.datetime    :ordered_at
      t.datetime    :confirmed_at
      t.text        :message_text
      t.string      :payment_type
      t.integer     :amount, default: 1

      t.timestamps
    end
  end
end
