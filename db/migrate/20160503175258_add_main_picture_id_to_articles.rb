class AddMainPictureIdToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :main_picture_id, :integer
  end
end
