class FixNicknames < ActiveRecord::Migration
  def change
    User.all.each do |user|
      user.update(nickname: nil) if !user.valid?
    end
  end
end
