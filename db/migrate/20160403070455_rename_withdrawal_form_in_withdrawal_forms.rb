class RenameWithdrawalFormInWithdrawalForms < ActiveRecord::Migration
  def change
    remove_column :withdrawal_forms, :withdrawal_form, :string
    add_column :withdrawal_forms, :file, :string
  end
end