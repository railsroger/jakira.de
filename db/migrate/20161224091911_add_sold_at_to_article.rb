class AddSoldAtToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :sold_at, :datetime
  end
end
