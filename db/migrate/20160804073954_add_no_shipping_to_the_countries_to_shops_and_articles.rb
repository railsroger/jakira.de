class AddNoShippingToTheCountriesToShopsAndArticles < ActiveRecord::Migration
  def change
    add_column :shops, :no_shipping_to_the_countries, :string, array: true
    add_column :articles, :no_shipping_to_the_countries, :string, array: true
  end
end
