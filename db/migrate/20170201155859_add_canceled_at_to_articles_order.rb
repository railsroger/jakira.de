class AddCanceledAtToArticlesOrder < ActiveRecord::Migration
  def up
    add_column :articles_orders, :canceled_at, :datetime
    ArticlesOrder.canceled.each do |articles_order|
      articles_order.update_attribute('canceled_at', articles_order.updated_at)
    end
  end
  def down
    remove_column :articles_orders, :canceled_at
  end
end