class ChangeFromTypeToEnumInRating < ActiveRecord::Migration
  def up
    add_column :ratings, :from_number, :integer
    Rating.all.each do |rating|
      if rating.from == 'buyer'
        rating.update(from_number: 0)
      elsif rating.from == 'seller'
        rating.update(from_number: 1)
      end
    end
    remove_column :ratings, :from
    rename_column :ratings, :from_number, :from
  end
  def down
    add_column :ratings, :from_string, :string
    Rating.all.each do |rating|
      if rating.from_buyer?
        rating.update(from_string: 'buyer')
      elsif rating.from_seller?
        rating.update(from_string: 'seller')
      end
    end
    remove_column :ratings, :from
    rename_column :ratings, :from_string, :from
  end
end
