class AddUseShippingAddressAsPaymentAddress < ActiveRecord::Migration
  def up
    add_column :purchases,
        :use_shipping_address_for_payment_address,
        :boolean,
        default: false

    add_column :purchases,
        :address_for_all_items,
        :boolean,
        default: false

    add_column :articles_orders,
        :use_shipping_address_for_payment_address,
        :boolean,
        default: false

    add_column :purchases,
        :pickup,
        :boolean,
        default: false
  end

  def down
    remove_column :purchases,
        :use_shipping_address_for_payment_address

    remove_column :purchases,
        :address_for_all_items

    remove_column :articles_orders,
        :use_shipping_address_for_payment_address

    remove_column :purchases,
        :pickup
  end
end
