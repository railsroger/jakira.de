class CreateMainVoucherData < ActiveRecord::Migration
  def change
    create_table :main_voucher_data do |t|
      t.boolean :bank_transfer, default: false
      t.boolean :paypal, default: false
      t.boolean :paydirekt, default: false
      t.string :mandatory_approval
      t.string :voucher_terms
      t.string :cancellation
      t.string :imprint
    end
  end
end
