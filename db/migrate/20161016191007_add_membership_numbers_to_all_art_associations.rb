class AddMembershipNumbersToAllArtAssociations < ActiveRecord::Migration
  def change
    last_membership_number = 1
    ArtAssociation.all.each do |art_association|
      art_association.membership_number = last_membership_number
      art_association.save
      last_membership_number += 1
    end
  end
end
