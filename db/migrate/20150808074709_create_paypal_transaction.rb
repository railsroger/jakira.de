class CreatePaypalTransaction < ActiveRecord::Migration
  def change
    create_table :paypal_transactions do |t|
  	  t.integer  :articles_order_id
  	  t.string	 :hash_value
  	  t.string   :payment_id
  	  t.boolean  :complete, default: false
    end
  end
end
