class AddSentOnToVouchers < ActiveRecord::Migration
  def up
    add_column :vouchers, :sent_on, :string, array: true
    add_column :vouchers, :paid_on, :datetime
    Voucher.all.each do |voucher|
      voucher.update(sent_on: [voucher.updated_at],
                     paid_on: voucher.updated_at)
    end
  end
  def down
    remove_column :vouchers, :sent_on
    remove_column :vouchers, :paid_on
  end
end
