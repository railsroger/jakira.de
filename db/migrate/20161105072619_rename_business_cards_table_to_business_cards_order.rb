class RenameBusinessCardsTableToBusinessCardsOrder < ActiveRecord::Migration
  def up
    rename_table :business_cards, :business_cards_orders
    remove_column :business_cards_orders, :front, :integer
    remove_column :business_cards_orders, :rear, :integer
    add_column :business_cards_orders, :value, :integer
    BusinessCardsOrder.all.delete_all
  end
  def down
    add_column :business_cards_orders, :front, :integer
    add_column :business_cards_orders, :rear, :integer
    remove_column :business_cards_orders, :value, :integer
    rename_table :business_cards_orders, :business_cards
  end
end
