class CreatePaymentTypesShops < ActiveRecord::Migration
  def change
    create_table :payment_types_shops, id: false do |t|
      t.belongs_to :payment_type, index: true
      t.belongs_to :shop, index: true
    end
  end
end
