class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string   :city
      t.string   :street
      t.string   :first_name
      t.string   :last_name
      t.string   :place
      t.string   :house_number
      t.string   :occupation
      t.string   :postal_code
      t.string   :country
      t.date     :birth_date
      t.boolean  :show_to_public, default: false
      t.string   :area_code
      t.string   :phone
      t.string   :mobile
      t.boolean  :show_phone_to_public, default: false
    end
  end
end
