class RemoveUnnecessaryColumnsFromPurchase < ActiveRecord::Migration
  def up
    remove_columns :purchases, :use_shipping_address_for_payment_address,
                               :address_for_all_items,
                               :pickup
  end
  def down
    add_column :purchases,
        :use_shipping_address_for_payment_address,
        :boolean,
        default: false

    add_column :purchases,
        :address_for_all_items,
        :boolean,
        default: false

    add_column :purchases,
        :pickup,
        :boolean,
        default: false
  end
end
