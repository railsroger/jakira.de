class AddRatingsToAllArticlesOrders < ActiveRecord::Migration
  def change
    ArticlesOrder.delete_all
    Rating.delete_all
    ArticlesOrder.all.each do |articles_order|
      articles_order.create_rating_from_seller
      articles_order.create_rating_from_buyer
    end
  end
end
