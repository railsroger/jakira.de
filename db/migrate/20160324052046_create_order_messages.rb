class CreateOrderMessages < ActiveRecord::Migration
  def change
    create_table :order_messages do |t|
      t.text       :text
      t.belongs_to :articles_order
      t.belongs_to :user
      t.datetime   :created_at
    end
  end
end
