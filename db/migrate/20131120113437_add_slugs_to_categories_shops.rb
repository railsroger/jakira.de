class AddSlugsToCategoriesShops < ActiveRecord::Migration
  def change
    add_index :categories, :slug, unique: true
    add_index :shops, :slug, unique: true
  end
end
