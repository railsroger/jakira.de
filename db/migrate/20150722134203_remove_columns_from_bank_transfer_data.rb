class RemoveColumnsFromBankTransferData < ActiveRecord::Migration
  def change
  	remove_column :bank_transfer_data, :bank_transfer
  	remove_column :bank_transfer_data, :created_at
  	remove_column :bank_transfer_data, :updated_at
  end
end
