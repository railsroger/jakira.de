class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string     :name
      t.string     :company_type
      t.string     :tax_number
      t.integer    :user_id
      t.string     :slug
      t.integer    :address_id
      t.string     :subdomain
      t.text       :imprint
      t.text       :terms
      t.integer    :articles_type, default: 0
      t.boolean    :online,                       default: false
      t.text       :revocation_physical_products
      t.text       :revocation_services
      t.text       :revocation_digital_content
      t.text       :about_me
      t.text       :about_my_shop
      t.integer    :order_confirmation, default: 0
      t.money      :monthly_charge, amount:     { default: 1490 }
      t.boolean    :use_personal_monthly_charge,  default: false
      t.integer    :main_picture_id
      t.boolean    :deleted, default: false
      t.integer    :package
      t.timestamps
    end
  end
end
