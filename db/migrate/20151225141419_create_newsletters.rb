class CreateNewsletters < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.text    :text
      t.integer :receivers
      t.string  :subject
      t.boolean :template
      t.boolean :delivery_date_checked
      t.string  :template_name
      t.integer :interval
      t.date    :delivery_date
      t.date    :first_execution
      t.date    :last_execution
      t.date    :exposing_from
      t.date    :exposing_to
      t.timestamps
    end
  end
end
