class Change2Ratings < ActiveRecord::Migration
  def change
  	remove_column :ratings, :order_id, :integer
  	remove_column :ratings, :user_id, :integer
  	add_column    :ratings, :sender_id, :integer
  	add_column    :ratings, :receiver_id, :integer
  	rename_column :ratings, :to, :from
  end
end
