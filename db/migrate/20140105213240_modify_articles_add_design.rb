class ModifyArticlesAddDesign < ActiveRecord::Migration
  def change
    add_column :articles, :allow_design, :boolean
  end
end