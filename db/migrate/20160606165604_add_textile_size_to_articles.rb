class AddTextileSizeToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :textile_size, :integer
  end
end
