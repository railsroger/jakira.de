class AddMultipleChoicesToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :multiple_choices, :boolean, default: false
  end
end