class AddUploaderIdToImage < ActiveRecord::Migration
  def up
    add_column :images, :uploader_id, :integer
    Image.all.each do |image|
      if image.parent.present? &&
        %w(Shop Article).include?(image.parent.class.name)
        image.update(uploader_id: image.parent.user_id)
      end
    end
  end

  def down
    remove_column :images, :uploader_id, :integer
  end
end
