class AddUserAmountToVouchers < ActiveRecord::Migration
  def change
    add_column :vouchers, :used_amount, :integer, default: 0
    add_column :vouchers, :code, :string
  end
end
