class RemoveUnnecessaryColumnsFromOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :allow_partial_approval, :boolean
    remove_column :orders, :is_paid, :boolean
    remove_column :orders, :is_sent, :boolean
    remove_column :orders, :is_received, :boolean
    remove_column :orders, :shipping_method_id, :integer
    remove_column :shopping_baskets, :shipping_method_id, :integer
    remove_column :shopping_baskets, :payment_type_id, :integer
    remove_column :purchases, :state, :string
  end
end
