class AddVoucherIdToPaypalTransaction < ActiveRecord::Migration
  def change
    add_column :paypal_transactions, :voucher_id, :integer
  end
end
