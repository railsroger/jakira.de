class ChangeDefaultValueOfArticlesViewTypeForUsers < ActiveRecord::Migration
  def change
    change_column :users, :articles_view_type, :integer, default: 1
  end
end
