class AddTranslationsToSuitableFor < ActiveRecord::Migration
  def self.up
    SuitableFor.create_translation_table!({
      name: :string
    }, {
      migrate_data: true
    })
  end

  def self.down
    SuitableFor.drop_translation_table! migrate_data: true
  end
end
