class AddRemainingAmountToVoucher < ActiveRecord::Migration
  def change
    remove_column :vouchers, :used_amount, :integer, default: 0
    add_money :vouchers, :used_amount
    add_money :vouchers, :remaining_amount
  end
end
