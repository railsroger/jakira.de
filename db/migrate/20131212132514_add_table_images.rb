class AddTableImages < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.integer :article_id
      t.string  :image
      t.string  :document
      t.integer :shop_id
      t.string  :type
      t.string  :description
      t.string  :name
      t.timestamps
    end
  end
end
