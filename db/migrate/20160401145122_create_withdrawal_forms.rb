class CreateWithdrawalForms < ActiveRecord::Migration
  def change
    create_table :withdrawal_forms do |t|
      t.string     :withdrawal_form
      t.belongs_to :shop
    end
  end
end
