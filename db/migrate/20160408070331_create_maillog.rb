class CreateMaillog < ActiveRecord::Migration
  def change
    create_table :maillogs do |t|
      t.belongs_to   :user
      t.string       :email
      t.string       :subject
      t.datetime     :sent_at
    end
  end
end
