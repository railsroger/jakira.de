class RemoveArticleIdFromPaypalEmailsAndAddPaypalEmailableIdAndAddPaypalEmailableType < ActiveRecord::Migration
  def change
  	remove_column :bank_transfer_data, :shop_id
  	remove_column :bank_transfer_data, :article_id
  	add_column :bank_transfer_data, :bank_transfer_datable_id, :integer
  	add_column :bank_transfer_data, :bank_transfer_datable_type, :string
  end
end
