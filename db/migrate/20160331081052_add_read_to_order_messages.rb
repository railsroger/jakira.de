class AddReadToOrderMessages < ActiveRecord::Migration
  def change
    add_column :order_messages, :read, :boolean, default: false
  end
end
