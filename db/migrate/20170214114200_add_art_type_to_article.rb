class AddArtTypeToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :art_type, :integer
    Article.all.each do |a|
      a.update(art_type: rand(3))
    end
  end
end
