class AddBusinessCardsThumbsToAllShopOwners < ActiveRecord::Migration
  def up
    User.sellers.each do |seller|
      seller.touch
    end
  end
  def down
    FileUtils.rm_rf 'public/business_cards_thumbs/'
  end
end
