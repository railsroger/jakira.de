class AddFiltersToArticle < ActiveRecord::Migration
  def change
    create_table :article_filters do |t|
      t.integer :article_id
      t.string  :name
      t.string  :value

      t.timestamps
    end
  end
end
