class ChangeDocumentsArticlesOrder < ActiveRecord::Migration
  def change
    rename_column :documents, :article_order_id, :articles_order_id
  end
end
