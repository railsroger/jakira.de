class ChangeCompletelyDeletedInArticles < ActiveRecord::Migration
  def up
    change_column :articles, :completely_deleted, :boolean, default: false
  end

  def down
    change_column :articles, :completely_deleted, :boolean
  end
end
