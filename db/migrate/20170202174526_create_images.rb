class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :kind
      t.integer :parent_id
      t.string  :image
    end
  end
end
