class CreateBusinessCardData < ActiveRecord::Migration
  def change
    create_table :business_card_details do |t|
      t.string :price
      t.string :quantity
      t.string :format
      t.string :extent 
      t.string :dyeing
      t.string :paper
      t.string :gram_matur
      t.string :printing_process
      t.string :paintwork
      t.timestamps
    end
  end
end
