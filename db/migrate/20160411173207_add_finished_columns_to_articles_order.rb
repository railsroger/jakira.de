class AddFinishedColumnsToArticlesOrder < ActiveRecord::Migration
  def change
    remove_column :articles_orders, :finished
    add_column :articles_orders, :finished_by_seller, :boolean, default: false
    add_column :articles_orders, :finished_by_buyer,  :boolean, default: false
  end
end
