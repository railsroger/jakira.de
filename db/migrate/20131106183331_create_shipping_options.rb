class CreateShippingOptions < ActiveRecord::Migration
  def change
    create_table   :shipping_methods do |t|
      t.string     :name
      t.money      :price
      t.boolean    :checked, default: false
      t.integer    :shipping_methodable_id
      t.string     :shipping_methodable_type
      t.integer    :duration_from
      t.integer    :duration_to
      t.timestamps
    end
  end
end
