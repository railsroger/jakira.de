class AddRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :role, :integer, default: 1
    User.all.each do |user|
      user.role =
      if user.admin
        'admin'
      elsif user.guest
        'guest'
      else
        'regular'
      end
      user.save!(validate: false)
    end
  end
end
