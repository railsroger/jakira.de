class AddVoucherAcceptedAtToArticlesOrders < ActiveRecord::Migration
  def change
    add_column :articles_orders, :voucher_accepted_at, :datetime
    ArticlesOrder.all.each do |order|
      order.update(voucher_accepted_at: order.voucher.updated_at) if order.voucher.present?
    end
  end
end
