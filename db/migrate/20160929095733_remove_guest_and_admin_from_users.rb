class RemoveGuestAndAdminFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :guest
    remove_column :users, :admin
  end
  def down
    add_column :users, :guest, :boolean, default: false
    add_column :users, :admin, :boolean, default: false
    User.all.each do |user|
      case user.role
      when 'admin'
        user.admin = true
      when 'guest'
        user.guest = true
      end
      user.save!(validate: false)
    end
  end
end
