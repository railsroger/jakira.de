class RemoveAllPurchasesOrdersArticlesOrdersAndRatings < ActiveRecord::Migration
  def change
    Purchase.delete_all
    Order.delete_all
    ArticlesOrder.delete_all
    Article.ordered.delete_all
    Rating.delete_all
  end
end
