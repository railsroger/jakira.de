class RemovePickupFromArticlesOrder < ActiveRecord::Migration
  def change
    ArticlesOrder.where(pickup: true).each do |articles_order|
      pickup = articles_order.article.shipping_methods.find_by(name: "pickup")
      if pickup.present?
        articles_order.update(shipping_method_id: pickup.id)
      else
        articles_order.destroy
      end
    end
    remove_column :articles_orders, :pickup, :boolean
  end
end
