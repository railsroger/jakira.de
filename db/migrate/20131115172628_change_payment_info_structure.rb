class ChangePaymentInfoStructure < ActiveRecord::Migration
  def change
    create_table :payment_infos do |t|
      t.integer   :article_id
      t.string    :email
      t.string    :name
      t.string    :account
      t.string    :bank_code
    end
  end
end
