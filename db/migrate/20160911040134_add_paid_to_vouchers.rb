class AddPaidToVouchers < ActiveRecord::Migration
  def up
    add_column :vouchers, :paid, :boolean, default: false
    Voucher.all.each do |voucher|
      if voucher.paypal_transactions.present? && voucher.paypal_transactions.last.complete?
        voucher.paid = true
        voucher.save
      end
    end
  end
  def down
    remove_column :vouchers, :paid
  end
end
