class ChangeMessageReceivedNotificationInUsers < ActiveRecord::Migration
  def change
    change_column :users, :message_received_notification, :boolean, default: true
  end
end
