class RemovePurchaseNotificationEmailIsSentFromPurchases < ActiveRecord::Migration
  def change
    remove_column :orders, :purchase_notification_email_is_sent
  end
end
