class CreateSuitableFor < ActiveRecord::Migration
  def change
    create_table :suitable_fors do |t|
      t.string  :name
    end
    create_table :articles_suitable_fors, id: false do |t|
      t.belongs_to :article, index: true
      t.belongs_to :suitable_for, index: true
    end
  end
end
