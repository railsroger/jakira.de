class AddAddressIdToVouchers < ActiveRecord::Migration
  def change
    add_column :vouchers, :address_id, :integer
    add_column :vouchers, :payment_type, :integer
  end
end
