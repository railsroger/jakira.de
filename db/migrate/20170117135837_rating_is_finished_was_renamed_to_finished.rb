class RatingIsFinishedWasRenamedToFinished < ActiveRecord::Migration
  def up
    rename_column :ratings, :is_finished, :finished
  end
  def down
    rename_column :ratings, :finished, :is_finished
  end
end
