class AddPickupToArticlesOrder < ActiveRecord::Migration
  def change
    add_column :articles_orders, :pickup, :boolean, default: false
  end
end
