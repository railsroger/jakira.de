class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.integer     :worth
      t.string      :from
      t.string      :for
      t.text        :message_text
      t.string      :email
      t.timestamps
    end
  end
end
