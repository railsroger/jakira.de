class CreateOrderedArticles < ActiveRecord::Migration
  def up
    add_column :articles_orders, :original_article_id, :integer
    ArticlesOrder.all.each do |articles_order|
      articles_order.original_article_id = articles_order.article.id
      articles_order.article_id = articles_order.article.create_ordered_article.id
      articles_order.save
    end
  end

  def down
    ArticlesOrder.all.each do |articles_order|
      articles_order.article_id = articles_order.orignal_article_id
      articles_order.save
    end
    remove_column :articles_orders, :original_article_id, :integer
  end
end
