class CreateAssignmentsTable < ActiveRecord::Migration
  def change
    create_table :assignments_tables do |t|
      t.string  :title
      t.string  :description
      t.integer :buyer_id
      t.integer :seller_id
      t.string  :is_approved

      t.money :price

      t.timestamps
    end
  end
end
