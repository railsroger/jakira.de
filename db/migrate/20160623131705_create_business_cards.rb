class CreateBusinessCards < ActiveRecord::Migration
  def change
    create_table :business_cards do |t|
      t.belongs_to :user
      t.integer    :front
      t.integer    :rear
      t.integer    :status
      t.timestamps
    end
  end
end
