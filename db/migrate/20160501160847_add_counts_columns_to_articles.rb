class AddCountsColumnsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :views_count, :integer, default: 0
    add_column :articles, :added_to_favorites_count, :integer, default: 0
    add_column :articles, :added_to_cart_count, :integer, default: 0
  end
end
