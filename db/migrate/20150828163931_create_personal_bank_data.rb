class CreatePersonalBankData < ActiveRecord::Migration
  def change
    create_table :personal_bank_data do |t|
      t.string  "account_holder"
      t.string  "bank_name"
      t.integer "shop_id"
      t.string  "account_number"
      t.string  "bank_code"
      t.string  "iban"
      t.string  "swft_bic"
    end
  end
end