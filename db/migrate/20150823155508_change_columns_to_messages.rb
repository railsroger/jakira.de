class ChangeColumnsToMessages < ActiveRecord::Migration
  def change
    remove_column :messages, :sender_deleted
    remove_column :messages, :container
    add_column :messages, :recipient_status, :string
    add_column :messages, :sender_status, :string
  end
end
