class AddShopIdToBankTransferData < ActiveRecord::Migration
  def change
  	add_column :bank_transfer_data, :shop_id, :integer
  end
end
