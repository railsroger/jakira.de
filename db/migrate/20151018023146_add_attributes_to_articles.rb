class AddAttributesToArticles < ActiveRecord::Migration
  def change
    create_table :material_textiles do |t|
      t.string  :name
      t.string  :percent
      t.integer :article_id
    end

    create_table :colors do |t|
      t.string  :name
    end

    create_table :articles_colors, id: false do |t|
      t.belongs_to :article, index: true
      t.belongs_to :color, index: true
    end
  end
end