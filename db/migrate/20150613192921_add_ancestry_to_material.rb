class AddAncestryToMaterial < ActiveRecord::Migration
  def change
  	add_column :materials, :ancestry, :string
    add_index  :materials, :ancestry
  end
end