# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
# You can also reset the database by rake db:reset.

# Data will be created by FactoryGirl
# require 'factory_girl_rails'
# include FactoryGirl::Syntax::Methods

#
# Categories
#
root_category = Category.find_or_create_by(name: "root")

%w(Metall Holz Papier Glas Keramik Textil Malerei Graphik
   Bildhauerei).each do |category_name|
  root_category.children.find_or_create_by(name: category_name)
end

%w(Interieur Schmiedekunst Schmuck Skulptur Uhren Objekt).each do |category_name|
  category = Category.find_by(name: "Metall")
                     .children
                     .find_or_create_by(name: category_name)
  case category_name
  when "Interieur"
    %w(Besteck Gefäß Möbel).each do |category_name|
      category.children.find_or_create_by(name: category_name)
    end
  when "Schmiedekunst"
    %w(Gefäß Metallgestaltung Skulptur).each do |category_name|
      category.children.find_or_create_by(name: category_name)
    end
  when "Schmuck"
    %w(Armschmuck Ansteckschmuck Halsschmuck Handschmuck Herrenschmuck
       Ohrschmuck).each do |category_name|
      category.children.find_or_create_by(name: category_name)
    end
  end
end

%w(Gefäß Möbel Objekt Skulptur).each do |category_name|
  Category.find_by(name: "Holz").children.find_or_create_by(name: category_name)
end

%w(Bücher Fotoalben Kästen Objekte Schmuck).each do |category_name|
  Category.find_by(name: "Papier").children.find_or_create_by(name: category_name)
end

%w(Gefäß Lampen Objekt Sklptur).each do |category_name|
  Category.find_by(name: "Glas").children.find_or_create_by(name: category_name)
end

%w(Gefäß Geschirr Lampen Objekt Skulptur).each do |category_name|
  Category.find_by(name: "Keramik").children.find_or_create_by(name: category_name)
end

%w(Kleidung Interieur Leder Objekt).each do |category_name|
  category = Category.find_by(name: "Textil")
                     .children
                     .find_or_create_by(name: category_name)
  case category_name
  when "Kleidung"
    %w(Hüte Kleider Schals).each do |category_name|
  category.children.find_or_create_by(name: category_name)
    end
  when "Interieur"
    %w(Bildweberei Decken Teppich).each do |category_name|
      category.children.find_or_create_by(name: category_name)
    end
  when "Leder"
    %w(Bekleidung Schuhe Taschen Gürtel).each do |category_name|
      category.children.find_or_create_by(name: category_name)
    end
  end
end

%w(Malkunst).each do |category_name|
  Category.find_by(name: "Malerei").children.find_or_create_by(name: category_name)
end

%w(Aquarelle Öl Acryl Tempera Pastell Zeichnung Mischtechnik).each do |category_name|
  Category.find_by(name: "Malkunst").children.find_or_create_by(name: category_name)
end

%w(Fotografie Holzschnitt Linolschnitt Lithographie Radierung).each do |category_name|
  Category.find_by(name: "Graphik").children.find_or_create_by(name: category_name)
end
    
%w(Plastiken Skulpturen Relief).each do |category_name|
  Category.find_by(name: "Bildhauerei").children.find_or_create_by(name: category_name)
end

%w(Metall Leder Holz Ton Papier Glas Filz Perlmutt Porzellan 
  Keramik Perlen Stein Kunststoff Textil Sonstiges).each do |material_name|
  Material.find_or_create_by name: material_name
end

%w(Gold
  Silber
  Bronze
  Stahl
  Aluminium
  Eisen
  Kupfer
  Platin
  Zink
  Zinn
  Palladium
  Chrom
  Rhodium
  Titan
  Wolfram
  Sonstiges).each do |material_name|
  Material.find_by(name: "Metall").children.find_or_create_by name: material_name
end

%w(Ahorn
  Azobé
  Balau
  Balsa
  Birke
  Buche 
  Douglasie
  Ebenholz
  Eibe
  Eiche
  Erle
  Esche
  Espe
  Fichte
  Kiefer
  Kirsche
  Lärche
  Linde
  Mahagoni
  Pappel
  Platane
  Robinie
  Tanne
  Teak
  Zypresse
  Sonstiges).each do |material_name|
  Material.find_by(name: "Holz").children.find_or_create_by name: material_name
end

if BusinessCardDetails.first.nil?
  BusinessCardDetails.create(price: '14,50€',
    quantity: '250 Stück',
    format: '8,5 x 5,4 cm',
    extent: '2-seitig',
    dyeing: '5/5 farbig SKALA + Matt',
    paper: 'Beidseitig gestrichen in hochwertiger Ausführung',
    gram_matur: '350g',
    printing_process: 'Offsetdruck',
    paintwork: 'Keine')
end


%w(wire paypal last_name cash credit_card).each do |name|
  PaymentType.find_or_create_by(name: name)
end

if Color.all.empty? || GemColor.all.empty?
  colors = [{en: "yellow",         de: "gelb"},
            {en: "beige",          de: "beige"},
            {en: "brown",          de: "braun"},
            {en: "orange",         de: "orange"},
            {en: "red",            de: "rot"},
            {en: "dark red",       de: "dunkelrot"},
            {en: "rosa",           de: "rosa"},
            {en: "pink",           de: "pink"},
            {en: "lila",           de: "lila"},
            {en: "purple",         de: "violett"},
            {en: "blue",           de: "blau"},
            {en: "light blue",     de: "hellblau"},
            {en: "dark blue",      de: "dunkelblau"},
            {en: "mahogany brown", de: "mahagonibraun"},
            {en: "turquoise",      de: "türkis"},
            {en: "lime",           de: "hellgrün"},
            {en: "green",          de: "grün"},
            {en: "white",          de: "weiß"},
            {en: "gray",           de: "grau"},
            {en: "light gray",     de: "hellgrau"},
            {en: "dark gray",      de: "dunkelgrau"},
            {en: "black",          de: "schwarz"},
            {en: "gold",           de: "gold"},
            {en: "silver",         de: "silber"},
            {en: "bronze",         de: "bronze"},
            {en: "colorful",       de: "bunt"}]
  colors.each_index do |i|
    I18n.locale = 'en'
    color = Color.new name: colors[i][:en]
    gem_color = GemColor.new name: colors[i][:en]
    I18n.locale = 'de'
    color.name = colors[i][:de]
    gem_color.name = colors[i][:de]
    color.save
    gem_color.save
  end
end


if SuitableFor.all.empty?
  names = [{en: "Men",    de: "Männer"},
           {en: "Women",  de: "Frauen"},
           {en: "Girls",  de: "Mädchen"},
           {en: "Boys",   de: "Jungen"},
           {en: "Babies", de: "Babys"}]
  names.each_index do |i|
    I18n.locale = 'en'
    suitable_for = SuitableFor.new name: names[i][:en]
    I18n.locale = 'de'
    suitable_for.name = names[i][:de]
    suitable_for.save
  end
end


if User.where(role: User.roles[:admin]).empty?
  admin = User.new(role: "admin",
                   nickname: "admin",
                   email: "admin@jakira.de",
                   password: "wdu2014",
                   password_confirmation: "wdu2014")
  admin.build_address(first_name: "Main", last_name: "Admin")
  admin.skip_confirmation!
  admin.save!(validate: false)
end

main_voucher_data = MainVoucherData.first.nil? ? MainVoucherData.create : MainVoucherData.first
main_voucher_data.create_bank_transfer_data if main_voucher_data.bank_transfer_data.nil?
main_voucher_data.create_paypal_data if main_voucher_data.paypal_data.nil?

# if MonthlyCharge.where(is_for_all: true).empty?
#   MonthlyCharge.create(is_for_all: true, price: 14.9)
# end
# #
# # Users
# #
# unless shop_user = User.find_by(email: "shop@example.com")
#   shop_user = create(:user, email: "shop@example.com", first_name: "Max", last_name: "Häkeltalent") do |user|
#     user.address = Address.new({
#       country: "Germany",
#       city: "Berlin",
#       postal_code: "10000",
#       street: "Friedrichstr. 100"
#     })
#     user.sign_in_count = 1
#     user.save!
#   end
#   Subscription.find_by(user_id: shop_user.id) || create(:subscription, user: shop_user, state: 'active')
# end

# unless User.find_by(email: "buyer@example.com")
#   create(:user, email: "buyer@example.com", first_name: "Tina", last_name: "Kaufrausch") do |user|
#     user.address = Address.new({
#       country: "Germany",
#       city: "Köln",
#       postal_code: "40000",
#       street: "Blumenthalstr. 20"
#     })
#     user.sign_in_count = 1
#     user.save!
#   end
# end

# if ENV['FAKER']
#   10.times do |n|
#     email = "buyer-#{sprintf('%03d', n+1)}@example.com"
#     User.find_by(email: email) || create(:user, email: email)
#   end
# end

# #
# # Shops
# #
# Shop.find_by(shop_name: "Häkelparadies") || create(:shop, shop_name: "Häkelparadies", user: shop_user)

# unless Article.find_by(user_id: shop_user.id, title: "Topflappen")
#   create(:finished_article, user: shop_user, category: category_handarbeit, title: "Topflappen", price_cents: 12.5*100)
# end

# unless Article.find_by(user_id: shop_user.id, title: "Mütze")
#   create(:finished_article, user: shop_user, category: category_handarbeit, title: "Mütze", price_cents: 31.9*100)
# end

# unless Article.find_by(user_id: shop_user.id, title: "Deckchen")
#   create(:finished_article, user: shop_user, category: category_handarbeit, title: "Deckchen", price_cents: 12.5*100)
# end

# unless Article.find_by(user_id: shop_user.id, title: "Schal")
#   create(:finished_article, user: shop_user, category: category_handarbeit, title: "Schal", price_cents: 28.9*100)
# end

# unless Article.find_by(user_id: shop_user.id, title: "Decke")
#   create(:finished_article, user: shop_user, category: category_handarbeit, title: "Decke", price_cents: 35.9*100)
# end

# if ENV['FAKER']
#   10.times do |n|
#     email = "shop-#{sprintf('%03d', n+1)}@example.com"
#     name = "Shop (#{sprintf('%03d', n+1)})"
#     user = User.find_by(email: email) || create(:user, email: email)

#     shop = Shop.find_by(shop_name: name) || build(:shop, shop_name: name, created_at: DateTime.now - 1.day)
#     shop.user_id = user.id
#     shop.save

#     Subscription.find_by(user: user) || create(:subscription, user: user, state: 'active')

#     if shop.default_article.shipping_methods.empty?
#       default_article = create(:default_article)
#       shop.default_article = default_article
#       shop.save

#       2.times do
#         shipping_method = build(:shipping_method)
#         shipping_method.article_id = default_article.id
#         shipping_method.save
#       end
#     end

#     if user.articles.empty?
#       5.times do
#         article = build(:article)
#         article.user_id = user.id
#         article.save!

#         default_article.shipping_methods.each do |shipping_method|
#           new_shipping_method = shipping_method.dup
#           new_shipping_method.article_id = article.id
#           new_shipping_method.save
#         end

#         article.payment_selection = default_article.payment_selection.dup
#         article.state = 'finished'

#         article.save!
#       end
#     end
#   end
# end
