# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170214114200) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: true do |t|
    t.string  "city"
    t.string  "street"
    t.string  "first_name"
    t.string  "last_name"
    t.string  "place"
    t.string  "house_number"
    t.string  "occupation"
    t.string  "postal_code"
    t.string  "country"
    t.date    "birth_date"
    t.boolean "show_to_public",       default: false
    t.string  "area_code"
    t.string  "phone"
    t.string  "mobile"
    t.boolean "show_phone_to_public", default: false
    t.string  "company_name"
  end

  create_table "article_filters", force: true do |t|
    t.integer  "article_id"
    t.string   "name"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "articles", force: true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "description"
    t.integer  "category_id"
    t.integer  "price_cents",                  default: 0,     null: false
    t.string   "price_currency",               default: "EUR", null: false
    t.integer  "vat"
    t.string   "artist"
    t.string   "dispatch_delay"
    t.integer  "status",                       default: 0
    t.string   "made_in"
    t.boolean  "completely_deleted",           default: false
    t.integer  "article_type"
    t.string   "gems"
    t.string   "width"
    t.string   "length"
    t.string   "height"
    t.string   "weight"
    t.string   "weight_of_gem"
    t.string   "sku"
    t.boolean  "customizable",                 default: false
    t.string   "state"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "allow_design"
    t.boolean  "deleted",                      default: false
    t.text     "customizable_comment"
    t.integer  "order_confirmation",           default: 0
    t.integer  "views_count",                  default: 0
    t.integer  "added_to_favorites_count",     default: 0
    t.integer  "added_to_cart_count",          default: 0
    t.string   "template_name"
    t.integer  "main_picture_id"
    t.integer  "textile_size"
    t.string   "no_shipping_to_the_countries",                              array: true
    t.integer  "kind",                         default: 0
    t.string   "year"
    t.string   "motive"
    t.datetime "sold_at"
    t.integer  "art_type"
  end

  add_index "articles", ["slug"], name: "index_articles_on_slug", unique: true, using: :btree

  create_table "articles_categories", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "category_id"
  end

  add_index "articles_categories", ["article_id"], name: "index_articles_categories_on_article_id", using: :btree
  add_index "articles_categories", ["category_id"], name: "index_articles_categories_on_category_id", using: :btree

  create_table "articles_colors", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "color_id"
  end

  add_index "articles_colors", ["article_id"], name: "index_articles_colors_on_article_id", using: :btree
  add_index "articles_colors", ["color_id"], name: "index_articles_colors_on_color_id", using: :btree

  create_table "articles_gem_colors", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "gem_color_id"
  end

  add_index "articles_gem_colors", ["article_id"], name: "index_articles_gem_colors_on_article_id", using: :btree
  add_index "articles_gem_colors", ["gem_color_id"], name: "index_articles_gem_colors_on_gem_color_id", using: :btree

  create_table "articles_materials", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "material_id"
  end

  add_index "articles_materials", ["article_id"], name: "index_articles_materials_on_article_id", using: :btree
  add_index "articles_materials", ["material_id"], name: "index_articles_materials_on_material_id", using: :btree

  create_table "articles_orders", force: true do |t|
    t.integer  "article_id"
    t.integer  "order_id"
    t.integer  "shipping_method_id"
    t.integer  "payment_type_id"
    t.integer  "shipping_address_id"
    t.integer  "payment_address_id"
    t.integer  "status"
    t.integer  "invoice_id"
    t.datetime "message_sent_at"
    t.datetime "sent_at"
    t.datetime "paid_at"
    t.datetime "ordered_at"
    t.datetime "confirmed_at"
    t.text     "message_text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "amount",                                   default: 1
    t.boolean  "finished_by_seller",                       default: false
    t.boolean  "finished_by_buyer",                        default: false
    t.integer  "voucher_id"
    t.integer  "voucher_discount_cents",                   default: 0,     null: false
    t.string   "voucher_discount_currency",                default: "EUR", null: false
    t.integer  "original_article_id"
    t.datetime "voucher_accepted_at"
    t.boolean  "use_shipping_address_for_payment_address", default: false
    t.boolean  "canceled",                                 default: false
    t.datetime "canceled_at"
  end

  create_table "articles_payment_types", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "payment_type_id"
  end

  add_index "articles_payment_types", ["article_id"], name: "index_articles_payment_types_on_article_id", using: :btree
  add_index "articles_payment_types", ["payment_type_id"], name: "index_articles_payment_types_on_payment_type_id", using: :btree

  create_table "articles_suitable_fors", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "suitable_for_id"
  end

  add_index "articles_suitable_fors", ["article_id"], name: "index_articles_suitable_fors_on_article_id", using: :btree
  add_index "articles_suitable_fors", ["suitable_for_id"], name: "index_articles_suitable_fors_on_suitable_for_id", using: :btree

  create_table "assignments", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "buyer_id"
    t.integer  "seller_id"
    t.string   "is_approved_seller"
    t.integer  "price_cents",        default: 0,     null: false
    t.string   "price_currency",     default: "EUR", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state"
    t.string   "is_approved_buyer"
  end

  create_table "bank_transfer_data", force: true do |t|
    t.string  "account_holder"
    t.string  "bank_name"
    t.integer "bank_transfer_datable_id"
    t.string  "bank_transfer_datable_type"
    t.string  "account_number"
    t.string  "bank_code"
    t.string  "iban"
    t.string  "swft_bic"
  end

  create_table "business_card_detail_translations", force: true do |t|
    t.integer  "business_card_detail_id", null: false
    t.string   "locale",                  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "price"
    t.string   "quantity"
    t.string   "format"
    t.string   "extent"
    t.string   "dyeing"
    t.string   "paper"
    t.string   "gram_matur"
    t.string   "printing_process"
    t.string   "paintwork"
  end

  add_index "business_card_detail_translations", ["business_card_detail_id"], name: "index_a249e37d9c704b03c35f041f13bbe65539970b9b", using: :btree
  add_index "business_card_detail_translations", ["locale"], name: "index_business_card_detail_translations_on_locale", using: :btree

  create_table "business_card_details", force: true do |t|
    t.string   "price"
    t.string   "quantity"
    t.string   "format"
    t.string   "extent"
    t.string   "dyeing"
    t.string   "paper"
    t.string   "gram_matur"
    t.string   "printing_process"
    t.string   "paintwork"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "business_cards_orders", force: true do |t|
    t.integer  "user_id"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "value"
  end

  create_table "categories", force: true do |t|
    t.string  "name"
    t.string  "slug"
    t.string  "ancestry"
    t.boolean "multiple_choices", default: false
  end

  add_index "categories", ["ancestry"], name: "index_categories_on_ancestry", using: :btree
  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "category_translations", force: true do |t|
    t.integer  "category_id", null: false
    t.string   "locale",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "category_translations", ["category_id"], name: "index_category_translations_on_category_id", using: :btree
  add_index "category_translations", ["locale"], name: "index_category_translations_on_locale", using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "color_translations", force: true do |t|
    t.integer  "color_id",   null: false
    t.string   "locale",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "color_translations", ["color_id"], name: "index_color_translations_on_color_id", using: :btree
  add_index "color_translations", ["locale"], name: "index_color_translations_on_locale", using: :btree

  create_table "colors", force: true do |t|
    t.string "name"
  end

  create_table "documents", force: true do |t|
    t.integer  "article_id"
    t.string   "image"
    t.string   "document"
    t.integer  "shop_id"
    t.string   "type"
    t.string   "description"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "articles_order_id"
    t.integer  "assignment_id"
    t.integer  "user_id"
  end

  create_table "faq_question_translations", force: true do |t|
    t.integer  "faq_question_id", null: false
    t.string   "locale",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "question"
    t.text     "answer"
  end

  add_index "faq_question_translations", ["faq_question_id"], name: "index_faq_question_translations_on_faq_question_id", using: :btree
  add_index "faq_question_translations", ["locale"], name: "index_faq_question_translations_on_locale", using: :btree

  create_table "faq_questions", force: true do |t|
    t.string  "question"
    t.text    "answer"
    t.integer "faq_section_id"
  end

  create_table "faq_section_translations", force: true do |t|
    t.integer  "faq_section_id", null: false
    t.string   "locale",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "faq_section_translations", ["faq_section_id"], name: "index_faq_section_translations_on_faq_section_id", using: :btree
  add_index "faq_section_translations", ["locale"], name: "index_faq_section_translations_on_locale", using: :btree

  create_table "faq_sections", force: true do |t|
    t.string "name"
  end

  create_table "favorite_shops", force: true do |t|
    t.integer "shop_id"
    t.integer "user_id"
  end

  create_table "favorites", force: true do |t|
    t.integer  "article_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "gem_color_translations", force: true do |t|
    t.integer  "gem_color_id", null: false
    t.string   "locale",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "gem_color_translations", ["gem_color_id"], name: "index_gem_color_translations_on_gem_color_id", using: :btree
  add_index "gem_color_translations", ["locale"], name: "index_gem_color_translations_on_locale", using: :btree

  create_table "gem_colors", force: true do |t|
    t.string "name"
  end

  create_table "images", force: true do |t|
    t.integer "kind"
    t.integer "parent_id"
    t.string  "image"
    t.integer "uploader_id"
  end

  create_table "invoices", force: true do |t|
    t.integer  "status"
    t.string   "number"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "EUR", null: false
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "maillogs", force: true do |t|
    t.integer  "user_id"
    t.string   "email"
    t.string   "subject"
    t.datetime "sent_at"
  end

  create_table "main_voucher_data", force: true do |t|
    t.boolean "bank_transfer",      default: false
    t.boolean "paypal",             default: false
    t.boolean "paydirekt",          default: false
    t.string  "mandatory_approval"
    t.string  "voucher_terms"
    t.string  "cancellation"
    t.string  "imprint"
  end

  create_table "material_textiles", force: true do |t|
    t.string  "name"
    t.string  "percent"
    t.integer "article_id"
  end

  create_table "material_translations", force: true do |t|
    t.integer  "material_id", null: false
    t.string   "locale",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "material_translations", ["locale"], name: "index_material_translations_on_locale", using: :btree
  add_index "material_translations", ["material_id"], name: "index_material_translations_on_material_id", using: :btree

  create_table "materials", force: true do |t|
    t.string "name"
    t.string "ancestry"
  end

  add_index "materials", ["ancestry"], name: "index_materials_on_ancestry", using: :btree

  create_table "messages", force: true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.string   "subject"
    t.text     "body"
    t.datetime "read_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file"
    t.string   "recipient_status"
    t.string   "sender_status"
    t.datetime "sent_at"
  end

  create_table "newsletters", force: true do |t|
    t.text     "text"
    t.integer  "receivers"
    t.string   "subject"
    t.boolean  "template"
    t.boolean  "delivery_date_checked"
    t.string   "template_name"
    t.integer  "interval"
    t.date     "delivery_date"
    t.date     "first_execution"
    t.date     "last_execution"
    t.date     "exposing_from"
    t.date     "exposing_to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "order_messages", force: true do |t|
    t.text     "text"
    t.integer  "articles_order_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.boolean  "read",              default: false
  end

  create_table "orders", force: true do |t|
    t.integer  "purchase_id"
    t.integer  "seller_address_id"
    t.integer  "seller_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_infos", force: true do |t|
    t.integer "article_id"
    t.string  "email"
    t.string  "name"
    t.string  "account"
    t.string  "bank_code"
  end

  create_table "payment_selections", force: true do |t|
    t.integer  "article_id"
    t.string   "type"
    t.boolean  "payment_wire"
    t.boolean  "payment_paypal"
    t.boolean  "payment_advance"
    t.boolean  "payment_cash"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_types", force: true do |t|
    t.string "name"
  end

  create_table "payment_types_shops", id: false, force: true do |t|
    t.integer "payment_type_id"
    t.integer "shop_id"
  end

  add_index "payment_types_shops", ["payment_type_id"], name: "index_payment_types_shops_on_payment_type_id", using: :btree
  add_index "payment_types_shops", ["shop_id"], name: "index_payment_types_shops_on_shop_id", using: :btree

  create_table "paypal_data", force: true do |t|
    t.string  "client_id"
    t.string  "client_secret"
    t.integer "paypal_datable_id"
    t.string  "paypal_datable_type"
  end

  create_table "paypal_transactions", force: true do |t|
    t.integer "articles_order_id"
    t.string  "hash_value"
    t.string  "payment_id"
    t.boolean "complete",          default: false
    t.integer "voucher_id"
  end

  create_table "personal_bank_data", force: true do |t|
    t.string  "account_holder"
    t.string  "bank_name"
    t.integer "shop_id"
    t.string  "account_number"
    t.string  "bank_code"
    t.string  "iban"
    t.string  "swft_bic"
  end

  create_table "purchases", force: true do |t|
    t.integer  "user_id"
    t.integer  "shipping_address_id"
    t.integer  "payment_address_id"
    t.datetime "confirmed_at"
    t.boolean  "confirmed",           default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ratings", force: true do |t|
    t.integer  "score"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "articles_order_id"
    t.boolean  "finished",          default: false
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.integer  "from"
  end

  create_table "shipping_methods", force: true do |t|
    t.string   "name"
    t.integer  "price_cents",              default: 0,     null: false
    t.string   "price_currency",           default: "EUR", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "checked",                  default: false
    t.integer  "shipping_methodable_id"
    t.string   "shipping_methodable_type"
    t.integer  "duration_from"
    t.integer  "duration_to"
  end

  create_table "shipping_readinesses", force: true do |t|
    t.integer "readiness"
    t.integer "from"
    t.integer "to"
    t.integer "article_id"
    t.integer "shop_id"
  end

  create_table "shopping_baskets", force: true do |t|
    t.integer  "article_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "article_amount",  default: 1
    t.text     "message_text"
    t.datetime "message_sent_at"
  end

  create_table "shops", force: true do |t|
    t.string   "name"
    t.string   "company_type"
    t.string   "tax_number"
    t.integer  "user_id"
    t.string   "slug"
    t.integer  "address_id"
    t.string   "subdomain"
    t.text     "imprint"
    t.text     "terms"
    t.boolean  "online",                       default: false
    t.text     "revocation_physical_products"
    t.text     "revocation_services"
    t.text     "revocation_digital_content"
    t.text     "about_me"
    t.text     "about_my_shop"
    t.integer  "monthly_charge_cents",         default: 1490,  null: false
    t.string   "monthly_charge_currency",      default: "EUR", null: false
    t.boolean  "use_personal_monthly_charge",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",                      default: false
    t.integer  "order_confirmation",           default: 0
    t.integer  "articles_type",                default: 0
    t.integer  "package"
    t.string   "no_shipping_to_the_countries",                              array: true
  end

  add_index "shops", ["slug"], name: "index_shops_on_slug", unique: true, using: :btree

  create_table "subscriptions", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "activated_at"
    t.integer  "duration"
    t.string   "state"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "EUR", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "suitable_for_translations", force: true do |t|
    t.integer  "suitable_for_id", null: false
    t.string   "locale",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "suitable_for_translations", ["locale"], name: "index_suitable_for_translations_on_locale", using: :btree
  add_index "suitable_for_translations", ["suitable_for_id"], name: "index_suitable_for_translations_on_suitable_for_id", using: :btree

  create_table "suitable_fors", force: true do |t|
    t.string "name"
  end

  create_table "users", force: true do |t|
    t.string   "email",                             default: "",    null: false
    t.string   "encrypted_password",                default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                   default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "title"
    t.string   "nickname"
    t.integer  "articles_view_type",                default: 1
    t.integer  "address_id"
    t.boolean  "wizard_completed",                  default: false
    t.boolean  "message_received_notification",     default: true
    t.boolean  "rated_notification",                default: false
    t.boolean  "my_favorites_notification",         default: false
    t.boolean  "monthly_letters_notification",      default: false
    t.integer  "membership_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "last_url"
    t.boolean  "deleted",                           default: false
    t.string   "provider"
    t.string   "uid"
    t.integer  "role",                              default: 1
    t.string   "type"
    t.integer  "art_association_id"
    t.integer  "art_association_membership_number"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "vouchers", force: true do |t|
    t.string   "from"
    t.string   "for"
    t.text     "message_text"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.integer  "value_cents",               default: 0,     null: false
    t.string   "value_currency",            default: "EUR", null: false
    t.integer  "used_amount_cents",         default: 0,     null: false
    t.string   "used_amount_currency",      default: "EUR", null: false
    t.integer  "remaining_amount_cents",    default: 0,     null: false
    t.string   "remaining_amount_currency", default: "EUR", null: false
    t.integer  "address_id"
    t.integer  "payment_type"
    t.string   "number"
    t.boolean  "paid",                      default: false
    t.integer  "status"
    t.string   "sent_on",                                                array: true
    t.datetime "paid_on"
  end

  create_table "withdrawal_forms", force: true do |t|
    t.integer "shop_id"
    t.string  "file"
  end

end
