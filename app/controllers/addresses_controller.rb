class AddressesController < ApplicationController
  load_and_authorize_resource

  before_action :set_address, only: [:update_purchase_address]

  def load_country_code
    @code = params[:code]
    respond_to do |format|
      format.js
    end
  end

  def update_purchase_address
    if @address.update(purchase_address_params)
      flash.now.notice = "Successfully!"
    else
      flash.now.alert = t '.error'
    end
    @errors = @address.errors
    @purchase = @address.articles_order.purchase
  end

  private

  def set_address
    @address = Address.find(params[:id])
  end

  def purchase_address_params
    params.require(:address).permit(:first_name,
                                    :last_name,
                                    :street,
                                    :company_name,
                                    :house_number,
                                    :postal_code,
                                    :place,
                                    :country)
  end

end