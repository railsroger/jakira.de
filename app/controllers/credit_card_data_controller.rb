class CreditCardDataController < ApplicationController
  skip_authorization_check

  require 'paypal-sdk-rest'
  include PayPal::SDK::REST
  def create
    @credit_card_data = CreditCardData.new(params[:credit_card_data])
    @articles_order = ArticlesOrder.find(session[:articles_order_id])
    respond_to do |format|
      if @credit_card_data.valid?
      	format.html { }
	    PayPal::SDK.configure({
	      mode: "sandbox",
	      client_id: @articles_order.article.paypal_data.client_id,
	      client_secret: @articles_order.article.paypal_data.client_secret })

	    @payment = Payment.new({
	      intent: "sale",
	      payer: {
	        payment_method:"credit_card",
	        funding_instruments: [{
	          credit_card: {
	            type: @credit_card_data.type,
	            number: @credit_card_data.number,
	            expire_month: @credit_card_data.expire_month,
	            expire_year: @credit_card_data.expire_year,
	            first_name: @credit_card_data.first_name,
	            last_name: @credit_card_data.last_name }}]},
	      transactions: [ {
	        amount: {
	          total: @articles_order.article.price_cents/100 + @articles_order.shipping_method.price_cents/100,
	          currency: "USD" },
	        description: @articles_order.article.title}]})

	    @payment.create

	    session[:paypal_hash] = Digest::MD5.hexdigest(@payment.id)

	    PaypalTransaction.create(articles_order_id: @articles_order.id,
	                             payment_id: @payment.id,
	                             hash_value: session[:paypal_hash])

	    redirect_to purchase_steps_url(@articles_order.order.purchase) + "/purchase?approved=true"
      else
        format.html { render template: 'articles_orders/credit_card_payment' }
        format.json { render json: @credit_card_data.errors, status: :unprocessable_entity }
      end
    end
  end
end
