class ContactsController < ApplicationController
  skip_authorization_check
  def new
    @contact = Contact.new(
      current_user ? { first_name: current_user.address.first_name,
                       last_name: current_user.address.last_name,
                       email: current_user.email} : {} )
  end

  def create
    @contact = Contact.new(params[:contact])
    UserMailer.send_contact_form(@contact).deliver if @contact.valid?
  end
end