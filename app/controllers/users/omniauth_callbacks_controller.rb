class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  skip_authorize_resource
  skip_authorization_check

  def all
    @user = User.from_omniauth(request.env["omniauth.auth"])
    @provider = @user.provider
    @provider.slice! "_oauth2"
    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, kind: @provider.capitalize) if is_navigational_format?
    else
      session["devise." + @provider + "_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  alias_method :paypal, :all
  alias_method :facebook, :all
  alias_method :google_oauth2, :all
  alias_method :twitter, :all

  def failure
    redirect_to root_path
  end
end
