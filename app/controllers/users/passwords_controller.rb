# This class exists for the purpose of managing the authorizations.
# It is necessary to provide own Controllers to change the Device logic.
class Users::PasswordsController < Devise::PasswordsController
  skip_authorize_resource
  skip_authorization_check
end