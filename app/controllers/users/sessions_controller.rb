# This class exists for the purpose of managing the authorizations.
# It is necessary to provide own Controllers to change the Device logic.
class Users::SessionsController < Devise::SessionsController
  skip_authorize_resource
  skip_authorization_check
  prepend_before_filter :verify_user, only: :destroy


  def after_sign_in_path_for(resource)
    if current_user.admin?
      return admin_members_url
    elsif current_user.art_association?
      return admin_panel_art_associations_members_url
    else
      super
    end
  end

  private

  def verify_user
    redirect_to :root,
      notice: t('.you_have_already_signed_out') and return unless user_signed_in?
  end
  
end