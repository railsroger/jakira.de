# Adds permitted parameters to the sign up process like first_name, last_name, etc.
# see: https://github.com/plataformatec/devise
class Users::ParameterSanitizer < Devise::ParameterSanitizer

  # These are the parameters that will be accepted during the sign up process.
  def sign_up
    default_params.permit(:email, :password, :password_confirmation, :nickname,
                          :account_type # Used for automatic shop creation
                        )
  end

  # These are the parameters that will be accepted during the account update process.
  # The update requires a current_password to validate the actual one. It uses password and password_confirmatino
  # to change the password.
  def account_update
    default_params.permit(:email,
                          :password,
                          :password_confirmation,
                          :current_password,
                          :first_name,
                          :last_name)
  end

  def show
    default_params.permit(:id)
  end

end