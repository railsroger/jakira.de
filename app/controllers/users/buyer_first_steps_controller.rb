class Users::BuyerFirstStepsController < ApplicationController
  include Wicked::Wizard
  steps :welcome, :profile, :addresses

  skip_authorization_check

  # This will render the wizard. See partials named by steps.
  def show
    @user = current_user
    render_wizard
  end

  def update
    @user = current_user

    case step
      when :welcome, :profile
        @user.update_attributes(user_params)

      when :addresses
        if @user.update_attributes(user_params)
          @user.wizard_completed = true
          @user.save
        end
    end
    
    render_wizard @user
  end

  private

  def user_params
    params.require(:user).permit(:first_name,:last_name,address_attributes:[:country,:state,:city,:street,:phone,:mobile,:postal_code])
  end

  #def finish_wizard_path
  #  '/user_panel'
  #end

end

