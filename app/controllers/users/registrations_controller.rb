# This class exists for the purpose of managing the authorizations.
# It is necessary to provide own Controllers to change the Device logic.
class Users::RegistrationsController < Devise::RegistrationsController
  skip_authorize_resource
  skip_before_filter :verify_authenticity_token, only: :create
  before_action :authenticate_user!
  before_action :set_address, only: :edit
  skip_authorization_check

  # Shows an User profile. This is not handled by Devise!
  def show
    authorize! :show, @user
    @user = User.find_by(id: params[:id])
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        if update_needs_confirmation?(resource, prev_unconfirmed_email)
          flash_notice_message(nil,
                             t('.email_changed.first_part'),
                             t('.email_changed.second_part'))
        else
          set_flash_message :notice, :updated
        end
      end
      sign_in resource_name, resource, bypass: true
      respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def create
    super
    if resource.persisted? && URI(request.referer || '').path == '/purchases/new'
      resource.update_attribute('last_url', '/purchases/new')
    end
    if resource.persisted? &&
      !resource.active_for_authentication? &&
      resource.inactive_message == :unconfirmed
        flash_notice_message(t('.signed_up_but_unconfirmed.header'),
                             t('.signed_up_but_unconfirmed.first_part'),
                             t('.signed_up_but_unconfirmed.second_part'))
    end
  end

  def update_resource(resource, params)
    if params[:email] != resource.email ||
      (params[:password] == "" &&
        params[:password_confirmation] == "" )
      resource.update_without_password(params)
    elsif super
      UserMailer.password_changed(resource).deliver
    end
  end

  # Custom redirect after profile update.
  # Source: https://github.com/plataformatec/devise/wiki/How-To:-Customize-the-redirect-after-a-user-edits-their-profile
  def after_update_path_for(resource)
    :edit_user_registration
  end

  def destroy
    if resource.has_shop?
      respond_to do |format|
        format.js {render 'notice.js.erb',
          locals: {header: nil,
                   first_part: t('.cancellation_notice_if_user_is_seller'),
                   second_part: nil,
                   question: false,
                   link: nil}}
      end
    else
      UserMailer.buyer_cancellation(resource).deliver
      resource.update(deleted: true)
      Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
      set_flash_message :notice, :destroyed
      yield resource if block_given?
      respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
    end
  end
 
  private

  def set_address
    @address = current_user.address
  end


  def sign_up_params
    params.require(:user).permit(:nickname,
                                 :email,
                                 :password,
                                 :password_confirmation,
                                 :accept_privacy_policy,
                                 :accept_terms,
                                  address_attributes:[
                                    :first_name,
                                    :last_name])
  end

  def account_update_params
    params.require(:user).permit(:nickname,
                                 :email,
                                 :current_password,
                                 :password,
                                 :password_confirmation,
                                 :message_received_notification,
                                 :rated_notification,
                                 :my_favorites_notification,
                                 :monthly_letters_notification,
                                 address_attributes:[
                                   :id,
                                   :occupation,
                                   :postal_code,
                                   :street,
                                   :first_name,
                                   :last_name,
                                   :place,
                                   :house_number,
                                   :show_to_public,
                                   :phone,
                                   :mobile,
                                   :show_phone_to_public,
                                   :country,
                                   :birth_date,
                                   :area_code],
                                 shop_attributes:[
                                   :id,
                                   payment_type_ids:[],
                                   bank_transfer_data_attributes:[
                                     :id,
                                     :bank_transfer,
                                     :account_holder,
                                     :account_number,
                                     :bank_code,
                                     :bank_name,
                                     :iban,
                                     :swft_bic,
                                     :article_id],
                                   paypal_data_attributes:[
                                     :id,
                                     :client_id,
                                     :client_secret],
                                   shipping_methods_attributes:[
                                     :id,
                                     :checked,
                                     :name,
                                     :price,
                                     :duration_from,
                                     :duration_to,
                                     :_destroy]])
  end
end