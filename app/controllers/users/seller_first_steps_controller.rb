class Users::SellerFirstStepsController < ApplicationController
  include Wicked::Wizard
  steps :welcome, :profile, :shop, :addresses

  skip_authorization_check

  # This will render the wizard. See partials named by steps.
  def show
    @user = current_user
    case step
      when :shop
        @shop = current_user.shop
    end
    render_wizard
  end

  def update
    @user = current_user

    case step
      when :shop
        @shop = @user.shop
        @shop.update_attributes(shop_params)
        render_wizard @shop

      when :profile
        @user.update_attributes(user_params)
        render_wizard @user

      when :addresses
        if @user.update_attributes(user_params)
          @user.wizard_completed = true
          @user.save
        end
        render_wizard @user
    end


  end

  private

  def user_params
    params.require(:user).permit(:first_name,:last_name,
                                 address_attributes:[:country,:state,:city,:street,:phone,:mobile,:postal_code])
  end

  def shop_params
    params.require(:shop).permit(:shop_name, :company_type, :corporate_form, :tax_number, :field_of_action, :default_article)
  end

  #def finish_wizard_path
  #  '/user_panel'
  #end

end

