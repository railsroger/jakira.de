# This class exists for the purpose of managing the authorizations.
# It is necessary to provide own Controllers to change the Device logic.
class Users::ConfirmationsController < Devise::ConfirmationsController
  skip_authorize_resource
  skip_authorization_check


  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?
    if resource.errors.empty?
      flash_key = User.last.sign_in_count == 0 ? :confirmed : :confirmed_after_changing
      set_flash_message(:notice, flash_key) if is_flashing_format?
      sign_in(resource)
      respond_with_navigational(resource){ redirect_to after_confirmation_path_for(resource_name, resource) }
    else
      respond_with_navigational(resource.errors, status: :unprocessable_entity){ render :new }
    end
  end

  def new
    super
  end

  def create
    super
  end


  protected

  def after_confirmation_path_for(resource_name, resource)
    if resource.last_url.present?
      return resource.last_url
    else
      super
    end
  end

  def after_resending_confirmation_instructions_path_for(resource_name)
    super
  end

end
