class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_action :set_shop, only: [:index, :new]

  #before_action :authenticate_user!
  authorize_resource

  def index
    @documents = Document.where(shop_id: @shop.id)
  end

  def show
  end

  def new
    @document = Document.new
  end

  def edit
  end

  def create
    @document = Document.new(document_params)
    respond_to do |format|
      if @document.save
        format.json { render action: 'show', status: :created, location: @document }
        format.html { render action: 'show', notice: 'Document was successfully created.' }
      else
        format.html { render action: 'edit' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
      format.js
    end
  end

  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to @document, notice: 'Document was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @document.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.html {
        # Only happen in shop
        set_shop
        redirect_to shop_documents_path(@shop)
      }
      format.js
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_document
    @document = Document.find(params[:id])
  end

  def set_shop
    @shop = Shop.friendly.find(params[:shop_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def document_params
    params[:document].permit(:document, :shop_id, :article_id, :articles_order_id, :assignment_id,
                             :name, :description)
  end

end
