class PicturesController < ApplicationController
  before_action :set_picture, only: [:crop, :destroy, :edit]

  load_and_authorize_resource

  def create
    @picture = current_user.uploaded_pictures.create(picture_params)
  end

  def crop
    @picture.update(crop_picture_params)
  end

  def destroy
    @picture.destroy if @picture.parent.nil?
  end

  def create_shop_main
    @main_picture =
      current_user.uploaded_pictures
        .create(picture_params.merge(kind: :shop_main_pictures))
  end

  private

  def set_picture
    @picture = Image.find(params[:id])
  end

  def picture_params
    params.require(:image).permit(:image)
  end

  def crop_picture_params
    params.require(:image)
          .permit(:x, :y, :width, :height, :rotate, :scale_x, :scale_y)
  end
end
