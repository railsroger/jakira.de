class ArtAssociationsController < ApplicationController
  authorize_resource class: false

  before_action :authenticate_user!,
    except: [:register, :create, :index]

  def register
    @art_association = ArtAssociation.new
    @art_association.build_address
  end

  def create
    @art_association = ArtAssociation.new(art_association_params)
    if @art_association.save
      flash_notice_message(t('.signed_up_but_unconfirmed.header'),
                           t('.signed_up_but_unconfirmed.first_part'),
                           t('.signed_up_but_unconfirmed.second_part'))
      redirect_to :root
    else
      render 'register'
    end
  end

  def become_a_member
    @user = current_or_guest_user
    if @user.update(params.require(:user).permit(:art_association_membership_number,
                                                 :art_association_id,
                                                 address_attributes:[
                                                  :first_name,
                                                  :last_name]).merge({become_a_member: true}))
      redirect_to new_shop_url
    else
      render 'shops/open'
    end
  end

  def events

  end

  def settings
  end



  private

  def art_association_params
    params.require(:art_association).permit(:nickname,
                                            :email,
                                            :password,
                                            :password_confirmation,
                                            address_attributes:[
                                              :company_name,
                                              :first_name,
                                              :last_name,
                                              :street,
                                              :house_number,
                                              :city,
                                              :postal_code,
                                              :phone])
  end

end
