class MessagesController < ApplicationController
  before_action :authenticate_user!
  # authorize_resource
  skip_authorization_check

  before_action :set_message, only: [:show,
                                     :edit,
                                     :update,
                                     :destroy,
                                     :reply,
                                     :to_trash]

  before_action :set_recipient, only: [:update, :create]

  after_action :destroy_messages_if_more_than_100,
                  only: [:to_trash,
                         :check_boxes_action]

  def new
    @message = Message.new
    @recipient = params[:recipient] if params[:recipient]
    if params[:article_title]
      if params[:article_title] != ''
        @message.subject = t '.request_of_user_about_article',
                                user: current_user.nickname,
                                article: params[:article_title]
      elsif params[:article_title] == ''
        @message.subject = t '.request_of_user',
                                user: current_user.nickname
      end
    end
    render "_form"
  end

  def create
    @message = Message.new(message_params)
    @message.sender_id = current_user.id
    recipient = User.find_by(nickname: params[:recipient]) ||
                User.find_by(email: params[:recipient]) ||
                User.joins(:shop).find_by(shops: {name: params[:recipient]})
    if current_user == recipient
      flash.alert = t ".you_can_not_send_a_message_to_yourself"
      render '_form'
      return
    end
    if params[:send]  
      if !recipient
        flash.alert = t ".the_recipient_is_not_found"
        render '_form'
        return
      end
      @message.recipient_id = recipient.id
      @message.sent_at = Time.now
      if @message.save
        if recipient.message_received_notification
          UserMailer.new_message_notification(@message).deliver
        end
        flash.notice = t '.message_was_sent'
        redirect_to :sent_messages
      else
        flash.alert = t '.you_need_to_fill_all_fields_to_send_a_message'
        render '_form'
      end
    elsif params[:save_as_a_draft]
      if recipient
        @message.recipient_id = recipient.id
      end
      @message.sender_status = "draft"
      @message.recipient_status = "not_received"

      @messages = Message.drafts(current_user)
      if @messages.length > 49
        flash.alert = t '.can_not_be_saved_as_a_draft_because_'
        redirect_to :drafts_messages
      else
        @message.save!(validate: false)
        redirect_to :drafts_messages, notice: t(".added_to_drafts")
      end
    end
  end

  def edit
    if current_user == @message.sender && @message.sender_status == "draft"
      if @message.recipient.present?
        @recipient = @message.recipient.nickname
      end
      render "_form"
    else
      redirect_to :inbox_messages, alert: t('access_denied')
    end
  end

  def update
    recipient = User.find_by(nickname: params[:recipient]) ||
                User.find_by(email: params[:recipient]) ||
                User.joins(:shop).find_by(shops: {name: params[:recipient]})
    if current_user == recipient
      flash.alert = t ".you_can_not_send_a_message_to_yourself"
      render '_form'
      return
    end
    if params[:send]
      @message.sender_status = nil
      if !recipient
        flash.alert = t ".the_recipient_is_not_found"
        render '_form'
        return
      end
      @message.recipient_id = recipient.id
      @message.sent_at = Time.now
      @message.recipient_status = nil
      if @message.update(message_params)
        if recipient.message_received_notification
          UserMailer.new_message_notification(@message).deliver
        end
        flash.notice = t '.message_was_sent'
        redirect_to :sent_messages
      else
        flash.alert = t '.you_need_to_fill_all_fields_to_send_a_message'
        render '_form'
      end
    elsif params[:save_as_a_draft]
      if recipient
        @message.recipient_id = recipient.id
      end
      @message.sender_status = 'draft'
      @message.recipient_status = "not_received"
      @message.attributes = message_params
      @message.save!(validate: false)

      flash.notice = t ".the_message_was_update"
      redirect_to :drafts_messages
    end
  end

  def destroy
  end

  def inbox
    @messages = Message.received(current_user)
                       .page(params[:page]).per(20)
    @messages.each do |message| 
      message.update_attribute('read_at', Time.now)
    end
    if @messages.length > 95
      flash.alert = t ".no_more_than_100_emails_can_be_received"
    end
    while @messages.length > 100
      @messages.last.update_attribute("recipient_status", "archived")
      @messages = Message.received(current_user)
    end
    render "_messages"
  end

  def sent
    @messages = Message.sent(current_user)
                       .page(params[:page]).per(20)
    if @messages.length > 95
      flash.alert = t ".no_more_than_100_emails_can_be_stored_in_sent"
    end
    while @messages.length > 100
      @messages.last.update_attribute("sender_status", "archived")
      @messages = Message.sent(current_user)
    end
    render "_messages"
  end

  def drafts
    @messages = Message.drafts(current_user)
                       .page(params[:page]).per(20)
    render "_messages"
  end

  def bin
    @messages = Message.deleted(current_user)
                       .order('created_at DESC')
                       .page(params[:page]).per(20)
    render "_messages"
  end

  def archive
    if params[:filter_by] == "sender"
      order = 'nickname ASC'
    elsif params[:filter_by] == "subject"
      order = 'subject ASC'
    else
      order = 'sent_at DESC'
    end

    @messages = Message.archived(current_user)
                       .joins(:sender)
                       .order(order)
                       .page(params[:page]).per(20)
    render "_messages"
  end

  def to_trash
    if current_user == @message.recipient || current_user == @message.sender
      if current_user == @message.recipient
        @message.update_attribute('recipient_status', "deleted")
      elsif current_user == @message.sender
        @message.update_attribute('sender_status', "deleted")
      end
      redirect_to :back, notice: t(".the_message_was_deleted")
    else
      redirect_to :inbox_messages, alert: t('access_denied')
    end
  end

  def empty_trash
    Message.where("sender_id = ? AND sender_status = ?", 
                  current_user.id, 'deleted').each do |message|
      message.update_attribute("sender_status", "destroyed")
      if message.recipient_status == "destroyed"
        message.destroy
      end
    end
    Message.where("recipient_id = ? AND recipient_status = ?", 
                  current_user.id, 'deleted').each do |message|
      message.update_attribute("recipient_status", "destroyed")
      if message.sender_status == "destroyed"
        message.destroy
      end
    end
    redirect_to :back, notice: t(".the_trash_is_empty")
  end

  def reply
    if (current_user == @message.recipient ||
        current_user == @message.sender) &&
          @message.sender_status != "draft"
      if @message.recipient.present?
        if current_user == @message.recipient
          @recipient = @message.sender.nickname
        else
          @recipient = @message.recipient.nickname
        end
      end
      @message = Message.new(subject: "Re: " + @message.subject)
      render "_form"
    else
      redirect_to :inbox_messages, alert: t('access_denied')
    end
  end

  def check_boxes_action
    if params[:message_ids].present?
      if params[:to_archive]
        @messages = Message.archived(current_user)
                           .order('created_at DESC')
        if @messages.length + params[:message_ids].length > 1000
          redirect_to :back, alert: t(".the_archive_is_full_")
        else
          params[:message_ids].each do |id|
            message = Message.find(id)
            if current_user == message.recipient
              message.update_attribute('recipient_status', 'archived')
            elsif current_user == message.sender
              message.update_attribute('sender_status', 'archived')
            end
          end
          flash.notice = t(".the_messages_were_added_to_the_archive")
        end
      elsif params[:delete]
        params[:message_ids].each do |id|
          message = Message.find(id)
          if current_user == message.recipient
            if message.recipient_status == "deleted"
              message.update_attribute('recipient_status', 'destroyed')
              if message.sender_status == "destroyed"
                message.destroy
              end
            else
              message.update_attribute('recipient_status', 'deleted')
            end
          elsif current_user == message.sender
            if message.sender_status == "deleted"
              message.update_attribute('sender_status', 'destroyed')
              if message.recipient_status == "destroyed"
                message.destroy
              end
            else
              message.update_attribute('sender_status', 'deleted')
            end
          end
        end
        flash.notice = t(".the_messages_were_removed_to_the_bin")
      end
    else
      flash.alert = t(".you_did_not_choose_any_messages_to_delete")
    end
    redirect_to :back
  end

  private

  def destroy_messages_if_more_than_100
    messages = Message.deleted(current_user)
                      .order('created_at ASC')
    while messages.length > 100
      message = messages.first
      if current_user == message.sender 
        message.update_attribute("sender_status", "destroyed")
        if message.recipient_status == "destroyed"
          message.destroy
        end
      end
      if current_user == message.recipient
        message.update_attribute("recipient_status", "destroyed")
        if message.sender_status == "destroyed"
          message.destroy
        end
      end
      messages = Message.deleted(current_user)
                        .order('created_at ASC')
    end
  end

  def set_recipient
    if params[:recipient].present?
      @recipient = params[:recipient]
    end
  end

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:subject, :body, :file)
  end

end
