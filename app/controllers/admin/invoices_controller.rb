class Admin::InvoicesController < ApplicationController
  authorize_resource :admin, class: false

  before_action :invoices, except: [:send_monthly_invoice,
                                    :send_commission_invoice]

  def created
    @actions = ["to_mail"] + @actions
    @invoices = @invoices.check
    render "admin/invoices/index"
  end

  def open
    @actions = @actions - ["mark_as_open"]
    @invoices = @invoices.open
    render "admin/invoices/index"
  end

  def paid
    @actions = @actions - ["mark_as_paid"]
    @invoices = @invoices.paid
    render "admin/invoices/index"
  end

  def cancellations
    @actions = @actions - ["cancel"]
    @invoices = @invoices.canceled
    render "admin/invoices/index"
  end

  def credits
    @actions = @actions - ["credit"]
    @invoices = @invoices.credited
    render "admin/invoices/index"
  end

  def check_boxes_action
    if params[:invoice_ids].present?
      if params[:invoices_action] != "to_mail"
        status = case params[:invoices_action]
        when 'mark_as_paid';  "paid"
        when 'mark_as_open';  "open"
        when 'cancel';        "canceled"
        when 'credit';        "credited"
        end
        params[:invoice_ids].each do |id|
          Invoice.find(id).update(status: status)
        end
        flash.notice = t ".invoices_status_was_updated"
      elsif params[:invoices_action] == "to_mail"

        params[:invoice_ids].each do |id|
          @invoice = Invoice.find(id)
          UserMailer.send_invoice_to_user(@invoice).deliver
        end

        flash.notice = t ".the_invoice_was_sent"
      end
    else
      flash.alert = t ".you_did_not_choose_any_invoice"
    end
    redirect_to :back
  end

  def send_monthly_invoice
    User.monthly_invoices_generating
    redirect_to :back
  end

  def send_commission_invoice
    User.quarter_invoices_generating
    redirect_to :back
  end

  private

  def invoices
    @actions = ['mark_as_paid', 'mark_as_open', 'cancel', 'credit', 'delete']
    @invoices = Invoice.all.order("created_at DESC").search(params[:search_invoice])
  end

end