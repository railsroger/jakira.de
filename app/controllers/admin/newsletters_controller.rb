class Admin::NewslettersController < ApplicationController
  authorize_resource :admin, class: false
  before_action :authenticate

  def new
    if params[:template_id]
      @newsletter = Newsletter.new(Newsletter.find(params[:template_id]).attributes)
    else
      @newsletter = Newsletter.new
    end
  end

  def create
    @newsletter = Newsletter.new(newsletter_params)
    if @newsletter.save
      if params[:newsletter][:to_test_email]
        UserMailer.send_newsletters(@newsletter, params[:newsletter][:email]).deliver
      else
        if !(@newsletter.template || @newsletter.delivery_date_checked)
          User.send_newsletters(@newsletter)
        end
      end
      redirect_to :new_admin_newsletter, notice: t(".newsletter_was_sent")
    else
      render 'new'
    end
  end

  private

  def newsletter_params
    params.require(:newsletter).permit(:email,
                                       :to_test_email,
                                       :text,
                                       :receivers,
                                       :subject,
                                       :template,
                                       :delivery_date_checked,
                                       :delivery_date,
                                       :template_name,
                                       :interval,
                                       :first_execution,
                                       :last_execution,
                                       :exposing_from,
                                       :exposing_to)
  end


end