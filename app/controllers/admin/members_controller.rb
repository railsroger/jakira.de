class Admin::MembersController < ApplicationController
  authorize_resource :admin, class: false

  before_action :set_user, except: :index
  before_action :set_user_shop, only: [ :shop_profile_about_me,
                                        :shop_profile_settings,
                                        :shop_profile_terms_and_imprint,
                                        :shop_profile_revocation]

  def index
  	@users = User.regular
                 .search(params[:search_member])
                 .order("users.created_at DESC")
    case params[:status]
    when "seller"
      @users = @users.sellers
    when "buyer"
      @users = @users.buyers
    end
  end

  def purchases_and_sales
    case params[:show_type]
    when "", "all", nil
      @transactions = ArticlesOrder.transactions(@user.id)
    when "seller"
      @transactions = ArticlesOrder.seller_transactions(@user.id)
    when "buyer"
      @transactions = ArticlesOrder.buyer_transactions(@user.id)
    end
  end

  def user_profile
  	if @user.address.nil?
      @address = Address.new
    else
      @address = @user.address
    end
  end

  def user_settings
  end

  def update_monthly_charge_for_the_user
    @user.shop.update(shop_monthly_invoice_params)
    redirect_to :back, notice: t(".successfully")
  end

  def update_monthly_charge_for_all_users
    monthly_charge = params[:monthly_charge].to_money
    if monthly_charge > 0
      Shop.where(use_personal_monthly_charge: false).each do |shop|
        shop.update(monthly_charge: monthly_charge.to_money)
      end
      flash.notice = t ".successfully"
    else
      flash.alert = t ".invalid_value"
    end
    redirect_to :back
  end

  def update_user_data
    if @user.update(user_params)
      redirect_to user_profile_admin_member_url(id: @user.membership_number)
    else
      render :user_profile
    end
  end

  def shop_profile_about_me
    @picture = Picture.new
    if !@user.has_shop?
      flash.alert = t ".the_user_has_no_shop"
      redirect_to_back_or_default
    else
      @main_picture = @user.shop.main_picture || MainPicture.new
    end
  end

  def shop_profile_settings
    if @user.shop.bank_transfer_data.nil?
      @user.shop.build_bank_transfer_data
    end
    if @user.shop.paypal_data.nil?
      @user.shop.build_paypal_data
    end
    ["pickup",
     "free_within_germany",
     "within_germany",
     "within_eu",
     "worldwide"].each do |shipping_method_name|
      if !@user.shop.shipping_methods.map(&:name).include?(shipping_method_name)
        @user.shop.shipping_methods.build(name: shipping_method_name)
      end
    end
  end

  def shop_profile_terms_and_imprint
  end

  def shop_profile_revocation
  end

  def restore_deleted_account
    @user.update(deleted: false)
    redirect_to user_profile_admin_member_url(@user.membership_number),
      notice: t(".the_account_was_restored") 
  end

  private

  def set_user_shop
    @shop = @user.shop
  end

  def set_user
  	@user = User.find_by(membership_number: params[:id])
  	if @user.nil?
  	  redirect_to :admin_members, alert: t(".there_is_no_such_user")
  	end
  end  

  def shop_monthly_invoice_params
    params.require(:shop).permit(:monthly_charge,
                                 :use_personal_monthly_charge)
  end

  def user_params
    params.require(:user).permit(:nickname,
                                 :email,
                                 :message_received_notification,
                                 :rated_notification,
                                 :my_favorites_notification,
                                 :monthly_letters_notification,
                                 address_attributes:[
                                   :id,
                                   :occupation,
                                   :postal_code,
                                   :street,
                                   :first_name,
                                   :last_name,
                                   :place,
                                   :house_number,
                                   :show_to_public,
                                   :phone,
                                   :mobile,
                                   :show_phone_to_public,
                                   :country,
                                   :birth_date,
                                   :area_code])
  end
end