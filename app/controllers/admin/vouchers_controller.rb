class Admin::VouchersController < ApplicationController
  authorize_resource :admin, class: false

  def index
    @vouchers = Voucher.all
    @vouchers = @vouchers.search(params[:search_in_vouchers]) if params[:search_in_vouchers]
    @vouchers = @vouchers.paid if params[:paid] == "true"
    case params[:order]
    when 'value'
      @vouchers = @vouchers.order(value_cents: :desc)
    when 'date'
      @vouchers = @vouchers.order(created_at: :asc)
    # when 'action'
    #   @vouchers
    when 'email'
      @vouchers = @vouchers.order(email: :asc)
    end
  end

  def change
    @main_voucher_data = MainVoucherData.first
  end

  def change_status
    @voucher = Voucher.find(params[:id])
    case params[:status]
    when 'sent'
      @voucher.confirm_payment
    when 'canceled'
      @voucher.cancel
    end
    redirect_to :back
  end

  def change_paid_on
    @voucher = Voucher.find(params[:id])
    @voucher.update(params.require(:voucher).permit(:paid_on))
    respond_to do |format|
      format.js
    end
  end
end