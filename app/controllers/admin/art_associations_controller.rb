class Admin::ArtAssociationsController < ApplicationController
  authorize_resource :admin, class: false
  
  before_action :set_art_association, only: [:edit, :update]
  
  def index
    @art_associations = ArtAssociation.all

    @art_associations = @art_associations.search(params[:search_art_association]) if params[:search_art_association]

    case params[:filter]
    when 'customer_number'
      @art_associations = @art_associations.order(:membership_number)
    when 'date_of_registration'
      @art_associations = @art_associations.order(:created_at)
    when 'place'
      @art_associations = @art_associations.joins(:address).order('addresses.place')
    end
  end

  def new
    @art_association = ArtAssociation.new
    @art_association.build_address 
  end

  def create
    generated_password = Devise.friendly_token.first(8)
    @art_association = ArtAssociation.new(art_association_params.merge({password: generated_password}))
    @art_association.skip_confirmation!
    if @art_association.save
      UserMailer.registration_without_password(@art_association, generated_password).deliver
      redirect_to admin_art_associations_url, notice: 'Success'
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    if @art_association.update(art_association_params)
      redirect_to admin_art_associations_url, notice: 'Success'
    else
      render 'edit'
    end
  end

  private

  def set_art_association
    @art_association = ArtAssociation.find_by(membership_number: params[:id].delete("VM").to_i)
  end

  def art_association_params
    params.require(:art_association).permit(:email,
                                             address_attributes:[
                                               :company_name,
                                               :first_name,
                                               :last_name,
                                               :street,
                                               :house_number,
                                               :city,
                                               :country,
                                               :postal_code,
                                               :phone])
  end

end