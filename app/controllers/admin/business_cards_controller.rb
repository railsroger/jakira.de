class Admin::BusinessCardsController < ApplicationController
  authorize_resource :admin, class: false

  def index
    @business_cards_orders = BusinessCardsOrder.all.order(id: :desc)
    @user = User.sellers.order("RANDOM()").first
    if @user.nil? 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    else
      @shop = @user.shop
      @address = @shop.address
      @business_card_details = BusinessCardDetails.first
    end
  end

  def change_details
    BusinessCardDetails.first.update(business_card_details_params) 
    redirect_to :back, notice: t('.successfully')
  end

  private

  def business_card_details_params
    params.require(:business_card_details).permit(:price,
                                                  :quantity,
                                                  :format,
                                                  :extent,
                                                  :dyeing,
                                                  :paper,
                                                  :gram_matur,
                                                  :printing_process,
                                                  :paintwork)
  end

end