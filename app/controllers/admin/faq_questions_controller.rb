class Admin::FaqQuestionsController < ApplicationController
  authorize_resource :admin, class: false

  before_action :set_faq_section,
                  only: [:new, :create, :show, :edit, :destroy, :update]
  before_action :set_faq_question,
                  only: [:show, :edit, :update, :destroy]


  def show
    render 'admin/faq_sections/index'
  end

  def new
    @faq_question = @faq_section.faq_questions.new
  end

  def create
    @faq_question = @faq_section.faq_questions.new(faq_question_params)
    if @faq_question.save
      redirect_to admin_faq_question_url(@faq_section, @faq_question)
    else
      render 'new'
    end
  end

  def update
    if @faq_question.update(faq_question_params)
      redirect_to admin_faq_question_url(@faq_section, @faq_question)
    else
      render 'edit'
    end
  end
  
  def destroy
    @faq_question.destroy
    redirect_to admin_faq_section_url(@faq_section)
  end

  private

  def faq_question_params
    params.require(:faq_question).permit(:question, :answer)
  end

  def set_faq_section
    @faq_section = FaqSection.find(params[:faq_section_id])
    @faq_questions = @faq_section.faq_questions
  end

  def set_faq_question
    @faq_question = FaqQuestion.find(params[:faq_question_id])
  end

end