class Admin::FaqSectionsController < ApplicationController
  authorize_resource :admin, class: false
  
  before_action :set_faq_section,
                  only: [:show, :edit, :update, :destroy]

  def show
    @faq_questions = @faq_section.faq_questions
    render 'admin/faq_sections/index'
  end

  def new
    @faq_section = FaqSection.new
  end

  def create
    @faq_section = FaqSection.new(faq_section_params)
    if @faq_section.save
      redirect_to admin_faq_section_url(@faq_section)
    else
      render 'new'
    end
  end

  def update
    if @faq_section.update(faq_section_params)
      redirect_to admin_faq_section_url(@faq_section)
    else
      render 'edit'
    end
  end
  
  def destroy
    @faq_section.destroy
    redirect_to admin_faq_sections_url
  end

  private

  def faq_section_params
    params.require(:faq_section).permit(:name)
  end

  def set_faq_section
    @faq_section = FaqSection.find(params[:faq_section_id])
  end

end