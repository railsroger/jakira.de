class Admin::CorrespondenceController < ApplicationController
  authorize_resource :admin, class: false

  layout 'user_mailer', except: :index

  def index
    @shop_name = '<span class="blue_link">Shop Name</span>'
    @buyer_name = '<span class="blue_link">Member Name</span>'
  end

  def registration
    @resource = User.regular.order("RANDOM()").first
    if @resource.present?
      @email = @resource.email
      render file: 'devise/mailer/confirmation_instructions'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end

  def new_shop
    @user = User.sellers.order("RANDOM()").first
    if @user.present? 
      render file: 'user_mailer/new_shop'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end

  def to_old_email_about_email_changing
    @user = User.regular.order("RANDOM()").first
    if @user.present?
      render file: 'user_mailer/to_old_email_about_email_changing'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end

  def confirmation_email
    @resource = User.regular.order("RANDOM()").first
    if @resource.present?
      @email = @resource.email
      render file: 'devise/mailer/changing_email_confirmation'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end

  def password_changed
    @user = User.regular.order("RANDOM()").first
    if @user.present?
      render file: 'user_mailer/password_changed'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end


  def purchase_notification_confirmed_to_buyer
    @articles_order = ArticlesOrder.where(status: 1)
                                   .order("RANDOM()").first
    if @articles_order.present?
      @buyer = @articles_order.buyer
      @seller = @articles_order.seller
      @shop = @seller.shop
      render file: 'user_mailer/purchase_notification'
    else 
      flash.notice = t ".there_is_no_order_for_this_example"
      redirect_to_back_or_default
    end
  end

  def purchase_notification_confirmed_to_seller
    @articles_order = ArticlesOrder.where(status: 1)
                                   .order("RANDOM()").first
    if @articles_order.present?
      @buyer = @articles_order.buyer
      @seller = @articles_order.seller
      @shop = @seller.shop
      render file: 'user_mailer/purchase_notification_to_seller'
    else 
      flash.notice = t ".there_is_no_order_for_this_example"
      redirect_to_back_or_default
    end
  end

  def purchase_notification_unconfirmed_to_buyer
    @articles_order = ArticlesOrder.where(status: 0)
                                   .order("RANDOM()").first
    if @articles_order.present?
      @buyer = @articles_order.buyer
      @seller = @articles_order.seller
      @shop = @seller.shop
      render file: 'user_mailer/purchase_notification'
    else 
      flash.notice = t ".there_is_no_order_for_this_example"
      redirect_to_back_or_default
    end
  end

  def purchase_notification_unconfirmed_to_seller
    @articles_order = ArticlesOrder.where(status: 0)
                                   .order("RANDOM()").first
    if @articles_order.present?
      @buyer = @articles_order.buyer
      @seller = @articles_order.seller
      @shop = @seller.shop
      render file: 'user_mailer/purchase_notification_to_seller'
    else 
      flash.notice = t ".there_is_no_order_for_this_example"
      redirect_to_back_or_default
    end
  end

  def articles_order_dispatched
    @articles_order = ArticlesOrder.order("RANDOM()").first
    if @articles_order.present?
      @article = @articles_order.article
      @user = @articles_order.buyer
      @shop = @articles_order.seller.shop
      render file: 'user_mailer/articles_order_dispatched'
    else 
      flash.notice = t ".there_is_no_order_for_this_example"
      redirect_to_back_or_default
    end
  end

  def article_is_already_sold
    @articles_order = ArticlesOrder.order("RANDOM()").first
    if @articles_order.present?
      @user = @articles_order.buyer
      @shop = @articles_order.seller.shop
      render file: 'user_mailer/article_is_already_sold'
    else 
      flash.notice = t ".there_is_no_order_for_this_example"
      redirect_to_back_or_default
    end
  end

  def new_order_message_from_buyer
    @order_message = OrderMessage.joins(articles_order: :purchase)
                      .where('purchases.user_id = order_messages.user_id')
                      .order("RANDOM()").first
    if @order_message.present?
      @recipient = @order_message.recipient
      @sender = @order_message.user
      @articles_order = @order_message.articles_order
      @shop = @articles_order.seller.shop
      render file: 'user_mailer/new_order_message'
    else 
      flash.notice = t ".there_is_no_message_for_this_example"
      redirect_to_back_or_default
    end
  end

  def new_order_message_from_seller
    @order_message = OrderMessage.joins(articles_order: :order)
                      .where('orders.seller_id = order_messages.user_id')
                      .order("RANDOM()").first
    if @order_message.present?
      @recipient = @order_message.recipient
      @sender = @order_message.user
      @articles_order = @order_message.articles_order
      @shop = @articles_order.seller.shop
      render file: 'user_mailer/new_order_message'
    else 
      flash.notice = t ".there_is_no_message_for_this_example"
      redirect_to_back_or_default
    end
  end

  def buyer_cancellation
    @user = User.regular.order("RANDOM()").first
    if @user.present?
      @email = @user.email
      render file: 'user_mailer/buyer_cancellation'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end
  
  def seller_cancellation
    @user = User.regular.order("RANDOM()").first
    if @user.present?
      @email = @user.email
      render file: 'user_mailer/seller_cancellation'
    else 
      flash.notice = t ".there_is_no_user_for_this_example"
      redirect_to_back_or_default
    end
  end

  def new_message
    @message = Message.order("RANDOM()").first
    if @message.present?
      @recipient = @message.recipient
      @sender = @message.sender
      render file: 'user_mailer/new_message_notification'
    else 
      flash.notice = t ".there_is_no_message_for_this_example"
      redirect_to_back_or_default
    end
  end

  def share_article
    @article = Article.normal.order("RANDOM()").first
    @sender = User.regular.order("RANDOM()").first
    if @article.present? && @sender.present?
      @text = t '.text_example'
      render file: 'user_mailer/share_article'
    else
      flash.notice = t ".there_is_no_article_or_user_for_this_example"
      redirect_to_back_or_default
    end
  end

  def new_voucher
    @voucher = Voucher.new(code: CouponCode.generate(parts: 2))
    render file: 'user_mailer/new_voucher'
  end

end