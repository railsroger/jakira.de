class OrdersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def open
    @articles_orders = current_user.sales.not_finished_by_seller
    render 'purchases/index'
  end

  def completed
    @articles_orders = current_user.sales.finished_by_seller
    render 'purchases/index'
  end

  def canceled
    @articles_orders = current_user.sales.canceled
    render 'purchases/index'
  end
end
