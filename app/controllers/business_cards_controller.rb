class BusinessCardsController < ApplicationController
  
  before_action :authenticate_user!
  
  authorize_resource class: false

  def index
    @business_cards_thumbs_path = "/business_cards_thumbs/#{current_user.id.to_s}/"
    @business_cards_templates_names = [*1..6].map { |n| n.to_s.rjust(2, '0') }
    @business_cards_order = current_user.business_cards_orders.new(value: 1)
    @business_card_details = BusinessCardDetails.first
  end

  def create
    @business_cards_order = current_user.business_cards_orders
                                        .new(business_cards_order_params
                                        .merge({status: :ordered}))
    if @business_cards_order.save
      UserMailer.send_business_cards_order_to_admin(@business_cards_order).deliver
      flash.notice = t '.successfully'
    else
      flash.alert = t '.error'
    end
    redirect_to :back
  end

  private

  def business_cards_order_params
    params.require(:business_cards_order).permit(:value)
  end

end
