class ShoppingBasketsController < ApplicationController
  layout 'purchase_process', only: :index

  load_and_authorize_resource

  before_action :set_shopping_basket, only: [:update, :destroy]

  def index
    @shopping_baskets = current_or_guest_user.shopping_baskets
                                             .order(created_at: :desc)
  end

  def create
    @shopping_basket = current_or_guest_user.shopping_baskets
                                            .new create_params
    if @shopping_basket.save
      flash.notice = t '.the_article_was_added_to_the_cart'
    else
      flash.alert = @shopping_basket.errors.full_messages.join('<br>')
    end
    redirect_to :back
  end

  def update
    @shopping_basket.update add_message_params
    render nothing: true
  end

  def destroy
    @shopping_basket.destroy
    redirect_to :back, notice: t('.the_article_was_removed_from_the_cart')
  end

  private

  def set_shopping_basket
    @shopping_basket = ShoppingBasket.find(params[:id])
  end

  def create_params
    params.require(:shopping_basket).permit(:article_id)
  end

  def add_message_params
    params.require(:shopping_basket).permit(:message_text)
  end

end
