class ArticlesOrdersController < ApplicationController
  require 'paypal-sdk-rest'
  include PayPal::SDK::REST

  load_and_authorize_resource

  before_action :set_articles_order

  before_action :set_purchase,
                only: [:set_payment_type,
                       :use_shipping_address,
                       :set_pickup_as_shipping_method]

  def paypal_payment
    PayPal::SDK::REST.set_config({
      mode: "sandbox",
      client_id: @articles_order.article.paypal_data.client_id,
      client_secret: @articles_order.article.paypal_data.client_secret })

    @payment = PayPal::SDK::REST::Payment.new({
      intent: "sale",
      payer: {
        payment_method: "paypal" },
      redirect_urls: {
        return_url: open_purchases_url + "?approved=true",
        cancel_url: open_purchases_url + "?approved=false" },
      transactions: [ {
        amount: {
          total: @articles_order.total_price.to_f,
          currency: @articles_order.article.price_currency },
        description: @articles_order.article.title}]})


    @payment.create
    session[:paypal_hash] = Digest::MD5.hexdigest(@payment.id)
    PaypalTransaction.create(articles_order_id: @articles_order.id,
                             payment_id: @payment.id,
                             hash_value: session[:paypal_hash])
    @payment.links.collect do |l|
      if l.rel == "approval_url"
        @approval_url = l.href
      end
    end
    redirect_to @approval_url + "&useraction=commit"
  end

  def credit_card_payment
    @credit_card_data = CreditCardData.new
    session[:articles_order_id] = @articles_order.id
  end

  def destroy
    current_user.shopping_baskets
                .find_by(article_id: @articles_order.original_article_id)
                .destroy
    @articles_order.destroy
    redirect_to :back
  end

  def closure_confirmation
    render 'notice.js.erb',
      locals: { header: nil,
                first_part: t('.finish_shopping?'),
                second_part: nil,
                question: true,
                link: close_articles_order_path(@articles_order)
      }
  end

  def close
    flash.notice =
      case @articles_order.close_by(current_user)
      when 'seller'
        t '.by_seller'
      when 'buyer'
        t '.by_buyer'
      else
        @articles_order.errors.full_messages.join('<br>')
      end
    redirect_to_back_or_default
  end

  def update_status
    @articles_order.update(status_params)
    redirect_to :back
  end

  def mark_as_sold
    if @articles_order.seller == current_user 
      @articles_order.update(finished_by_seller: true, finished_by_buyer: true)
      UserMailer.article_is_already_sold(@articles_order).deliver
      flash.notice = t '.the_article_was_removed_to_'
      redirect_to open_orders_url
    else
      redirect_to :root, alert: t('access_denied')
    end
  end

  def mark_order_messages_as_read
    @articles_order.order_messages
                   .by_recipient(current_user)
                   .unread
                   .update_all(read: true)
  end

  def set_payment_type
    @articles_order.update(params.require(:articles_order)
                                 .permit(:payment_type_id))
    @articles_order.reload
  end

  def use_shipping_address
    if @articles_order.update(params.require(:articles_order)
                      .permit(:use_shipping_address_for_payment_address))
    else
      flash.now.alert = t '.the_shipping_address_is_not_available'
    end
  end

  def set_pickup_as_shipping_method
    @articles_order.update(params.require(:articles_order)
                                 .permit(:set_pickup))
  end

  def use_voucher
    if @articles_order.update(params.require(:articles_order)
                                    .permit(:voucher_code))
      flash.notice = "The voucher code was accepted"
    else
      flash.alert = @articles_order.errors.full_messages.join('<br>')
    end
    redirect_to :back
  end

  def bank_details
    @bank_details = @articles_order.article.bank_transfer_data
  end

  def cancel
    @articles_order.cancel!
    redirect_to :back
  end

  def send_payment_reminder
    UserMailer.send_payment_reminder(@articles_order).deliver
    render nothing: true
  end

  private

  def set_articles_order
    @articles_order = ArticlesOrder.find(params[:id])
  end

  def set_purchase
    @purchase = @articles_order.purchase
  end

  def status_params
    params.require(:articles_order).permit(:status)
  end

  def credit_card_data_params
    params.require(:article_order).permit(:type)
  end
end
