# This is the main controller of the application.
# Specific application wide configuration can be put here.
# Read the official rails documentation for more information about this controller.
class ApplicationController < ActionController::Base
  # Protect the whole site
  before_action :authenticate,
                if: -> { Rails.env.production? }
  
  before_action :set_locale,
                :set_withdrawal_form
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Force authorization checks with CanCan on in all controllers!!!
  check_authorization   # :unless => :devise_controller? # Cancan

  # Handler for an unauthorized access
  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.html { 
        render file: "#{Rails.root}/public/403.html",
        status: 403,
        layout: false
      }
      format.js {
        flash.now.alert = t 'access_denied'
        render 'flash_alert',
        status: 403
      }
    end
  end

  helper_method :current_or_guest_user

  protected

  def current_ability
    @current_ability ||= Ability.new(current_or_guest_user)
  end

  # Forces password authentication for site access
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "admin" && password == "wdu2014"
    end
  end

  # In order to pass more Parameters to devise like first_name, last_name, etc. it is necessary to implement
  # an own version of the ParameterSanitizer. This method registers the custom sanitizer.
  # Source: https://github.com/plataformatec/devise/
  def devise_parameter_sanitizer
    if resource_class == User
      Users::ParameterSanitizer.new(User, :user, params)
    else
      super # Use the default one
    end
  end

  def set_locale
    session[:locale] = params[:locale] if params[:locale].present?

    I18n.locale = session[:locale] || I18n.default_locale #|| http_accept_language.compatible_language_from([:de, :en])
  end

  def default_url_options
    { subdomain: false }
  end

  # def default_url_options(options={})
  #   if I18n.locale != I18n.default_locale
  #     logger.debug "default_url_options is passed options: #{options.inspect}\n"
  #     { locale: I18n.locale }
  #   else
  #     {}
  #   end
  # end

  def current_or_guest_user
    if current_user
      if session[:guest_user_id].present?
        migrate_shopping_baskets_to_user
        guest_user.destroy
        session[:guest_user_id] = nil
      end
      current_user
    else
      if session[:expires_at].present?
        guest_user.destroy if session[:expires_at] < Time.current
      end
      guest_user
    end
  end

  # def ckeditor_authenticate
  #   authorize! action_name, @asset
  # end

  # def ckeditor_before_create_asset(asset)
  #   asset.assetable = current_user
  #   return true
  # end

  def set_materials
    @materials = []
    Material.where(ancestry: nil).each do |m|
      array = []
      array << m.name
      array << m.id
      array << {class: "material_parent"}
      @materials << array
      m.descendants.each do |d|
        array = []
        array << d.name
        array << d.id
        array << {class: "material_child"}
        @materials << array
      end
    end
  end

  def migrate_shopping_baskets_to_user
    current_user.shopping_baskets << guest_user.shopping_baskets
    current_user.favorites << guest_user.favorites
  end

  def redirect_to_back_or_default(default = root_url)
    if request.env["HTTP_REFERER"]#.present? and request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]
      redirect_to :back
    else
      redirect_to default
    end
  end

  def access_denied
    flash.alert = t 'access_denied'
    redirect_to_back_or_default
  end

  def flash_notice_message(header=nil,
                           first_part=nil,
                           second_part=nil,
                           question=false,
                           link=nil)

    flash.notice = render_to_string partial: 'notice',
      locals: {header:      header,
               first_part:  first_part,
               second_part: second_part,
               question: question,
               link: link}
  end

  private

  def guest_user
    User.find_by(id: session[:guest_user_id]) || new_guest_user
  end

  def new_guest_user
    u = User.new(role: "guest",
                 email: "guest_#{Time.now.to_i}#{rand(100)}@example.com")
    u.skip_confirmation!
    u.save!(validate: false)
    session[:guest_user_id] = u.id
    session[:expires_at] = Time.current + 10.minutes
    u
  end

  def set_withdrawal_form
    @withdrawal_form = WithdrawalForm.new
  end
end
