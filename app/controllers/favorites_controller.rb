class FavoritesController < ApplicationController
  load_and_authorize_resource

  def index
    @articles = current_or_guest_user.favorite_articles
  end

  def create
    @favorite = current_or_guest_user.favorites.new favorite_params
    if @favorite.save
      flash.notice = t '.successfully'
    else
      flash.alert = @favorite.errors.full_messages.join('<br>')
    end
    redirect_to :back
  end

  def destroy
    Favorite.find(params[:id]).destroy
    redirect_to :back
  end

  private

  def favorite_params
    params.require(:favorite).permit(:article_id)
  end
end
