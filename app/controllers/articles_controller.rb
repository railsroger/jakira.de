class ArticlesController < ApplicationController
  before_action :authenticate_user!,
    except: [:search, :show]

  before_action :set_article,
    only: [:show, :edit, :destroy, :update, :change]

  before_action :articles,
    only: [:sold, :online, :offline]

  before_action :set_materials,
    only: [:sold, :online, :offline, :deleted,
           :create, :edit, :update]

  before_action :check_if_user_has_shop, 
    except: [:search, :show]

  before_action :set_before_form_path,
    only: [:new, :edit]


  #authorize_resource
  skip_authorization_check
  

  def online
    @articles = @articles.online
    render "articles/_articles"
  end

  def offline
    @articles = @articles.offline
    render "articles/_articles"
  end

  def sold
    @articles = @articles.sold
    render "articles/_articles"
  end

  def deleted
    @articles = current_user.articles.deleted
                            .page(params[:page]).per(15)
    if params[:articles_action].present?
      articles_length = current_user.articles.deleted.count
      if params[:articles_action] == "delete"
        current_user.articles.deleted.each do |article|
          article.update(completely_deleted: true)
        end
        if articles_length > 0
          flash.notice = t '.articles_were_deleted'
        else
          flash.notice = t '.deletion_not_possible'
        end
        redirect_to :back
      end
      if params[:articles_action] == "restore"
        current_user.articles.deleted.each do |article|
          article.update(deleted: false)
        end
        if articles_length > 0
          flash.notice = t '.articles_were_restored'
        else
          flash.notice = t '.recovery_impossible'
        end
        redirect_to :back
      end
    else
      render "articles/_articles"
    end
  end

  def templates
    @articles = current_user.articles.template
                                     .latest
                                     .page(params[:page]).per(15)
  end

  def search
    @articles = Article.visible
                       .search(params[:search])
                       .page(params[:page]).per(24)
  end

  def show
    if @article.completely_deleted
      redirect_to :root, alert: t('.the_article_was_deleted')
    elsif @article.template? && @article.user != current_or_guest_user
      redirect_to :root, alert: t('access_denied')
    else
      ActiveRecord::Base.record_timestamps = false
      @article.increment!('views_count', 1)
      ActiveRecord::Base.record_timestamps = true
      @articles = @article.user.articles.visible.where.not(id: @article.id)
      @article_materials = @article.materials.map(&:name).join(', ')
      @article_colors = @article.colors.map(&:name).map(&:capitalize).join(', ')
      
      @main_picture = @article.main_picture
      @usual_pictures = @article.pictures.where.not(id: @article.main_picture_id)

      @article_pictures = [@main_picture, @usual_pictures].flatten - [nil]
      @address = @article.user.address
      @shop = @article.shop
      @user = @shop.user


      @categories = @article.categories
      @first_category = @categories.first

      if @categories.children_of(@first_category).present?
        @second_category = @categories.children_of(@first_category).first
      end

      if @second_category.present? && @categories.children_of(@second_category).present?
        @third_category = @categories.children_of(@second_category).first
      end
    end
  end

  def new
    @templates = current_user.articles.template.latest
    template = @templates.find_by(id: params[:template])
    if template.present?
      @article = Article.new(template.attributes.except('id', 'slug', 'created_at', 'updated_at', 'template'))
      @article.kind = 'normal'
      @article.materials = template.materials
      @article.colors = template.colors
      @article.gem_colors = template.gem_colors
      @article.suitable_fors = template.suitable_fors
      @article.categories = template.categories
      @article.payment_types = template.payment_types
      ["pickup",
       "free_within_germany",
       "within_germany",
       "within_eu",
       "worldwide"].each do |shipping_method_name|
        shipping_method = template.shipping_methods
                                  .find_by(name: shipping_method_name)
        if shipping_method.present?
          @article.shipping_methods.build(shipping_method.attributes.except('id', 'shipping_methodable_id'))
        else
          @article.shipping_methods.build(name: shipping_method_name)
        end
      end
      @article.picture_ids = template.pictures.collect do |picture|
        created_picture = Picture.create(image: picture.image)
        @article.main_picture_id = created_picture.id if picture.main?
        created_picture.id
      end
      template.material_textiles.each do |material_textile|
        @article.material_textiles.build(material_textile.attributes.except('id', 'article_id'))
      end
      if template.paypal_data.present?
        @article.build_paypal_data(template.paypal_data.attributes.except('id', 'paypal_datable_id'))
      else
        @article.build_paypal_data
      end
      if template.bank_transfer_data.present?
        @article.build_bank_transfer_data(template.bank_transfer_data.attributes.except('id', 'bank_transfer_datable_id'))
      else
        @article.build_bank_transfer_data
      end
      @article.build_shipping_readiness(template.shipping_readiness.attributes.except('id'))
    else
      @article = Article.new(payment_type_ids: current_user.shop.payment_type_ids, order_confirmation: current_user.shop.order_confirmation)
      @article.kind = "template" if params[:new_template]
      @material_textiles = 3.times {@article.material_textiles.build}
      @article.no_shipping_to_the_countries = current_user.shop.no_shipping_to_the_countries
      @article.build_bank_transfer_data(current_user.shop.bank_transfer_data.attributes.except("id"))
      @article.build_paypal_data(current_user.shop.paypal_data.attributes.except("id"))
      @article.build_shipping_readiness(current_user.shop.shipping_readiness.attributes.except('id'))
      ["pickup",
       "free_within_germany",
       "within_germany",
       "within_eu",
       "worldwide"].each do |shipping_method_name|
        shipping_method = current_user.shop.shipping_methods
                                      .find_by(name: shipping_method_name)
        if shipping_method.present?
          @article.shipping_methods.build(shipping_method.as_json.except('id'))
        else
          @article.shipping_methods.build(name: shipping_method_name)
        end
      end
      @article.suitable_for_ids = SuitableFor.all.ids
    end
    render 'articles/_form'
  end

  def create
    @article = current_user.articles.new(article_params
                             .merge({picture_ids: params[:picture_ids]}))
    if @article.save
      if @article.artist == ""
        @article.update(artist: current_user.full_name)
      end
      if @article.template?
        redirect_to :templates_articles, notice: t('.template_successfully_saved')
      else
        redirect_to @article, notice: t('.successfully_created')
      end
    else
      @templates = current_user.articles.template
      template = @templates.find_by(id: params[:template])
      render 'articles/_form'
    end
  end

  def edit
    if @article.user == current_or_guest_user
      @material_textiles = @article.material_textiles
      @article.build_bank_transfer_data(current_user.shop.bank_transfer_data.attributes.except("id")) if @article.bank_transfer_data.nil?
      if @article.paypal_data.nil? || (@article.paypal_data.client_id.empty? && @article.paypal_data.client_secret.empty?)
        @article.build_paypal_data(current_user.shop.paypal_data.attributes.except("id"))
      end
      ["pickup",
       "free_within_germany",
       "within_germany",
       "within_eu",
       "worldwide"].each do |shipping_method_name|
        if !@article.shipping_methods.map(&:name).include?(shipping_method_name)
          @article.shipping_methods.build(name: shipping_method_name)
        end
      end
      render 'articles/_form'
    else
      redirect_to :root, alert: t('access_denied')
    end
  end

  def change
    if @article.user == current_or_guest_user
      @article.update(article_status_params)
    else
      flash.alert = t('access_denied')
    end
    redirect_to :back
  end

  def update
    if @article.update(article_params.merge({picture_ids: params[:picture_ids]}))
      if @article.artist == ""
        @article.update(artist: current_user.full_name)
      end
      if @article.template?
        redirect_to :templates_articles, notice: t('.template_successfully_saved')
      else
        redirect_to @article, notice: t('.successfully_updated')
      end
    else
      render 'articles/_form'
    end
  end

  def destroy
    if @article.template?
      @article.destroy
    else
      @article.update(completely_deleted: true)
    end
    redirect_to :back, notice: t('.the_article_was_deleted')
  end

  private

  def set_article
    @article = Article.friendly.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      redirect_to :root, alert: t('.there_is_no_such_article')
  end

  def article_status_params
    params.require(:article).permit(:status,
                                    :deleted)
  end

  def article_params
    params.require(:article).permit(:title,
                                    :description,
                                    :price,
                                    :state,
                                    :status,
                                    :deleted,
                                    :customizable,
                                    :artist,
                                    :motive,
                                    :sku,
                                    :made_in,
                                    :year,
                                    :article_type,
                                    :gems,
                                    :textile_size,
                                    :width,
                                    :length,
                                    :height,
                                    :weight,
                                    :customizable_comment,
                                    :weight_of_gem, 
                                    :order_confirmation,
                                    :save_as_template,
                                    :template_name,
                                    :main_picture_id,
                                    :kind,
                                    :art_type,
                                    suitable_for_ids: [],
                                    gem_color_ids: [],
                                    color_ids: [],
                                    material_ids: [],
                                    payment_type_ids:[],
                                    category_ids:[],
                                    no_shipping_to_the_countries: [],
                                    paypal_data_attributes:[
                                      :client_id,
                                      :client_secret],
                                    orders_attributes:[
                                      :id,
                                      :payment_type,
                                      articles_orders_attributes:[
                                        :id,
                                        :note,
                                        :shipping_method_id]],
                                    pictures_attributes:[
                                      :image],
                                    shipping_methods_attributes:[
                                      :checked,
                                      :name,
                                      :price,
                                      :duration_from,
                                      :duration_to,
                                      :id,
                                      :_destroy],
                                    shipping_readiness_attributes:[
                                      :readiness,
                                      :from,
                                      :to],
                                    bank_transfer_data_attributes:[
                                      :bank_transfer,
                                      :account_holder,
                                      :account_number,
                                      :bank_code,
                                      :bank_name,
                                      :iban,
                                      :swft_bic,
                                      :article_id],
                                    material_textiles_attributes:[
                                      :name,
                                      :percent,
                                      :id])
  end

  def articles
    @articles = current_user.articles.not_deleted
                                     .normal
                                     .latest
                                     .page(params[:page]).per(15)
  end

  def check_if_user_has_shop
    if !current_or_guest_user.has_shop?
      redirect_to :root, notice: t('.you_have_no_shop')
    end
  end

  def set_before_form_path
    session[:before_article_form] = request.referer.present? ? request.referer : :root
  end
end
