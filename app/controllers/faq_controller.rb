class FaqController < ApplicationController

  before_filter :set_faq_sections

  skip_authorization_check

  def index
    if FaqSection.all.present?
      @faq_questions = []
      if params[:faq_questions_search].present?
        @faq_questions = FaqQuestion.all.search(params[:faq_questions_search])
      end
    else
      flash.alert = 'Sorry, this page has no content yet'
      redirect_to_back_or_default
    end
  end

  def show
    @faq_section = FaqSection.find(params[:id])
    @faq_questions = @faq_section.faq_questions

    render 'faq/index'
  end

  private

  def set_faq_sections
    @faq_sections = FaqSection.all
  end

end