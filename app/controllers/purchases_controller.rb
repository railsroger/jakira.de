class PurchasesController < ApplicationController
  layout 'purchase_process',
         only: [:addresses_and_payment_types,
                :check_order,
                :show]

  before_action :authenticate_user!
  load_and_authorize_resource

  before_action :update_purchase_information, only: :open
  before_action :set_purchase, except: [:new,
                                        :open,
                                        :completed]
  before_action :check_if_articles_orders_present,
                only: [:addresses_and_payment_types,
                       :check_order,
                       :complete,
                       :show]

  def new
    @purchase = current_user.use_or_generate_purchase
    redirect_to addresses_and_payment_types_purchase_url(@purchase)
  end

  def addresses_and_payment_types
    @articles_orders = @purchase.articles_orders.created_at
  end

  def check_order
    unless @purchase.all_filled?
      redirect_to addresses_and_payment_types_purchase_url(@purchase),
        alert: t(".you_need_to_fill_all_fields")
    else
      @purchase.set_shipping_methods
      @purchase.reload
      @articles_orders = @purchase.articles_orders.created_at
    end
  end

  def complete
    unless @purchase.all_filled?
      redirect_to addresses_and_payment_types_purchase_url(@purchase),
        alert: t(".you_need_to_fill_all_fields")
    else
      @purchase.confirm
      redirect_to @purchase
    end
  end

  def show
    @articles_orders = @purchase.articles_orders.created_at
  end

  def open
    @articles_orders = current_user.articles_orders.not_finished_by_buyer
    render 'index'
  end

  def completed
    @articles_orders = current_user.articles_orders.finished_by_buyer
    render 'index'
  end

  private
  
  def set_purchase
    @purchase = Purchase.find(params[:id])
  end

  def update_purchase_information
    if params[:approved].present?
      approved = params[:approved]
      if approved == "true"
        payment_id = PaypalTransaction.find_by(hash_value: session[:paypal_hash]).payment_id
        @payment = PayPal::SDK::REST::Payment.find(payment_id)
        @payment.execute({payer_id: params[:PayerID]})
        PaypalTransaction.find_by(payment_id: payment_id).update(complete: true)
        PaypalTransaction.find_by(payment_id: payment_id).articles_order.paid!
        flash.now.notice = t(".the_article_was_successfully_bought")
      else
        flash.now.alert = t(".payment_could_not_be_executed")
      end
    end
  end

  def check_if_articles_orders_present
    redirect_to root_url if @purchase.articles_orders.empty?
  end

end
