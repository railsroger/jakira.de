class ArtAssociations::MembersController < ApplicationController
  authorize_resource :art_association_members, class: false

  before_action :authenticate_user!
  before_action :set_member, only: [:edit, :update]

  def index
    @members = current_or_guest_user.members

    @members = @members.search(params[:search_member]) if params[:search_member]

    case params[:filter]
    when 'membership_number'
      @members = @members.order(:art_association_membership_number)
    when 'country'
      @members = @members.joins(:address).order('addresses.country')
    when 'place'
      @members = @members.joins(:address).order('addresses.place')
    when 'in_jakira_active'

    end
    respond_to do |format|
      format.html
      format.xls { send_data @members.members_to_xls(col_sep: "\t") }
    end
  end

  def new
    @member = current_or_guest_user.members.new
    @member.build_address 
  end

  def create
    generated_password = Devise.friendly_token.first(8)
    @member = current_or_guest_user.members.new(user_params.merge({password: generated_password}))
    @member.skip_confirmation!
    if @member.save
      UserMailer.registration_without_password(@member, generated_password).deliver
      redirect_to :root, notice: 'Success'
    else
      render 'new'
    end
  end

  def edit
    
  end
  
  def update
    if @member.update(user_params)
      redirect_to :root, notice: 'Success'
    else
      render 'edit'
    end
  end

  def import_xls
    if params[:file].present?
      begin
        current_or_guest_user.import_members_from_xls(params[:file])
      rescue Ole::Storage::FormatError => e
        flash.alert = 'Invalid Format'
      else
        flash.notice = 'Successfully'
      end
    else
      flash.alert = "You didn't add any file"
    end
    redirect_to :admin_panel_art_associations_members
  end

  private

  def set_member
    @member = current_or_guest_user.members.find_by(id: params[:id])
  end

  def user_params
    params.require(:user).permit(:art_association_membership_number,
                                 :email,
                                 address_attributes:[
                                   :first_name,
                                   :last_name,
                                   :street,
                                   :house_number,
                                   :city,
                                   :country,
                                   :postal_code,
                                   :phone])
  end

end