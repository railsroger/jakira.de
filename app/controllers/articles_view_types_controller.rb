class ArticlesViewTypesController < ApplicationController

  skip_authorization_check

  def change
    current_or_guest_user.update_attribute('articles_view_type', params[:value])
    redirect_to :back
  end

end