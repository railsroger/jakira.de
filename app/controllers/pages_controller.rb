# Servers static pages like the "home" or "about" page.
class PagesController < ApplicationController

  skip_authorization_check

  # Here the "home" page will be printed.
  def home
    @articles = Article.visible
    @shops    = Shop.order('created_at DESC').limit(10)
  end

  def about_jakira

  end

  def privacy

  end
  
  def imprint

  end

end
