class VouchersController < ApplicationController

  require 'paypal-sdk-rest'
  include PayPal::SDK::REST

  before_action :authenticate_user!, only: :pdf_version
  skip_authorization_check
  

  def show
    @voucher = Voucher.find(params[:id])
  end

  def new
    if MainVoucherData.first.have_payment_types?
      @voucher = Voucher.new
      @voucher.build_address
    else
      redirect_to :root, alert: "Sorry, vouchers don't work now"
    end
  end

  def create
    @voucher = Voucher.new(voucher_params)
    if @voucher.save
      case @voucher.payment_type
      when 'paypal'
        paypal_payment
      when 'paydirekt'
        @voucher.confirm_payment
        @voucher.save
        redirect_to voucher_url(@voucher)
      else
        redirect_to voucher_url(@voucher)
      end
    else
      render 'new'
    end
  end

  def after_payment
    if params[:approved].present?
      approved = params[:approved]
      if approved == "true"
        payment_id = PaypalTransaction.find_by(hash_value: session[:paypal_hash]).payment_id
        @payment = PayPal::SDK::REST::Payment.find(payment_id)
        @payment.execute({payer_id: params[:PayerID]})
        @paypal_transaction = PaypalTransaction.find_by(payment_id: payment_id)
        @paypal_transaction.update(complete: true)
        @voucher = @paypal_transaction.voucher
        @voucher.confirm_payment
        redirect_to voucher_url(@voucher)
      else
        redirect_to :root, alert: "You have not bought the voucher"
      end
    end
  end

  def pdf_version
    PDFKit.configure do |config|
      config.default_options = {
        page_size:      'Letter',
        margin_top:     '0in',
        margin_right:   '0in',
        margin_bottom:  '0in',
        margin_left:    '0in'
      }
    end
    @voucher = Voucher.find(params[:id])
    render layout: false
  end

  protected

  def paypal_payment
    PayPal::SDK.configure({
      mode: "sandbox",
      client_id: MainVoucherData.first.paypal_data.client_id,
      client_secret: MainVoucherData.first.paypal_data.client_secret })

    @payment = Payment.new({
      intent: "sale",
      payer: {
        payment_method: "paypal" },
      redirect_urls: {
        return_url: after_payment_vouchers_url + "?approved=true",
        cancel_url: after_payment_vouchers_url + "?approved=false" },
      transactions: [ {
        amount: {
          total: @voucher.value.to_f,
          currency: "EUR" },
        description: "Gutscheine"}]})

    @payment.create
    session[:paypal_hash] = Digest::MD5.hexdigest(@payment.id)
    PaypalTransaction.create(voucher_id: @voucher.id,
                             payment_id: @payment.id,
                             hash_value: session[:paypal_hash])
    @payment.links.collect do |l|
      @approval_url = l.href if l.rel == "approval_url"
    end
    redirect_to @approval_url + "&useraction=commit"
  end

  private

  def voucher_params
    params.require(:voucher).permit(:value,
                                    :from,
                                    :for,
                                    :message_text,
                                    :email,
                                    :email_confirmation,
                                    :payment_type,
                                    :agreement,
                                     address_attributes:[
                                       :postal_code,
                                       :street,
                                       :first_name,
                                       :last_name,
                                       :place])

  end

end
