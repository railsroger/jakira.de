class ShareController < ApplicationController

  skip_authorization_check

  before_action :authenticate_user!, only: :index
  before_action :set_article

  def index
    redirect_to @article
  end

  def new
    respond_to do |format|
      format.js
    end
  end

  def create
    options = {
      body: {
        secret:   '6LdyVB8TAAAAACI6nk3zlSr_zdVs7LKU8ARhDTIS',
        response: params['g-recaptcha-response'],
    }}
    response = HTTParty.post('https://www.google.com/recaptcha/api/siteverify',options)
    @success_response = response['success']
    @email = params[:email]
    @email_is_valid = ValidateEmail.valid?(@email)
    if @success_response && @email_is_valid
      @sent = true
      @text = params[:text]
      UserMailer.share_article(@article, current_user, @email, @text).deliver
      flash.notice = t '.the_email_was_sent'
    else
      flash.alert = t '.the_email_was_not_sent'
    end
    respond_to do |format|
      format.js
    end
    flash.discard
  end

  private

  def set_article
    @article = Article.find_by(id: params[:article_id])
  end

end