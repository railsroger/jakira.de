class ShopsController < ApplicationController
  before_action :set_shop, only: [ :update,
                                   :show,
                                   :destroy,
                                   :ratings,
                                   :download_withdrawal_form]

  before_action :set_current_user_shop, only: [ :about_me,
                                                :settings,
                                                :terms_and_imprint,
                                                :revocation]

  before_action :check_if_shop_is_deleted,
                  except: [:new,
                           :create,
                           :update,
                           :download_withdrawal_form,
                           :open]


  before_action :authenticate_user!,
                  only: [:new, :create, :destroy, :update, :open]

  skip_authorization_check

  def ratings
    @user = @shop.user
    @address = @shop.address
    @reviews = @user.received_ratings.finished
    if params[:rating].present?
      @reviews = @reviews.where(score: params[:rating])
    end
    if params[:displays].present? && params[:displays] != "all"
      @reviews = @reviews.where(from: params[:displays])
    else
      @reviews
    end
  end

  def show
    @user = @shop.user
    if @shop.online == true || current_user == @user
      @articles = @shop.articles.visible.page(params[:page]).per(24)
      @address = @shop.address
    else
      redirect_to :root, notice: t(".the_shop_is_not_available.")
    end
  end

  def new
    if current_or_guest_user.art_association.present?
      redirect_to :root, alert: t(".your_shop_is_already_present") if current_user.has_shop?
      if current_or_guest_user.shop.present?
        @shop = current_or_guest_user.shop
      else
        @shop = Shop.new
        @shop.build_address(current_user.address.attributes.except("id"))
        @shop.build_bank_transfer_data
        @shop.build_personal_bank_data
        @shop.build_paypal_data
        @shop.build_shipping_readiness
      end
      ["pickup",
       "free_within_germany",
       "within_germany",
       "within_eu",
       "worldwide"].each do |shipping_method_name|
        @shop.shipping_methods.build(name: shipping_method_name) if !@shop.shipping_methods.map(&:name).include?(shipping_method_name)
      end
    else
      flash.notice = t 'access_denied'
      redirect_to_back_or_default
    end
  end

  def about_me
    @main_picture = MainPicture.new
  end

  def download_withdrawal_form
    send_file @shop.withdrawal_form.file.path
  end

  def settings
    current_user.shop.build_bank_transfer_data if current_user.shop.bank_transfer_data.nil?
    current_user.shop.build_paypal_data        if current_user.shop.paypal_data.nil?
    ["pickup",
     "free_within_germany",
     "within_germany",
     "within_eu",
     "worldwide"].each do |shipping_method_name|
      if !current_user.shop.shipping_methods.map(&:name).include?(shipping_method_name)
        current_user.shop.shipping_methods.build(name: shipping_method_name)
      end
    end
  end

  def terms_and_imprint
  end

  def revocation
  end

  def open
    @user = current_or_guest_user
    redirect_to new_shop_url if @user.art_association.present?
  end

  def create
    @shop =
      current_user.build_shop(shop_params
                  .merge(picture_ids: params[:picture_ids]))
    @shop.address.first_name = current_user.address.first_name
    @shop.address.last_name = current_user.address.last_name
    if @shop.save
      UserMailer.new_shop(@shop.user).deliver
      redirect_to root_url(subdomain: @shop.subdomain),
                           notice: t('.successfull_created')
    else
      render 'new'
    end
  end

  def update
    @user = @shop.user
    if params[:update_shop_images] || params[:create_shop]
      @shop.picture_ids = params[:picture_ids]
    end
    if params[:delete_main_image] == "true"
      @shop.remove_image!
    end
    @shop.slug = nil
    if @shop.update(shop_params)
      if @shop.withdrawal_form.present? &&
          (params[:shop].present? &&
            !params[:shop][:withdrawal_form_id].present?)
        @shop.withdrawal_form.destroy
      end
      @shop.update(deleted: false)
      flash.notice = t('.successfull_updated')
      if params[:settings]
        if !current_user.admin?
          redirect_to settings_shops_url
        else
          redirect_to shop_profile_settings_admin_member_url(id: @user.membership_number)
        end
      elsif params[:create_shop]
        UserMailer.new_shop(@shop.user).deliver
        redirect_to root_url(subdomain: @shop.subdomain)
      else
        redirect_to :back
      end
    else
      ["pickup",
       "free_within_germany",
       "within_germany",
       "within_eu",
       "worldwide"].each do |shipping_method_name|
        if !@shop.shipping_methods.map(&:name).include?(shipping_method_name)
          @shop.shipping_methods.build(name: shipping_method_name)
        end
      end
      if params[:create_shop]
        render action: 'new'
      elsif !current_user.admin?
        render action: 'settings'
      else
        render "admin/members/shop_profile_settings"
      end
    end
  end

  def destroy
    @shop.articles.each do |article|
      article.update(status: "offline")
    end
    UserMailer.seller_cancellation(@shop.user).deliver
    @shop.update(deleted: true)
    redirect_to :edit_user_registration
  end

  private

  def set_shop
    @shop = Shop.find_by(slug: params[:id]) || Shop.find_by(subdomain: request.subdomain)
  end

  def set_current_user_shop
    @shop = current_or_guest_user.shop
  end

  def check_if_shop_is_deleted
    if @shop.nil? || @shop.deleted?
      flash.alert = t ".the_shop_does_not_exist"
      redirect_to_back_or_default
    end
  end

  def shop_params
    params.require(:shop).permit(:_destroy,
                                 :payment_authorization,
                                 :id,
                                 :address_id,
                                 :online,
                                 :order_confirmation,
                                 :revocation,
                                 :articles_type,
                                 :revocation_physical_products,
                                 :revocation_services,
                                 :revocation_digital_content,
                                 :name,
                                 :subdomain,
                                 :company_type,
                                 :corporate_form,
                                 :tax_number,
                                 :field_of_action,
                                 :default_article,
                                 :image,
                                 :image_cache,
                                 :subscription,
                                 :accept_terms,
                                 :professional,
                                 :imprint,
                                 :terms,
                                 :terms_and_revocation_are_chosen,
                                 :about_me,
                                 :about_my_shop,
                                 :withdrawal_form_id,
                                 :package,
                                 :set_shipping,
                                 :set_payment_types,
                                 :main_picture_id,
                                 payment_type_ids:[],
                                 no_shipping_to_the_countries: [],
                                 bank_transfer_data_attributes:[
                                   :id,
                                   :bank_transfer,
                                   :account_holder,
                                   :account_number,
                                   :bank_code,
                                   :bank_name,
                                   :iban,
                                   :swft_bic],
                                 personal_bank_data_attributes:[
                                   :id,
                                   :bank_transfer,
                                   :account_holder,
                                   :account_number,
                                   :bank_code,
                                   :bank_name,
                                   :iban,
                                   :swft_bic],
                                 paypal_data_attributes:[
                                   :id,
                                   :client_id,
                                   :client_secret],
                                 shipping_methods_attributes:[
                                   :id,
                                   :checked,
                                   :name,
                                   :price,
                                   :duration_from,
                                   :duration_to,
                                   :_destroy],
                                 shipping_readiness_attributes:[
                                   :readiness,
                                   :from,
                                   :to],
                                 address_attributes:[
                                   :id,
                                   :occupation,
                                   :country,
                                   :postal_code,
                                   :street,
                                   :first_name,
                                   :last_name,
                                   :place,
                                   :house_number,
                                   :show_to_public,
                                   :phone,
                                   :mobile,
                                   :show_phone_to_public,
                                   :area_code])
  end
end
