class ShippingMethodsController < ApplicationController
  before_action :set_shipping_method, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!
  authorize_resource

  # GET /shipping_methods
  # GET /shipping_methods.json
  def index
    @shipping_methods = ShippingMethod.all
  end

  # GET /shipping_methods/1
  # GET /shipping_methods/1.json
  def show
    @shipping_method.destroy
    redirect_to :back
  end

  # GET /shipping_methods/new
  def new
    @shipping_method = ShippingMethod.new
  end

  # GET /shipping_methods/1/edit
  def edit
  end

  # POST /shipping_methods
  # POST /shipping_methods.json
  def create
    @shipping_method = ShippingMethod.new(shipping_method_params)
    #@shipping_method.article_id = current_user.shop.default_article_id

    respond_to do |format|
      if @shipping_method.save
        format.html { redirect_to @shipping_method, notice: 'Shipping method was successfully created.' }
        format.json { render action: 'show', status: :created, location: @shipping_method }
      else
        format.html { render action: 'new' }
        format.json { render json: @shipping_method.errors, status: :unprocessable_entity }
      end
      format.js
    end
  end

  # PATCH/PUT /shipping_methods/1
  # PATCH/PUT /shipping_methods/1.json
  def update
    respond_to do |format|
      if @shipping_method.update(shipping_method_params)
        format.html { redirect_to @shipping_method.article, notice: 'Shipping method was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @shipping_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shipping_methods/1
  # DELETE /shipping_methods/1.json
  def destroy
    # article = @shipping_method.article
    @shipping_method.destroy
    redirect_to :back
    # respond_to do |format|
    #   format.html { redirect_to :back }
    #   format.json { head :no_content }
    #   format.js
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shipping_method
      @shipping_method = ShippingMethod.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shipping_method_params
      params[:shipping_method].permit(:name, :duration, :price, :article_id)
    end
end
