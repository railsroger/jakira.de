class CategoriesController < ApplicationController
  before_action :set_category

  load_and_authorize_resource

  def show

    @textile_sizes = Article.textile_sizes
    @colors = Color.available

    @articles = @category.articles.visible

    @motives = @articles.map(&:motive).uniq.compact
    @artists = @articles.map(&:artist).uniq.compact

    if params[:textile_sizes].present?
      @articles = @articles.where(textile_size: params[:textile_sizes])
    end

    if params[:color_ids].present?
      @articles = @articles.joins(:colors)
                           .where(colors: { id: params[:color_ids] })
    end

    if params[:motive].present?
      @articles = @articles.where(motive: params[:motive])
    end

    if params[:artist].present?
      @articles = @articles.where(artist: params[:artist])
    end

    prices_array = @articles.collect { |x| x.price_cents / 100 }
    prices_array = [prices_array[0] / 2, prices_array[0] * 2] if prices_array.count == 1
    @min_price_peak = prices_array.min || 0
    @max_price_peak = prices_array.max || 500

    @min_price = params[:min_price].to_i || @min_price_peak
    @max_price = params[:max_price].to_i || @max_price_peak
    if @min_price < @min_price_peak || @min_price > @max_price_peak
      @min_price = @min_price_peak
      params.delete :min_price
    end
    if @max_price > @max_price_peak || @max_price < @min_price_peak
      @max_price = @max_price_peak
      params.delete :max_price
    end

    @main_category = @category.path.second
    @second_category = @category.path.third
    @third_category = @category.path.fourth

    @articles = @articles.where("articles.price_cents > ? AND articles.price_cents < ?",
                         (@min_price - 1) * 100, (@max_price + 1) * 100)
                         .uniq
                         .search_in_category(params[:search_in_category])
                         .search_in_category(params[:mobile_search_in_category])
                         .page(params[:page]).per(24)
  end

  private
  
  def set_category
    @category = Category.friendly.find(params[:id])
  end

end
