class OrderMessagesController < ApplicationController
  load_and_authorize_resource

  def show
    order_message = OrderMessage.find_by(id: params[:id])
    recipient = order_message.recipient
    anchor_hash = order_message.anchor_hash
    path =
      if recipient == order_message.articles_order.seller
        if order_message.articles_order.finished_by_seller?
          completed_orders_url(anchor_hash)
        else
          open_orders_url(anchor_hash)
        end
      elsif recipient == order_message.articles_order.buyer
        if order_message.articles_order.finished_by_buyer?
          completed_purchases_url(anchor_hash)
        else
          open_purchases_url(anchor_hash)
        end
      end
    redirect_to path
  end

  def create
    @order_message =
      current_user.order_messages.create(order_message_params)
  end

  private

  def order_message_params
    params.require(:order_message).permit(:text, :articles_order_id)
  end
end
