class FavoriteShopsController < ApplicationController
  
  # authorize_resource
  skip_authorization_check

  def index
    @favorite_shops = current_or_guest_user.favorite_shops
  end

  def create
    @favorite_shop = FavoriteShop.new(shop_id: params[:shop_id])
    @favorite_shop.user_id = current_or_guest_user.id
    if @favorite_shop.save
      flash.notice = @favorite_shop.shop.name + ' ' + t('.is_stored_in_your_favorite_shops')
    end
    redirect_to :back
  end

  def destroy
    FavoriteShop.find(params[:id]).destroy
    redirect_to :back, notice: t('.the_shop_was_removed_from_your_favorites')
  end
  # private

  # def favorite_shops_params
  #   params.require(:favorite_shop).permit(:shop_id)
  # end
end
