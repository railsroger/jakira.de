class InvoicesController < ApplicationController

  before_action :authenticate_user!
  skip_authorization_check

  before_action :set_invoice, only: [:show, :pdf_version]

  def index
  	@invoices = current_user.invoices
                            .order("created_at DESC")
                            .search(params[:search_invoice])
  end

  def show

  end

  def pdf_version
    if request.original_url.last(3) != "pdf"
      redirect_to (current_user.admin? ? created_admin_invoices_url : :invoices),
        notice: t('access_denied')
    else
      PDFKit.configure do |config|
        config.default_options = {
          page_size:      'Letter',
          margin_top:     '0.3in',
          margin_right:   '0.3in',
          margin_bottom:  '0.3in',
          margin_left:    '0.3in'
        }
      end
      render layout: "pdf_layout"
    end
  end

  private

  def set_invoice
  	@invoice = Invoice.find(params[:id])
  end

end