class RatingsController < ApplicationController
  before_action :set_rating, only: [:update, :destroy]
  before_action :authenticate_user!, only: :index
  load_and_authorize_resource only: :update

  skip_authorization_check

  def index
    @user = current_user
    @reviews = @user.received_ratings.finished
    if params[:rating].present?
      @reviews = @reviews.where(score: params[:rating])
    end
    if params[:displays].present? && params[:displays] != "all"
      @reviews = @reviews.where(from: params[:displays])
    else
      @reviews
    end
    # if params[:displays] == "buyer"
    #   @reviews = @reviews.to_seller
    # elsif params[:displays] == "seller"
    #   @reviews = @reviews.to_buyer
    # else
    #   @reviews
    # end
    # @ratings = Order.where(seller_id: @shop.user_id).delete_if {|o| o.rating.blank?}.map(&:rating)
  end

  def update
    if @rating.update(rating_params) && @rating.finished?
      flash.now.notice = t ".the_review_was_successfully_saved"
    else
      flash.now.alert = @rating.errors.full_messages.join('<br>')
    end
  end

  private

  def set_rating
    @rating = Rating.find(params[:id])
  end

  def rating_params
    params.require(:rating).permit(:score, :text, :submit)
  end
end
