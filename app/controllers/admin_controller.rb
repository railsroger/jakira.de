class AdminController < ApplicationController
  authorize_resource class: false

  def show
  	redirect_to :admin_members
  end

  # check later if unnecessary do
  def newsletter
    
  end

  def new_newsletter
    @newsletter_text = params[:newsletter_text]
    if @newsletter_text.present?
      User.newsletters_receivers.each do |user|
        UserMailer.send_newsletters(@newsletter_text, user).deliver
      end
      flash.notice = t ".newsletter_was_sent"
    else
      flash.alert = t ".newsletter_was_not_sent"
    end
    redirect_to :back
  end
  # check later if unnecessary end
  
  def maillog
    @maillog = Maillog.all.order(sent_at: :desc)
  end
  
  def text_content
    redirect_to admin_faq_sections_url
  end

  def discover_the
  
  end

  def find_yours
  end

end
