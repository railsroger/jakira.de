class WithdrawalFormsController < ApplicationController

  # authorize_resource
  skip_authorization_check

  def preview
    @withdrawal_form = WithdrawalForm.find(params[:id])
    send_file @withdrawal_form.file.path, disposition: 'inline'
  end

  def create
    @withdrawal_form = WithdrawalForm.create(withdrawal_form_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @withdrawal_form = WithdrawalForm.find(params[:id])
    @withdrawal_form.destroy if @withdrawal_form.shop_id.nil?
  end

  private

  def withdrawal_form_params
    params[:withdrawal_form].permit(:file, :shop_id)
  end

end