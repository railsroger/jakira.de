class MainVoucherDataController < ApplicationController
  
  before_action :check_if_admin
  skip_authorization_check

  def update
    @main_voucher_data = MainVoucherData.find(params[:id])
    if @main_voucher_data.update(main_voucher_data_params)
      redirect_to :change_admin_vouchers, notice: "Successfully"
    else
      render 'admin/vouchers/change'
    end
    
  end

  private

  def main_voucher_data_params
    params.require(:main_voucher_data).permit(:bank_transfer,
                                              :paypal,
                                              :paydirekt,
                                              :mandatory_approval,
                                              :voucher_terms,
                                              :cancellation,
                                              :imprint,
                                              bank_transfer_data_attributes:[
                                                :id,
                                                :bank_name,
                                                :account_holder,
                                                :iban,
                                                :swft_bic],
                                              paypal_data_attributes:[
                                                :id,
                                                :client_id,
                                                :client_secret])

  end
end
