module FavoritesHelper
  def favorites_left_menu_count(title)
    if title == :favorite_products
      current_or_guest_user.favorites.count
    elsif title == :favorite_shops
      current_or_guest_user.favorite_shops.count
    end
  end
end
