# This module can hold functions that will be accessible through the whole application.
# Keep in mind that this pollutes the namespace and should be avoided! Bears the risk of
# bad PHP-like development style. Use "Decorators" instead.
#
# Ideally this file should be empty.
module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def shopping_baskets_count
    current_or_guest_user.shopping_baskets.count
  end

  def favorites_count
    current_or_guest_user.favorites.count
  end

  def unread_messages?
    current_or_guest_user.has_unread_messages?
  end

  def unread_messages_in_sales?
    OrderMessage.unread_in_sales(current_or_guest_user.id).count > 0
  end

  def unread_messages_in_purchases?
    OrderMessage.unread_in_purchases(current_or_guest_user.id).count > 0
  end

  def unread_messages_in_sales_or_purchases?
    unread_messages_in_sales? || unread_messages_in_purchases?
  end

  def current_user_name
    if current_or_guest_user.admin?
      current_or_guest_user.nickname
    else
      current_or_guest_user.full_name
    end
  end

  def must_the_page_be_responsive?
    %w(vouchers categories faq art_associations favorites favorite_shops
       art_associations/members admin/faq_sections admin/faq_questions faq
       contacts users/registrations users/sessions pages shopping_baskets
       purchases orders messages ratings invoices shops
       business_cards).include?(params[:controller]) ||
      (params[:controller] == 'shops' && params[:action] == 'open') || 
      (params[:controller] == 'articles' && params[:action] != 'show')
  end

  def currency(value)
    precision = value.zero? || value.to_f != value.to_i ? 2 : 0
    number_to_currency(value.to_f, precision: precision, format: '%n%u')
  end
end
