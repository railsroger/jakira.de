module ArticlesOrdersHelper

  def is_the_articles_order_status_can_be_updatable?(articles_order, status)
    (ArticlesOrder.statuses[articles_order.status] + 1 == ArticlesOrder.statuses[status] &&
      articles_order.awaiting_confirmation?) ||
     !articles_order.awaiting_confirmation?
  end

  def articles_order_statuses
    ArticlesOrder.statuses.keys.drop(1) + %w(review finished)
  end

end
