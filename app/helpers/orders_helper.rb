module OrdersHelper

  # Returns 'Yes' or 'No'
  def bool_to_str(val)
    if val == true
      I18n.t('yes')
    else
      I18n.t('no')
    end
  end

  # Translates the state of the approval
  def approval_state_to_str(val)
    if val == 'approved'
      return I18n.t('approved')
    end
    if val == 'declined'
      return I18n.t('declined')
    end
    return I18n.t('pending')
  end

end
