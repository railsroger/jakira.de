module AdminHelper

  def is_text_content_path?
    (params[:controller] == 'admin' &&
      ['text_content', 'discover_the', 'find_yours'].include?(params[:action])) ||
    is_admin_faq_path?    
  end

  def is_admin_faq_path?
    params[:controller] == 'admin/faq_sections' ||
    params[:controller] == 'admin/faq_questions'
  end

  def subdomain_is_administration?
    request.subdomain == "administration"
  end

end