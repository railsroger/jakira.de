module CategoriesHelper

  def categories_drop_down_columns(category)
    category_children = category.children.reorder(id: :asc)
    categories_column = []
    categories_count = 0
    columns = []
    category_children.each do |subcategory|
      categories_column << subcategory
      categories_count += 2 + subcategory.children.count
      if categories_count >= 9
        columns << categories_column
        categories_column = []
        categories_count = 0
      end
    end
    columns << categories_column
    columns
  end

  def categories_limit_in_column(column)
    parent_categories = column.flatten
    all_categories = (parent_categories.map { |category| [category, category.children.reorder(id: :asc)] }).flatten
    all_categories.first(9 - (parent_categories.count - 1))
  end

  def show_more_categories?(column, subcategory)
    (subcategory.children.flatten - categories_limit_in_column(column)).count > 0
  end

end
