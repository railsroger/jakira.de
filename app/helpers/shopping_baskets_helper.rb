module ShoppingBasketsHelper

  def purchase_thumb_image(purchase)
    if purchase.main_picture.present?
      purchase.main_picture.image.purchase_thumb
    else
      'placeholder.gif'
    end
  end

end
