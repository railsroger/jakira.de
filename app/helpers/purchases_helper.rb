module PurchasesHelper

  def if_purchase_step_path_is_available(path_index, index)
    'available' if index <= path_index  
  end

  def open_or_completed_purchases_pages?
    current_page?(open_purchases_url) ||
      current_page?(completed_purchases_url)
  end

end
