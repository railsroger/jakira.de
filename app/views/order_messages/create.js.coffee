<% if @order_message.errors.count == 0 %>
  newMessageForm = $(".new_order_message input[value=<%= @order_message.articles_order.id %>]")
                     .closest('.new_message_form_wrapper')  
  newMessageForm.before("<%= j render @order_message %>")
                .find('textarea').val('')
  newMessageForm.prev().hide().slideDown()
                .prev().hide().slideDown()
<% end %>