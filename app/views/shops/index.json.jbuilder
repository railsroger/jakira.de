json.array!(@shops) do |shop|
  json.extract! shop, :shop_name, :company_type, :corporate_form, :tax_number, :field_of_action, :user, :default_article
  json.url shop_url(shop, format: :json)
end
