$('#notice-area').empty()
<% if @sent %>
$('#notice-area').append('<div id="notice"><%= flash.notice %></div>')
$('.share').remove()
<% else %>
$('#notice-area').append('<div id="alert"><%= flash.alert %></div>')
grecaptcha.reset()
<% if @email_is_valid %>
$('#email').css('border-color', '')
<% else %>
$('#email').css('border-color', 'red')
<% end %>

<% if @success_response %>
$('.g-recaptcha > div > div').css('border', '')
<% else %>
$('.g-recaptcha > div > div').css('border', '1px solid red').css('border-radius', '4px')
<% end %>

<% end %>