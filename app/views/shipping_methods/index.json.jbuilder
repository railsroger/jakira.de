json.array!(@shipping_methods) do |shipping_method|
  json.extract! shipping_method, 
  json.url shipping_method_url(shipping_method, format: :json)
end
