<% if @main_picture.errors.count == 0 %>

$('.main_picture_block #add_main_picture_button').hide()
$('.main_picture_block #add_main_picture_button').before("<%= j render partial: 'main_pictures/picture_edit', locals: { main_picture: @main_picture } %>")

<% end %>

$('.main_picture_block .loading').hide()

$("#main_picture_image").change ->
  $(this).closest('form').submit()