$('.crop_picture').remove()
newCropPicture = "<%= j render partial: 'pictures/crop' %>"
$('body').append(newCropPicture)
$('.crop_picture').hide()
$('#pictures_place').spin()

<% if @picture.image.file.exists? %>
$('.crop_picture img').load ->
  $('.spinner').remove()
  $('.crop_picture').show()
# imageWidth = $('.crop_image').attr('image-width')
# imageHeight = $('.crop_image').attr('image-height')
# koo = 1200 / (imageWidth / (imageWidth / (imageHeight / 454)))
$('.crop_image').cropper
  autoCropArea: 1
  dragMode: 'move'
  background: false
  viewMode: 2
  preview: '.preview'
  # minCropBoxWidth: koo
  build: (e) ->
    $(this).css
      maxHeight: '450px'
  crop: (e) ->
    $('#picture_x').val e.x
    $('#picture_y').val e.y
    $('#picture_width').val e.width
    $('#picture_height').val e.height
    $('#picture_rotate').val e.rotate
$('.rotate').click ->
  $('.crop_image').cropper("rotate", 90).cropper('clear').cropper('crop')
$('.crop_picture .cancel').click ->
  $('.crop_picture').remove()
$('.crop_picture').draggable handle: ".head"
# $('.crop_picture').resizable
#   handles: "n, en, e, se, s, ws, w, nw"
#   minHeight: 250
#   minWidth: 450
#   # resize: (event, ui) ->
#   #   ui.size.height = ui.size.height + 150
$('.zoom-in').click ->
  $('.crop_image').cropper('zoom', 0.1)
$('.zoom-out').click ->
  $('.crop_image').cropper('zoom', -0.1).cropper('clear').cropper('crop')
$('.change_color').minicolors
  opacity: true
  change: (value, opacity) ->
    $('.cropper-modal').css('opacity', opacity)
    $('.cropper-modal').css('background-color', value)
<% else %>
$('.spinner').remove()
alert('Something goes wrong, we will find out. The picture will be deleted')
$('#picture_<%= @picture.id %>').remove()
<% end %>