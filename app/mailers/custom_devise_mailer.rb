class CustomDeviseMailer < Devise::Mailer   
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views
  layout 'user_mailer'
  before_action :set_logo
  def confirmation_instructions(record, token, opts={})
    if record.art_association?
      @token = token
      initialize_from_record(record)
      mail(to: resource.email,
           from: mailer_sender(devise_mapping),
           reply_to: mailer_reply_to(devise_mapping),
           subject: 'Art Association Confirmation Email') do |format|
        format.html { render 'devise/mailer/art_association_confirmation' }
      end
    else
      if record.unconfirmed_email.present? &&
         record.sign_in_count > 0
        UserMailer.to_old_email_about_email_changing(record).deliver
        opts[:subject] = t '.your_email_was_changed'
        opts[:template_name] = 'changing_email_confirmation'
      end
      super
    end
  end

  private

  def set_logo
    attachments.inline['jakira_color_logo.png'] =
      File.read('app/assets/images/jakira_color_logo.png')

    attachments.inline['jakira_white.png'] =
      File.read('app/assets/images/jakira_white.png')
  end

end