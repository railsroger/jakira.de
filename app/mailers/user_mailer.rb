class UserMailer < ActionMailer::Base
  default from: "JAKIRA.de <no-reply@jakira.de>"
  before_action :set_logo, except: [:send_newsletter]

  def purchase_notification(articles_order, recipient)
    @articles_order = articles_order
    @buyer = @articles_order.buyer
    @seller = @articles_order.seller
    @shop = @seller.shop
    attachments[t('.terms') + '.txt'] = {mime_type: 'application/x-gzip',
                                         content: @shop.terms}
    picture = @articles_order.article.main_picture
    if picture.present?
      attachments.inline[picture.image.file.filename] = File.read('public' + picture.image.url)
    else
      attachments.inline['placeholder.gif'] = File.read('app/assets/images/placeholder.gif')
    end
    if recipient == @seller
      template = "user_mailer/purchase_notification_to_seller"
    else
      template = "user_mailer/purchase_notification"
    end
    if @articles_order.confirmed?
      if recipient == @buyer
        subject = '.your_shopping_with_'
      else
        subject = '.shopping_with_'
      end
    else
      if recipient == @buyer
        subject = '.your_order_from_'
      else
        subject = '.order_from_'
      end
    end

    mail(to: recipient.email, subject: t(subject, shop: @shop.name)) do |format|
      format.html { render template }
    end
  end

  def password_changed(user)
    @user = user
    mail to: @user.email, subject: t('.your_new_password')
  end

  def new_shop(user)
    @user = user
    mail to: @user.email, subject: t('.subject')
  end

  def to_old_email_about_email_changing(user)
    @user = user
    mail to: @user.email, subject: t(".subject")
  end

  def articles_order_dispatched(articles_order)
    @articles_order = articles_order
    @article = @articles_order.article
    @user = @articles_order.buyer
    @shop = @articles_order.seller.shop
    picture = @article.main_picture
    if picture.present?
      attachments.inline[picture.image.file.filename] = File.read('public' + picture.image.url)
    else
      attachments.inline['placeholder.gif'] = File.read('app/assets/images/placeholder.gif')
    end
    mail to: @user.email,
      subject: t('.subject', shop: @shop.name)
  end

  def monthly_invoices_sending
    mail to: "shamardin.k@gmail.com", subject: "Mounthly invoices sending"
  end

  def send_invoice_to_user(invoice)
    @invoice = invoice

    path_to_file = 'public/' + @invoice.number + '.pdf'

    PDFKit.configure do |config|
      config.default_options = {
        page_size:      'Letter',
        margin_top:     '0.3in',
        margin_right:   '0.3in',
        margin_bottom:  '0.3in',
        margin_left:    '0.3in'
      }
    end

    kit = PDFKit.new(render_to_string(template: 'invoices/pdf_version',
                                        layout: "pdf_layout"))
    kit.to_file(path_to_file)

    attachments.inline["Invoice #{@invoice.number}.pdf"] = File.read(path_to_file)
    mail to: @invoice.user.email, subject: "Invoice " + @invoice.number 

    File.delete(path_to_file)
  end

  def send_newsletter(newsletter, email)
    @text = newsletter.text
    mail(to: email, subject: newsletter.subject, from: "newsletter@jakira.de") do |format|
      format.html { render layout: false }
    end
  end

  def new_message_notification(message)
    @message = message
    @recipient = @message.recipient
    @sender = @message.sender
    mail to: @recipient.email, subject: t('.subject', user: @sender.nickname)
  end

  def new_review_notification(user)
    @user = user
    @email = @user.email
    mail to: @email, subject: "New review"
  end

  def buyer_cancellation(user)
    @user = user
    @email = @user.email
    mail to: @email, subject: t('.subject')
  end

  def seller_cancellation(user)
    @user = user
    @email = @user.email
    mail to: @email, subject: t('.subject')
  end

  def new_order_message(order_message)
    @order_message = order_message
    @recipient = @order_message.recipient
    @sender = @order_message.user
    @articles_order = @order_message.articles_order
    @shop = @articles_order.seller.shop
    if @sender == @articles_order.seller
      user = @sender.shop.name
    else
      user = @sender.nickname
    end
    mail to: @recipient.email, subject: t('.subject', user: user)
  end

  def article_is_already_sold(articles_order)
    @articles_order = articles_order
    @user = @articles_order.buyer
    @shop = @articles_order.seller.shop
    picture = @articles_order.article.main_picture
    if picture.present?
      attachments.inline[picture.image.file.filename] = File.read('public' + picture.image.url)
    else
      attachments.inline['placeholder.gif'] = File.read('app/assets/images/placeholder.gif')
    end
    mail to: @user.email,
    subject: t('.subject', shop: articles_order.seller.shop.name)
  end

  def share_article(article, sender, email, text)
    @article = article
    @sender = sender
    @text = text
    picture = @article.main_picture
    if picture.present?
      attachments.inline[picture.image.file.filename] = File.read('public' + picture.image.url)
    else
      attachments.inline['placeholder.gif'] = File.read('app/assets/images/placeholder.gif')
    end
    mail to: email,
         subject: t('.subject', user: @sender.full_name),
         from: @sender.email_from
  end

  def new_voucher(voucher)
    @voucher = voucher

    path_to_file = 'public/' + @voucher.number + '.pdf'

    PDFKit.configure do |config|
      config.default_options = {
        page_size:      'Letter',
        margin_top:     '0in',
        margin_right:   '0in',
        margin_bottom:  '0in',
        margin_left:    '0in'
      }
    end

    kit = PDFKit.new(render_to_string(template: 'vouchers/pdf_version',
                                        layout: false))
    kit.to_file(path_to_file)

    attachments.inline["Voucher #{@voucher.number}.pdf"] = File.read(path_to_file)
    
    mail to: @voucher.email, subject: 'New voucher'

    File.delete(path_to_file)
  end

  def registration_without_password(member, password)
    @member = member
    @password = password
    mail to: @member.email,
      subject: 'Your account data'
  end

  def send_business_cards_order_to_admin(business_cards_order)
    @business_cards_order = business_cards_order
    @buyer = @business_cards_order.buyer
    file_name = @business_cards_order.value.to_s.rjust(2, '0')
    
    business_cards_thumbs = 'public/business_cards_thumbs/'
    Dir.mkdir(business_cards_thumbs) unless File.exists?(business_cards_thumbs)
    
    directory = business_cards_thumbs + @buyer.id.to_s + '/'
    Dir.mkdir(directory) unless File.exists?(directory)
    
    path_to_temporary_pdf = directory + file_name + '.pdf'
    pdftk = PdfForms.new('/usr/bin/pdftk')
    pdftk.fill_form ('public/business_cards_templates/' + file_name + '.pdf' ),
                      path_to_temporary_pdf,
                      { "Shopname in Großbuchstaben" => @buyer.shop.name.mb_chars.upcase.to_s,
                        "Name Kunde" => @buyer.full_name_occupation,
                        "Adresse & Kontaktdaten" => @buyer.address_and_contact_data_for_business_card,
                        "Adresse" => @buyer.address_for_business_card,
                        "Kontaktdaten" => @buyer.contact_data_for_business_card},
                      flatten: true
    
    attachments.inline[file_name + '.pdf'] = File.read(path_to_temporary_pdf)
    File.delete path_to_temporary_pdf
    
    mail to: (Rails.env.production? ?
             'yasser.chehade@jakira.de' :
             'kirill.shamardin@jakira.de'),
      subject: "New business cards order"
  end

  def send_contact_form(contact)
    @first_name = contact.first_name
    @last_name = contact.last_name
    @email = contact.email
    @theme = contact.theme
    @message = contact.message

    mail to: (Rails.env.production? ?
             'yasser.chehade@jakira.de' :
             'shamardin.k@gmail.com'),
      subject: t('.subject')
  end

  def order_was_canceled(articles_order)
    @articles_order = articles_order
    @user = articles_order.buyer
    mail to: @user.email,
      subject: "Your order was canceled"
  end

  def send_payment_reminder(articles_order)
    @articles_order = articles_order
    @buyer = @articles_order.buyer
    mail to: @buyer.email,
      subject: "Your order still awaits payment"
  end

  private

  def set_logo
    attachments.inline['jakira_color_logo.png'] = File.read('app/assets/images/jakira_color_logo.png')
    attachments.inline['jakira_white.png'] = File.read('app/assets/images/jakira_white.png')
  end
end