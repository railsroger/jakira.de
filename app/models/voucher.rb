class Voucher < ActiveRecord::Base

  enum payment_type: [:bank_transfer, :paypal, :paydirekt]
  enum status: [:sent, :canceled]

  attr_accessor :agreement

  monetize :value_cents
  monetize :used_amount_cents
  monetize :remaining_amount_cents

  before_create :add_remaining_amount
  before_create :add_number

  has_many :paypal_transactions
  has_many :articles_orders
  belongs_to :address, inverse_of: :voucher

  accepts_nested_attributes_for :address

  validates_presence_of :from,
                        :for,
                        :value

  validates_inclusion_of :value,
    in: [25, 50, 75, 100, 125, 150, 200, 250, 500, 1000].collect {|n| n.to_money}
 
  validates_inclusion_of :payment_type,
    in: :valid_payment_types,
    on: :create

  validates :email, email: true

  validates_confirmation_of :email

  validates_acceptance_of :agreement

  alias_attribute :recipient, :for

  scope :paid, -> { where(paid: true) }

  scope :search, lambda { |text|
    if text
      where('LOWER(vouchers.email) LIKE :search
          OR LOWER(vouchers.code) LIKE :search',
          search: "%#{text.downcase}%")
    else
      all
    end
  }


  def self.values
    [25, 50, 75, 100, 125, 150, 200, 250, 500, 1000]
  end

  def cancel
    update(status: 'canceled')
  end

  def confirm_payment
    mark_as_paid if !paid?
    create_code if !code?
    send_code
  end

  def last_sent_on
    sent_on.last
  end

  def sent_on_dates
    (sent_on.collect { |datetime| datetime.to_date }).uniq
  end

  protected

  def valid_payment_types
    MainVoucherData.first.payment_types
  end

  def mark_as_paid
    update(paid_on: Time.now, paid: true)
  end

  def create_code
    update(code: CouponCode.generate(parts: 2))
  end

  def send_code
    UserMailer.new_voucher(self).deliver
    update(status: 'sent')
    if sent_on.present?
      update(sent_on: (sent_on + [Time.now]))
    else
      update(sent_on: [Time.now])
    end
  end

  private

  def add_remaining_amount
    self.remaining_amount = self.value
  end

  def add_number
    last_voucher = Voucher.last
    if last_voucher.present?
      new_number = last_voucher.number[3..-1].to_i + 1
    else
      new_number = 1
    end
    self.number = "JKG" + new_number.to_s.rjust(7, '0')
  end

end