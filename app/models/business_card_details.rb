class BusinessCardDetails < ActiveRecord::Base

  translates :price,
             :quantity,
             :format,
             :extent,
             :dyeing,
             :paper,
             :gram_matur,
             :printing_process,
             :paintwork

end