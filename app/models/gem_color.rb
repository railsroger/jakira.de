class GemColor < ActiveRecord::Base
  has_and_belongs_to_many :articles, touch: true

  translates :name

  scope :order_by_name, -> { with_translations(I18n.locale)
  							.order('gem_color_translations.name') }
end
