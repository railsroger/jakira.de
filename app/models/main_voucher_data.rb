class MainVoucherData < ActiveRecord::Base
  has_one :bank_transfer_data,
    as: :bank_transfer_datable,
    inverse_of: :bank_transfer_datable,
    dependent: :destroy
                               
  has_one :paypal_data,
    as: :paypal_datable,
    inverse_of: :paypal_datable,
    dependent: :destroy

  validates_associated :bank_transfer_data, :paypal_data

  accepts_nested_attributes_for :bank_transfer_data
  accepts_nested_attributes_for :paypal_data


  def have_payment_types?
    payment_types.present?
  end

  def payment_types
    [('bank_transfer' if bank_transfer),
     ('paypal'        if paypal),
     ('paydirekt'     if paydirekt)].compact
  end

end
