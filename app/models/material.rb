class Material < ActiveRecord::Base

  translates :name

  has_ancestry
  has_and_belongs_to_many :articles, touch: true

  validates :name, presence: true

  def label
    name.downcase + '_' + id.to_s
  end
end
