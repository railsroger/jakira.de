class ShippingReadiness < ActiveRecord::Base
  enum readiness: [:immediately, :in_some_days]

  belongs_to :article, touch: true
  belongs_to :shop

  validates_presence_of :readiness,
    if: -> {article.present? ||
              (shop.present? && shop.set_shipping == '1')}
  
  validate :validate_duration
  
  private

  def validate_duration
    if in_some_days?
      if from.nil? ||
        from < 0 ||
        from == 0 ||
        (to.present? && (from >= to && to != 0))
        errors.add(:duration, I18n.t("activerecord.errors.models.shipping_method.attributes.validate_duration.valid") )
        errors.add(:from, nil)
      end
      if to.nil? ||
        to <= 0 ||
        (from.present? && from >= to)
        errors.add(:duration, I18n.t("activerecord.errors.models.shipping_method.attributes.validate_duration.valid") )
        errors.add(:to, nil)
      end
    end
  end
end