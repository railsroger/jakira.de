class BankTransferData < ActiveRecord::Base

  belongs_to :bank_transfer_datable, polymorphic: true, touch: true

  validates_presence_of :account_holder,
                        :iban,
                        :swft_bic,
                          if: :include_bank_transfer_data?

  def include_bank_transfer_data?
    self.bank_transfer_datable_type != 'MainVoucherData' &&
    self.bank_transfer_datable.payment_types.map(&:name).include?("wire") ||
    self.bank_transfer_datable_type == 'MainVoucherData' &&
    self.bank_transfer_datable.bank_transfer
  end
  
end