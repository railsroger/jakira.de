class Invoice < ActiveRecord::Base
  enum status: [:check, :paid, :open, :canceled, :credited]

  monetize :price_cents
  before_save :add_number

  belongs_to :user
  has_many :articles_orders


  scope :search, lambda { |text|
    if text
      where('LOWER(number) LIKE ?', "%#{text.downcase}%")
    else
      all
    end
  }
  
  private

  def add_number
    last_invoice_number = Invoice.last.nil? ? 'R0000000' : Invoice.last.number
    if self.number == nil
      self.number = "R" + (last_invoice_number[1, 7].to_i + 1).to_s.rjust(7, "0")
    end
  end

end
