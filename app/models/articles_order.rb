class ArticlesOrder < ActiveRecord::Base
  enum status: [:awaiting_confirmation,
                :confirmed,
                :paid,
                :sent]

  monetize :voucher_discount_cents

  attr_accessor :voucher_code,
                :set_pickup

  before_validation :set_pickup_method,
                    if: -> { set_pickup.present? }

  before_validation :set_pickup_if_cash,
                    if: -> {
                      payment_type.present? &&
                        payment_type.name == 'cash'
                    }

  after_validation :accept_voucher_code,
                   if: -> { new_voucher.present? }
  after_create :add_ratings
  before_save :update_around_status,
              if: -> { status_changed? }

  belongs_to :original_article, class_name: 'Article'
  belongs_to :article
  belongs_to :order
  has_one :purchase, through: :order
  has_one :buyer, through: :purchase, source: :user
  has_one :seller, through: :order, source: :seller
  belongs_to :payment_type
  belongs_to :shipping_method
  belongs_to :invoice
  belongs_to :voucher

  belongs_to :shipping_address, class_name: 'Address'
  belongs_to :payment_address, class_name: 'Address'

  has_many :paypal_transactions
  has_one :rating_from_seller,
          -> { where(from: Rating.froms[:from_seller]) },
          class_name: 'Rating'
  has_one :rating_from_buyer,
          -> { where(from: Rating.froms[:from_buyer]) },
          class_name: 'Rating'
  has_many :order_messages

  validates_presence_of :article,
                        :original_article,
                        :order

  validate :has_no_voucher_yet,
           :voucher_code_is_valid,
           if: -> { !voucher_code.nil? }

  validate :voucher_has_remaining_amount,
           :voucher_is_paid,
           :voucher_is_not_canceled,
           :order_was_already_paid,
           if: -> { new_voucher.present? }

  validates_acceptance_of :use_shipping_address_for_payment_address,
                          accept: false,
                          if: -> {
                            !shipping_address.all_filled? || pickup?
                          }

  validate :last_name_and_pickup_are_excluded,
           if: -> { pickup? && payment_type.present? }

  validate :cash_only_with_pickup,
           if: -> { payment_type.present? }

  validates :payment_type,
            inclusion: { in: :available_payment_types },
            allow_nil: true

  validates_inclusion_of :status,
                         in: :valid_statuses,
                         if: -> { status_was.present? }

  validate :unchangeable

  validate :cant_be_finished,
           if: -> {
             finished_by_seller_changed? ||
               finished_by_buyer_changed?
           }

  scope :created_at, -> { order(created_at: :desc) }

  scope :not_nil, -> { where.not(status: nil).desc }

  scope :desc, -> { order(id: :desc) }

  scope :canceled,     -> { not_nil.where(canceled: true ) }
  scope :not_canceled, -> { not_nil.where(canceled: false ) }

  scope :not_finished_by_seller,
        -> { not_canceled
             .where(finished_by_seller: false) }

  scope :not_finished_by_buyer,
        -> { not_canceled
             .where(finished_by_buyer: false) }

  scope :finished_by_seller,
        -> { not_canceled
             .where(finished_by_seller: true) }

  scope :finished_by_buyer,
        -> { not_canceled
             .where(finished_by_buyer: true) }

  scope :seller_transactions,
        ->(user_id) {
          joins(order: :purchase)
            .order(created_at: :desc)
            .where('orders.seller_id = ?', user_id)
            .where.not(status: nil)
        }

  scope :buyer_transactions,
        ->(user_id) {
          joins(order: :purchase)
            .order(created_at: :desc)
            .where('purchases.user_id = ?', user_id)
            .where.not(status: nil)
        }

  scope :transactions,
        ->(user_id) {
          joins(order: :purchase)
            .order(created_at: :desc)
            .where('orders.seller_id = ?
                   OR purchases.user_id = ?',
                   user_id, user_id)
            .where.not(status: nil)
        }

  def quarter_of_sale
    quarter =
      case confirmed_at.month
      when 1, 2, 3
        1
      when 4, 5, 6
        2
      when 7, 8, 9
        3
      when 10, 11, 12
        4
      end
    "Q#{quarter}/#{confirmed_at.year}"
  end

  def shipping_method_price
    shipping_method.present? ? shipping_method.price : 0.to_money
  end

  alias shipping_price shipping_method_price

  def price_with_discount
    article.price - voucher_discount
  end

  def total_price
    price_with_discount + shipping_method_price
  end

  def pickup?
    shipping_method.present? && shipping_method.name == 'pickup'
  end

  def original_article_not_changed?
    created_at > original_article.updated_at
  end

  def ordered_article
    original_article_not_changed? ? original_article : article
  end

  delegate :available_countries,
           :available_countries_except_forbidden,
           to: :article

  def delete_voucher_discount
    if voucher.present?
      voucher = voucher
      voucher.remaining_amount = voucher.remaining_amount + voucher_discount
      voucher.save
    end
    articles_order = self
    articles_order.voucher_discount = 0
    articles_order.voucher_id = nil
    articles_order.save
  end

  def available_payment_types
    article.payment_types.order(id: :desc)
           .where.not(name: [('cash' if !pickup_available?),
                            ('last_name' if only_pickup?)])
  end

  def actual_shipping_address
    shipping_address
  end

  def actual_payment_address
    if use_shipping_address_for_payment_address?
      shipping_address
    else
      payment_address
    end
  end

  def pickup_available?
    article.pickup.present?
  end

  def filled?
    (actual_shipping_address.valid? || pickup?) &&
      actual_payment_address.valid? &&
      payment_type.present?
  end

  def find_shipping_method(method)
    ordered_article.shipping_methods.find_by(name: method)
  end

  def set_shipping_method
    free_within_germany = find_shipping_method('free_within_germany')
    within_germany = find_shipping_method('within_germany')
    self.shipping_method_id =
      if actual_shipping_address.country == 'DE' &&
         (free_within_germany.present? || within_germany.present?)
        if free_within_germany.present?
          free_within_germany.id
        else
          within_germany.id
        end
      elsif ISO3166::Country.all.select(&:in_eu?).map(&:alpha2)
                            .include?(actual_shipping_address
                                                    .country) &&
            find_shipping_method('within_eu').present?
        find_shipping_method('within_eu').id
      else
        find_shipping_method('worldwide').id
      end
    save
  end

  def new_voucher
    Voucher.find_by(code: voucher_code)
  end

  def last_name?
    payment_type.present? && payment_type.name == 'last_name'
  end

  def only_pickup?
    article.shipping_methods.map(&:name) == ['pickup']
  end

  def close_by(user)
    if seller == user
      'seller' if update(finished_by_seller: true)
    elsif buyer == user
      'buyer' if update(finished_by_buyer: true)
    end
  end

  def duration_range
    return if pickup?
    shipping_method.duration_from.to_s + '-' + shipping_method.duration_to.to_s
  end

  def unread_messages?(user)
    order_messages.by_recipient(user).unread.count > 0
  end

  def cancel!
    update(canceled: true, canceled_at: Time.now)
    original_article.online!
    UserMailer.order_was_canceled(self).deliver
    canceled?
  end

  def disabled_statuses
    ArticlesOrder.statuses.select do |k, v|
      v <= ArticlesOrder.statuses[status]
    end.keys
  end

  def can_be_finished_by?(user)
    !canceled? && sent? &&
      (user == buyer ?
        !finished_by_buyer? :
        !finished_by_seller)
  end

  def two_weeks_after_confirmation?
    confirmed? &&
      confirmed_at + 2.weeks < Time.now
  end

  private

  def cant_be_finished
    return unless canceled? || !sent?
    if finished_by_seller_changed?
      errors.add(:finished_by_seller, :cant_be_finished)
    end
    if finished_by_buyer_changed?
      errors.add(:finished_by_buyer, :cant_be_finished)
    end
  end

  def valid_statuses
    ArticlesOrder.statuses.select do |k, v|
      v >= ArticlesOrder.statuses[status_was]
    end
  end

  def has_no_voucher_yet
    return unless voucher.present?
    errors.add(:voucher, 'This order already has discount')
  end

  def voucher_code_is_valid
    return if new_voucher.present?
    errors.add(:voucher, 'The voucher code is invalid')
  end

  def voucher_has_remaining_amount
    return if new_voucher.remaining_amount > 0
    errors.add(:voucher, 'The voucher has no remaining amount')
  end

  def voucher_is_paid
    return if new_voucher.paid?
    errors.add(:voucher, 'The voucher was not paid')
  end

  def voucher_is_not_canceled
    return unless new_voucher.canceled?
    errors.add(:voucher, 'The voucher was canceled')
  end

  def order_was_already_paid
    return unless paid?
    errors.add(:voucher, 'The order was already paid ')
  end

  def accept_voucher_code
    self.voucher = new_voucher
    price = article.price
    if price >= voucher.remaining_amount
      self.voucher_discount = voucher.remaining_amount
      voucher.update(remaining_amount: 0.to_money)
    else
      self.voucher_discount = price
      voucher.update(remaining_amount: voucher.remaining_amount - price)
    end
    self.voucher_accepted_at = Time.now
  end

  def set_pickup_method
    if set_pickup == '1'
      self.use_shipping_address_for_payment_address = false
    end
    self.shipping_method = set_pickup == '1' ? article.pickup : nil
  end

  def set_pickup_if_cash
    if set_pickup.present? && set_pickup == '0'
      self.shipping_method = nil
      self.payment_type = nil
    else
      self.use_shipping_address_for_payment_address = false
      self.shipping_method = article.pickup
    end
  end

  def last_name_and_pickup_are_excluded
    return unless payment_type.name == 'last_name'
    errors.add(:pickup,
               'Last Name is an improper payment method for pickup')
  end

  def cash_only_with_pickup
    return unless payment_type.name == 'cash' && !pickup?
    errors.add(:pickup,
               'Cash payment method is available only for pickup')
  end

  def add_ratings
    create_rating_from_seller
    create_rating_from_buyer
  end

  def update_around_status
    case status
    when 'awaiting_confirmation'
      UserMailer.purchase_notification(self, buyer).deliver
      UserMailer.purchase_notification(self, seller).deliver
    when 'confirmed'
      original_article.update_columns(status:  Article.statuses[:sold],
                                      sold_at: Time.now)
      self.confirmed_at = Time.now
      UserMailer.purchase_notification(self, buyer).deliver
      UserMailer.purchase_notification(self, seller).deliver
    when 'paid'
      self.confirmed_at = Time.now if confirmed_at.nil?
      self.paid_at = Time.now
    when 'sent'
      self.confirmed_at = Time.now if confirmed_at.nil?
      self.paid_at = Time.now if paid_at.nil?
      self.sent_at = Time.now
      UserMailer.articles_order_dispatched(self).deliver
    end
  end

  def unchangeable
    return unless (canceled? && !canceled_changed?) || canceled_was == true
    errors.add(:canceled, :forbidden)
  end
end