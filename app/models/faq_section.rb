class FaqSection < ActiveRecord::Base
  
  has_many :faq_questions, dependent: :destroy

  translates :name

  validates :name, length: {minimum: 3, maximum: 255}

end
