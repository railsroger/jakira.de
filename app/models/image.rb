class Image < ActiveRecord::Base
  enum kind: [:user_pictures,
              :article_example_pictures,
              :shop_main_pictures,
              :shop_pictures,
              :article_pictures]

  mount_uploader :image, ImageUploader

  belongs_to :uploader,
             class_name: 'User',
             foreign_key: :uploader_id

  belongs_to :article,
             class_name: 'Article',
             foreign_key: :parent_id

  attr_accessor :x, :y, :width, :height, :rotate
  after_update :crop_image, if: :cropping?

  def main?
    article.present? &&
      article.main_picture_id.present? &&
      article.main_picture_id == id
  end

  def parent
    case kind
    when 'user_pictures'
      User.find_by(id: parent_id)
    when 'article_example_pictures'
      Shop.find_by(id: parent_id)
    when 'shop_main_pictures'
      Shop.find_by(id: parent_id)
    when 'shop_pictures'
      Shop.find_by(id: parent_id)
    when 'article_pictures'
      Article.find_by(id: parent_id)
    end
  end

  def zoomable?
    small_width = FastImage.size(self.image.middle.path)[0]
    small_height = FastImage.size(self.image.middle.path)[1]
    original_width = FastImage.size(self.image.path)[0]
    original_height = FastImage.size(self.image.path)[1]
    small_width < original_width &&
    small_height < original_height
  end

  def cropping?
    !x.blank? && !y.blank? && !width.blank? && !height.blank?
  end

  private

  def crop_image
    image.recreate_versions!
  end
end
