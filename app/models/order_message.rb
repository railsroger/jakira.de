class OrderMessage < ActiveRecord::Base
  after_create :send_notification

  belongs_to :articles_order
  belongs_to :user

  validates_presence_of :text, :articles_order

  validate :order_is_not_canceled

  scope :unread_in_sales,
        ->(current_user_id) { by_recipient(User.find_by(id: current_user_id))
                              .unread
                              .joins(articles_order: :order)
                              .where(orders: {seller_id: current_user_id}) }

  scope :unread_in_purchases,
        ->(current_user_id) { by_recipient(User.find_by(id: current_user_id))
                              .unread
                              .joins(articles_order: :purchase)
                              .where(purchases: {user_id: current_user_id}) }

  scope :by_recipient,
        ->(current_user) { where.not(user_id: current_user.id) }

  scope :unread, -> { where(read: false) }

  def sender
    user
  end

  def recipient
    ([articles_order.buyer, articles_order.seller] - [sender]).first
  end

  def sender_is_seller?
    articles_order.seller == sender
  end

  def anchor_hash
    {anchor: ('order_' + articles_order.id.to_s)}
  end

  private
  
  def send_notification
    UserMailer.new_order_message(self).deliver
  end

  def order_is_not_canceled
    return unless articles_order.canceled?
    errors.add(:articles_order, :canceled)
  end
end