class ArtAssociation < User

  before_save :set_role

  before_create :add_membership_number_to_art_association

  belongs_to :address
  has_many :members, class_name: "User"

  def membership_number_with_formatting
    "VM" + membership_number.to_s.rjust(5, '0') if membership_number.present?
  end

  def import_members_from_xls(file)
    spreadsheet = Spreadsheet.open file.path
    sheet = spreadsheet.worksheet(0)
    headers = spreadsheet.worksheet(0).row(0)

    (1..(sheet.rows.count - 1)).each do |i|
      row = Hash[[headers, sheet.row(i)].transpose]
      user_attributes = row.select { |k,v| User.column_names.include?(k) }
      address_attributes = row.select { |k,v| Address.column_names.include?(k) }
      generated_password = Devise.friendly_token.first(8)
      member = self.members.new(user_attributes.merge({password: generated_password}))
      member.build_address(address_attributes)
      member.skip_confirmation!
      if member.save
        UserMailer.send_password_to_new_art_association_member(member, generated_password).deliver
      end
    end
  end

  private

  def set_role
    self.role = :art_association
  end

  def add_membership_number_to_art_association
    last_art_association = ArtAssociation.last
    last_membership_number = last_art_association.present? ? last_art_association.membership_number + 1 : 1
    self.membership_number = last_membership_number
  end


end