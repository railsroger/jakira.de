class PaypalData < ActiveRecord::Base

  belongs_to :paypal_datable, polymorphic: true, touch: true

  validates_presence_of :client_id,
                        :client_secret,
                          if: :include_paypal?

  def include_paypal?
    self.paypal_datable_type != 'MainVoucherData' &&
    (self.paypal_datable.payment_types.map(&:name).include?("paypal") ||
     self.paypal_datable.payment_types.map(&:name).include?("credit_card")) ||
    self.paypal_datable_type == 'MainVoucherData' &&
    self.paypal_datable.paypal
  end

end