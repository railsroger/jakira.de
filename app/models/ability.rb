# This class implements the CanCan access control for resources.
class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities

    # user ||= User.new # guest user (not logged in)

    if user.admin?
      can :manage, :admin
    elsif user.art_association?
      can :manage, [:art_association_members,
                    :art_association]
    end

    can [:index,
         :register,
         :create,
         :become_a_member], :art_association

    can :manage, :business_card if user.has_shop?

    can [:index,
         :create], ShoppingBasket

    can [:update,
         :destroy], user.shopping_baskets

    can [:index,
         :create], Favorite

    can :destroy, user.favorites

    # ArticlesOrder
    can [:destroy,
         :use_shipping_address,
         :set_pickup_as_shipping_method,
         :set_payment_type], user.articles_orders do |articles_order|
      !articles_order.purchase.confirmed?
    end

    can [:use_voucher], user.articles_orders.confirmed do |articles_order|
      articles_order.purchase.confirmed?
    end

    can [:paypal_payment],
      user.articles_orders.confirmed do |articles_order|
      articles_order.purchase.confirmed? &&
        articles_order.payment_type.name == 'paypal'
    end

    can [:bank_details],
      user.articles_orders.confirmed do |articles_order|
      articles_order.purchase.confirmed? &&
        articles_order.payment_type.name == 'wire'
    end

    can [:closure_confirmation,
         :close],
      user.articles_orders.not_finished_by_buyer.sent +
        user.sales.not_finished_by_seller.sent

    can :mark_order_messages_as_read,
      user.articles_orders + user.sales

    can [:cancel,
         :update_status], user.sales.not_finished_by_seller

    can :send_payment_reminder,
        user.sales.not_finished_by_seller do |articles_order|
      articles_order.two_weeks_after_confirmation?
    end

    # must be checked carefully later
    can [:paypal_payment,
         :credit_card_payment], ArticlesOrder

    # Purchase
    can [:new], Purchase if user.shopping_baskets.present?

    can [:addresses_and_payment_types,
         :check_order,
         :complete], user.purchases.not_confirmed

    can :show, user.purchases.confirmed

    can [:open, :completed], Purchase if user.regular?

    # Address
    can :load_country_code, Address

    can [:update_purchase_address], Address do |address|
      (address.articles_order.present? &&
        address.articles_order.buyer.id == user.id &&
        !address.articles_order.purchase.confirmed?)
    end

    # Rating
    can :update, Rating do |rating|
      rating.from_buyer? && rating.articles_order.buyer == user ||
        rating.from_seller? && rating.articles_order.seller == user
    end

    # OrderMessage
    can :create, OrderMessage do |order_message|
      order_message.articles_order.present? &&
        [order_message.articles_order.buyer,
        order_message.articles_order.seller].include?(user)
    end

    can :show, OrderMessage do |order_message|
      order_message.recipient == user
    end

    # Order
    can [:open,
         :completed,
         :canceled], Order if user.seller?

    # Category
    can :show, Category

    # Picture
    can [:manage], user.uploaded_pictures

    ########################################################

    # Article AC
    can [:read, :search], Article # Everybody can read Articles
    can :update, Article, user_id: user.id # Only the Owner can edit his Articles
    can :create, Article do |article|
      user.has_shop?
    end

    # Shop AC
    can [:read, :create], Shop # Everybody can read Shops
    can [:update, :change_image], Shop, user_id: user.id

    # ShippingMethod AC
    can :manage, ShippingMethod do |shipping_method|
      shipping_method.article.user_id == user.id || shipping_method.article.shop.id == user.shop.id
    end

    # Message AC
    can [:read, :destroy], Message do |message|
      message.owner_id == user.id
    end

    can [:create, :reply], Message

    # TODO
    can :manage, Document

    can :access, :ckeditor

    can [:read, :create, :destroy], Ckeditor::Picture
    can [:read, :create, :destroy], Ckeditor::AttachmentFile
  end
end
