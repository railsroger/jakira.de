class Ckeditor::Picture < Ckeditor::Asset
  mount_uploader :data, CkeditorPictureUploader, mount_on: :data_file_name

  # def url_content
  #   url(:content)
  # end

  def url_content
    if Rails.env.production?
        host_url="http://jakira.de"
    else
        host_url="http://lvh.me:3000"
    end
    host_url + url(:content)
  end

end
