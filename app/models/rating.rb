class Rating < ActiveRecord::Base
  attr_accessor :submit

  enum from: [:from_buyer, :from_seller]

  after_validation :set_sender,
                   :set_receiver,
                   if: -> { submit.present? }

  before_save :finish,
              if: -> { submit.present? }

  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :receiver, class_name: 'User', foreign_key: 'receiver_id'
  belongs_to :articles_order

  validates_presence_of :articles_order

  validates :score,
            presence: true,
            inclusion: { in: 1..5 },
            if: -> { submit.present? }

  validates_length_of :text, maximum: 300

  validate :changeable,
           :order_was_not_canceled

  scope :finished, -> { where(finished: true) }

  def disabled?
    finished? || articles_order.canceled?
  end

  private

  def set_sender
    self.sender = from_buyer? ? articles_order.buyer : articles_order.seller
  end

  def set_receiver
    self.receiver = from_buyer? ? articles_order.seller : articles_order.buyer
  end

  def finish
    self.finished = true
  end

  def changeable
    return unless finished?
    errors.add(:finished, :completed)
  end

  def order_was_not_canceled
    return unless articles_order.canceled?
    errors.add(:articles_order, :canceled)
  end

end
