class Document < ActiveRecord::Base
  #attr_accessible :gallery_id, :name, :image, :remote_image_url
  belongs_to :article
  belongs_to :shop
  belongs_to :articles_order

  mount_uploader :document, DocumentUploader

  validates :name, presence: true, if: lambda{type.blank?}

end

