class Newsletter < ActiveRecord::Base
  enum receivers: [:everyone,
                   :sellers,
                   :buyers]

  enum interval:  [:weekly,
                   :monthly,
                   :bimonthly,
                   :quarterly,
                   :half_yearly,
                   :yearly ]

  attr_accessor :email
  attr_accessor :to_test_email

  validates_presence_of :email, if: -> { to_test_email.present?}

  validates_format_of :email,
    with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
      if: -> { email.present? }

  validates_presence_of :receivers, if: -> { !to_test_email.present? }

  validates_presence_of :subject,
                        :text

  # validates_presence_of :delivery_date,
  #                         if: -> { delivery_date_checked }

  validates_presence_of :template_name,
                        :interval,
                          if: -> { template }

  validates :delivery_date,
              date: { after: Proc.new { Date.current } },
                if: -> { delivery_date_checked }

  validates :first_execution,
              date: { after: Proc.new { Date.current } },
                if: -> { template }

  validates :last_execution,
              date: { after: Proc.new { Date.current },
                      after: :first_execution },
                if: -> { template }

  validates :exposing_from,
              date: { after: Proc.new { Date.current } },
                if: -> { template }

  validates :exposing_to,
              date: { after: Proc.new { Date.current },
                      after: :exposing_from },
                if: -> { template }

  scope :templates, -> { where(template: true) }

  scope :actual_templates, -> { templates
                               .where("last_execution > ?", Time.now) }

  def self.send_newsletters
    Newsletter.actual_templates.each do |newsletter|
      period = case newsletter.interval
        when "weekly"
          1.week
        when "monthly"
          1.month
        when "bimonthly"
          2.month
        when "quarterly"
          3.month
        when "half_yearly"
          6.month
        when "yearly"
          1.year
      end
      array_of_dates = newsletter.first_execution.to_i..newsletter.last_execution.to_i
      if array_of_dates.step(period).map { |d| Time.at(d).to_date }.include?(Time.now.to_date)
        User.send_newsletters(newsletter)
      end
    end
  end

  


end
