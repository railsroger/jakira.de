class Shop < ActiveRecord::Base
  enum order_confirmation: [:automatically,
                            :manually,
                            :with_exception]

  enum articles_type: [:physical, :digital, :service]

  enum package: [:smart, :basic, :professional]

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  attr_accessor :withdrawal_form_id,
                :terms_and_revocation_are_chosen,
                :set_shipping,
                :set_payment_types,
                :accept_terms,
                :professional,
                :payment_authorization,
                :subscription,
                :main_picture_id

  before_validation :add_main_picture,
                    unless: -> { main_picture_id.nil? }

  monetize :monthly_charge_cents

  before_validation :set_proper_subdomain
  before_save :add_withdrawal_form

  belongs_to :user, touch: true
  belongs_to :address, inverse_of: :shop

  has_and_belongs_to_many :payment_types

  has_one :personal_bank_data
  has_one :bank_transfer_data, 
            as: :bank_transfer_datable, 
            inverse_of: :bank_transfer_datable,
            dependent: :destroy
  has_one :paypal_data,
            as: :paypal_datable,
            inverse_of: :paypal_datable,
            dependent: :destroy
  has_one :withdrawal_form
  has_one :shipping_readiness,
            inverse_of: :shop,
            dependent: :destroy
  
  has_many :shipping_methods,
              as: :shipping_methodable,
              inverse_of: :shipping_methodable
  has_many :documents
  has_many :pictures,
           -> { shop_pictures.limit(2) },
           class_name: 'Image',
           foreign_key: :parent_id

  has_one :main_picture,
          -> { shop_main_pictures },
          class_name: 'Image',
          foreign_key: :parent_id

  has_many :article_example_pictures,
           -> { article_example_pictures },
           class_name: 'Image',
           foreign_key: :parent_id

  has_many :articles, through: :user
  has_many :favorite_shops, dependent: :destroy

  accepts_nested_attributes_for :user,
                                :address,
                                :shipping_methods,
                                :personal_bank_data,
                                :shipping_readiness

  accepts_nested_attributes_for :paypal_data,
                                  allow_destroy: true
  
  accepts_nested_attributes_for :bank_transfer_data,
                                  allow_destroy: true

  validates_associated :address
  validates_associated :personal_bank_data

  validates_uniqueness_of :subdomain

  validates :accept_terms,
            :professional,
            :payment_authorization,
            :subscription,
              acceptance: true,
              allow_nil: false,
              on: :create

  validates :payment_authorization,
              acceptance: true,
              allow_nil: true,
              on: :update,
              unless: :is_payment_authorization_needed?

  validates :payment_authorization,
              acceptance: true,
              allow_nil: false,
              on: :update,
              if: :is_payment_authorization_needed?

  validates_presence_of :name,
                        :package,
                        :subdomain,
                        :imprint,
                        :address,
                        :personal_bank_data

  validates_presence_of :terms,
                          if: -> {terms_and_revocation_are_chosen == '1'}

  validates_presence_of :payment_types,
                          if: -> {set_payment_types == '1'}

  validates_presence_of :shipping_methods,
                          if: -> {set_shipping == '1'}

  scope :not_deleted, -> { where(deleted: false) }

  def slug_candidates
    [
      :name,
      [:name, self.user.nickname],
      [:name, :id]
    ]
  end

  def active_subscription?
    Subscription.where(user_id: self.user_id).where(state: 'active').count > 0
  end

  def is_payment_authorization_needed?
    package_changed? || personal_bank_data.changed?
  end

  def no_shipping_to_the_following_countries
    if no_shipping_to_the_countries.present?
      countries = no_shipping_to_the_countries
      countries.delete("")
      array = countries.collect do |c|
        country = ISO3166::Country[c]
        country.translations[I18n.locale.to_s] || country.name if country.present?
      end
      array.join(', ')
    end
  end

  def main_picture_url
    main_picture.present? ? main_picture.image_url : 'placeholder.gif'
  end

  def main_picture_small_url
    main_picture.present? ? main_picture.image.small.url : 'placeholder.gif'
  end

  def picture_ids=(ids)
    super ids
    Image.where(id: ids).each do |picture|
      picture.update(kind: :shop_pictures)
    end
  end

  def article_example_picture_ids=(ids)
    super ids
    Image.where(id: ids).each do |picture|
      picture.update(kind: :article_example_pictures)
    end
  end

  private

  # This creates an default_article that will belong to this user
  def add_default_article
    self.default_article = DefaultArticle.create
  end

  def self.get_overall_monthly_charge
    if Shop.where(use_personal_monthly_charge: false).present?
      Shop.where(use_personal_monthly_charge: false).first.monthly_charge
    else
      14.9.to_money
    end
  end

  def set_proper_subdomain
    shop_name_array = []
    self.name.downcase.split('').each do |letter|
      case letter
      when " "
        shop_name_array.push("-")
      when "ä"
        shop_name_array.push('ae')
      when "ö"
        shop_name_array.push('oe')
      when "ü"
        shop_name_array.push('ue')
      else
        if /[^\W_]|[\-]/.match(letter) == nil
          shop_name_array.push('')
        else
          shop_name_array.push(letter)
        end
      end
    end
    self.subdomain = shop_name_array.join
  end

  def add_withdrawal_form
    if withdrawal_form_id.present?
      self.withdrawal_form = WithdrawalForm.find_by(id: withdrawal_form_id)
    elsif withdrawal_form_id == ""
      self.withdrawal_form.destroy if self.withdrawal_form.present?
    end
  end

  def add_main_picture
    self.main_picture = Image.find_by(id: main_picture_id)
  end
end
