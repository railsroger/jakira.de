class PaymentType < ActiveRecord::Base

  has_and_belongs_to_many :articles, touch: true
  has_and_belongs_to_many :shops
  has_many :shopping_baskets
  has_many :article_orders

  def displayed_name
    I18n.t('payment_type.' + self.name)
  end
  
end
