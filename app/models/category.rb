# This model represents the article category hierarchy.
# Currently it is flat. A gem will be used to create hierarchy layers.
class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  translates :name

  has_ancestry
  has_and_belongs_to_many :articles, touch: true

  # The root category contains all children. It must not be changed by itself.
  scope :root, -> { find_by(name: 'root') }
  
  default_scope { order(id: :asc) }

  # Returns all Articles that belongs to this Category and all children Categories.
  # This only produced two queries. First find all descendants by 'LIKE' and then
  # search all Articles that belongs to the found ids.
  def articles_recursive
    Article.where(category_id: (self.descendant_ids << self.id))
  end

  # From the developer of Ancestry. This arranges the categories in a tree structure.
  # Source: https://github.com/stefankroes/ancestry/wiki/arrange_as_array
  def self.arrange_as_array(options={}, hash=nil)
    hash ||= arrange(options)

    arr = []
    hash.each do |node, children|
      arr << node
      arr += arrange_as_array(options, children) unless children.nil?
    end
    arr
  end

  def slug_candidates
    [:name, [:name, :id]]
  end

  def name_for_selects
    "#{'-' * depth} #{name}"
  end

  # For category selection
  # See: https://github.com/stefankroes/ancestry/wiki/Creating-a-selectbox-for-a-form-using-ancestry
  def possible_parents
    parents = Category.arrange_as_array(order: 'name')
    return new_record? ? parents : parents - subtree
  end

end