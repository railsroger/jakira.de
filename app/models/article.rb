# Represents an article.
class Article < ActiveRecord::Base
  enum order_confirmation: [:automatically,
                            :manually,
                            :with_exception]
  enum status:       [:online, :offline, :sold]
  enum article_type: [:physical, :digital, :service]
  enum made_in_year: ["1299_and_older",
                      "1300 – 1399",
                      "1400 – 1499",
                      "1500 – 1599",
                      "1600 – 1699",
                      "1700 – 1799",
                      "1800 – 1899"] +
                        (1900..Time.now.year).to_a.map(&:to_s)
  enum textile_size: ["XS", "S", "M", "L", "XL", "XXL", "XXXL", "XXXXL", "one_size"]
  enum kind: [:normal, :template, :ordered]
  enum art_type: [:applied_art, :fine_art, :old_masters]

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged
  attr_accessor :save_as_template

  before_save :change_status_to_offline
  before_save :set_template_name, if: -> {kind == "template" && template_name.nil?}
  after_save :create_template, if: -> {save_as_template == '1'}
  after_save :check_if_pictures_have_images
  
  validates_presence_of :template_name,
    if: -> {save_as_template == '1'}
  validates :template_name,
    uniqueness: { scope: :user_id },
      if: -> {save_as_template == '1' && template_name.present?}

  validates_presence_of :categories,
                        :article_type,
                        :artist,
                        :materials,
                        :colors,
                        :suitable_fors,
                        :art_type

  validates_presence_of :payment_types

  validates :title, length: {minimum: 3, maximum: 255}
  validates :price_cents, numericality: {greater_than: 0}
  validates :description, length: {minimum: 5, maximum: 3000}

  belongs_to :user
  has_one :shop, through: :user

  has_and_belongs_to_many :categories
  has_and_belongs_to_many :materials
  has_and_belongs_to_many :payment_types
  has_and_belongs_to_many :colors
  has_and_belongs_to_many :gem_colors
  has_and_belongs_to_many :suitable_fors

  has_one :bank_transfer_data,
    as: :bank_transfer_datable,
    inverse_of: :bank_transfer_datable,
    dependent: :destroy

  has_one :paypal_data,
    as: :paypal_datable,
    inverse_of: :paypal_datable,
    dependent: :destroy

  has_one :shipping_readiness, inverse_of: :article

  belongs_to :main_picture,
             class_name: 'Image'

  has_many :pictures,
           -> { article_pictures },
           class_name: 'Image',
           foreign_key: :parent_id,
           inverse_of: :article

  has_many :material_textiles, inverse_of: :article

  has_many :shipping_methods, as: :shipping_methodable,
                              inverse_of: :shipping_methodable

  has_many :documents

  has_many :shopping_baskets, dependent: :destroy
  has_many :favorites, dependent: :destroy
  
  has_one :articles_order
  has_many :orders, through: :articles_orders

  monetize :price_cents

  accepts_nested_attributes_for :pictures,
                                :shipping_methods,
                                :material_textiles,
                                :shipping_readiness

  accepts_nested_attributes_for :bank_transfer_data, allow_destroy: true
  accepts_nested_attributes_for :paypal_data, allow_destroy: true

  scope :latest, -> { order('articles.created_at DESC') }

  scope :not_deleted, -> { where(deleted: false) }

  scope :deleted, -> { where(deleted: true)
                      .where(completely_deleted: nil)}

  scope :visible, -> { not_deleted
                      .normal
                      .where("articles.status = ? OR (articles.status = ? AND articles.sold_at > ?)",
                        Article.statuses[:online],
                        Article.statuses[:sold],
                        3.days.ago)
                      .joins(user: :shop)
                      .where(shops: {online: true})
                      .latest }

  scope :search, lambda { |text|
    if text
       includes(:categories)
      .includes(:user)
      .includes(:shop)
      .includes(:materials)
      .includes(:colors)
      .where('LOWER(articles.title) LIKE :search
           OR LOWER(articles.artist) LIKE :search
           OR LOWER(categories.name) LIKE :search
           OR LOWER(users.nickname) LIKE :search
           OR LOWER(shops.name) LIKE :search
           OR LOWER(materials.name) LIKE :search
           OR LOWER(colors.name) LIKE :search',
           search: "%#{text.downcase}%")
      .references(:categories)
      .references(:user)
      .references(:shop)
      .references(:materials)
      .references(:colors)
    else
      all
    end
  }

  scope :search_in_category, lambda { |text|
    if text
      where('LOWER(articles.title) LIKE ?', "%#{text.downcase}%")
    else
      all
    end
  }

  # This overrides the 'has_many :documents' association
  def documents
    Document.where(article_id: self.id).where(type: nil)
  end

  # If vat is true, VAT will be included in the calculation.
  def price_vat
    p = self.price
    if vat && self.vat != nil
      p + p * self.vat/100
    else
      p
    end
  end

  # Defines the last part of the SEO url of this resource.
  def slug_candidates
    if categories == nil
      [
        :title,
        [:title, :id]
      ]
    else
      [
        :title,
        [self.categories.name, :title],
        [:title, :id]
      ]
    end
  end

  def categories_with_backslashes
    Category.sort_by_ancestry(self.categories).map do |category|
      if category.ancestry == '1'
        ' / ' + category.name
      else
        '-' + category.name
      end
    end.join()[3..-1]
  end

  def main_picture_url
    if self.main_picture.present?
      self.main_picture.image_url
    else
      'placeholder.gif'
    end
  end

  def main_picture_for_mobile_url
    if self.main_picture.present?
      self.main_picture.image.for_mobile.url
    else
      'placeholder.gif'
    end
  end

  def small_main_picture
    if self.main_picture.present?
      self.main_picture.image.small.url
    else
      'placeholder.gif'
    end
  end

  def middle_main_picture
    if self.main_picture.present?
      self.main_picture.image.middle.url
    else
      'placeholder.gif'
    end
  end

  def textile_size_value
    I18n.t(textile_size, default: textile_size) if textile_size.present?
  end

  def no_shipping_to_the_following_countries
    if no_shipping_to_the_countries.present?
      countries = no_shipping_to_the_countries
      countries.delete("")
      array = countries.collect do |c|
        country = ISO3166::Country[c]
        country.translations[I18n.locale.to_s] || country.name if country.present?
      end
      array.join(', ')
    end
  end

  def create_ordered_article
    article = Article.new(attributes.except('id',
                                            'slug',
                                            'created_at',
                                            'updated_at'))
    article.kind          = "ordered"
    article.materials     = materials
    article.colors        = colors
    article.gem_colors    = gem_colors
    article.suitable_fors = suitable_fors
    article.categories    = categories
    article.payment_types = payment_types
    shipping_methods.each do |shipping_method|
      article.shipping_methods
             .build(shipping_method.attributes
                                   .except('id', 'shipping_methodable_id'))
    end
    article.save!
    pictures.each do |picture|
      created_picture = article.pictures.create(image: picture.image)
      article.main_picture_id = created_picture.id if picture.main?
      article.save!
    end
    material_textiles.each do |material_textile|
      article.material_textiles
             .create(material_textile.attributes
                                     .except('id', 'article_id'))
    end
    if paypal_data.present?
      article.create_paypal_data(paypal_data.attributes
                                            .except('id', 'paypal_datable_id'))
    end
    if bank_transfer_data.present?
      article.create_bank_transfer_data(bank_transfer_data.attributes
                                     .except('id', 'bank_transfer_datable_id'))
    end
    article.create_shipping_readiness(shipping_readiness.attributes
                                                        .except('id'))
    article
  end

  def self.years(custom_year=nil)
    array = ["1299_and_older", "1300 – 1399", "1400 – 1499",
             "1500 – 1599", "1600 – 1699", "1700 – 1799",
             "1800 – 1899"] + (1900..Time.now.year).to_a.map(&:to_s)
    array += [custom_year] if custom_year.present? && array.exclude?(custom_year)
    array
  end

  def available_countries
    shipping_methods_names = shipping_methods.collect(&:name)

    if shipping_methods_names.include?('worldwide')
      ISO3166::Country.all.map(&:alpha2)
    elsif shipping_methods_names.include?('within_eu')
      ISO3166::Country.all.select(&:in_eu?).map(&:alpha2)
    elsif shipping_methods_names.include?('free_within_germany') || 
          shipping_methods_names.include?('within_germany')
      ['DE']
    else
      []
    end
  end

   def available_countries_except_forbidden
    if no_shipping_to_the_countries.present?
      available_countries - no_shipping_to_the_countries
    else
      available_countries
    end
   end

  def self.material_textiles_names 
    %w(alcantara angora cotton_blended cotton_pure
       organic_fabric chiffon cord elastane fur
       felt fleece yarn rubber wood jeans jersey
       kashmir synthetic_fiber faux_fur
       faux_leather latex leather linen merino
       metal mixed_fabrics modal mohair organza
       plush polyester velvet satin sailcloth
       silk lace tarpaulin pleating taffeta
       tulle viscose oilcloth wool)
  end

  def self.gems
    %w(agate alexandrite amazonit amethyst ametrine apatite apophyllit
       aquamarine aventurine azurite rock_crystal amber beryl
       blue_quartz chalcedony chrysocolla chrysopras citrin demantoid
       diamond hawk_eye fluorite garnet hematite heliotrope jade
       jasper carnelian kunzit kyanite labradorite lapis_lazuli
       malachite marcasite marble meteorite moonstone moss_agate
       moonstone moss_agate morganite obsidian onyx opal peridot
       pearl pop_rocks quartz smoky_quartz rhodochrosit rhodonit
       rose_quartz ruby rutilquarz sapphire snowflake_obsidian
       emerald sphen sodalite sunstone sphen star_ruby star_sapphire
       tanzanite tigereye topaz turquoise tourmaline unakit zircon
       zultanit)
  end

  def colors_names
    colors.map(&:name).join(', ')
  end

  def materials_names
    materials.map(&:name).join(', ')
  end

  def pickup
    shipping_methods.find_by(name: 'pickup')
  end

  def original_article
    return unless ordered?
    articles_order.original_article
  end

  def picture_ids=(ids)
    super ids
    Image.where(id: ids).each do |picture|
      picture.update(kind: :article_pictures)
    end
  end

  private

  def create_payment_selection
    payment_selection = PaymentSelection.new
    payment_selection.article_id = self.id
    payment_selection.payment_advance = true
    payment_selection.save!
  end

  def active?
    state == 'finished'
  end

  # Check if the Article can be updated. There should be no pending or confirmed order
  def can_be_updated
    errors.add(:base, 'is sold') if self.sold?
    articles = Article.where(id: self.id).joins('
      INNER JOIN articles_orders ON articles.id = articles_orders.article_id
      INNER JOIN orders ON articles_orders.order_id = orders.id
      INNER JOIN purchases ON orders.purchase_id = purchases.id').where(
        'purchases.state = ? AND articles_orders.is_approved = ?', 'finished', '')
    if articles.count > 0
      errors.add(:base, 'has pending orders')
    end
  end

  def is_payment_selected
    ps = self.payment_selection
    if ps == nil or (!ps.payment_advance && !ps.payment_wire && !ps.payment_cash && !ps.payment_paypal)
      errors.add(:base, 'needs at last one payment method')
    end
  end

  def change_status_to_offline
    if self.deleted || self.completely_deleted
      self.status = "offline"
    end
  end


  def create_template
    article = Article.new(self.attributes.except('id', 'slug', 'created_at', 'updated_at'))
    article.kind          = "template"
    article.materials     = self.materials
    article.colors        = self.colors
    article.gem_colors    = self.gem_colors
    article.suitable_fors = self.suitable_fors
    article.categories    = self.categories
    article.payment_types = self.payment_types
    self.shipping_methods.each do |shipping_method|
      article.shipping_methods.build(shipping_method.attributes.except('id', 'shipping_methodable_id'))
    end
    article.save!
    self.pictures.each do |picture|
      created_picture = article.pictures.create(image: picture.image)
      article.main_picture_id = created_picture.id if picture.main?
      article.save!
    end
    self.material_textiles.each do |material_textile|
      article.material_textiles.create(material_textile.attributes.except('id', 'article_id'))
    end
    if self.paypal_data.present?
      article.create_paypal_data(self.paypal_data.attributes.except('id', 'paypal_datable_id'))
    end
    if self.bank_transfer_data.present?
      article.create_bank_transfer_data(self.bank_transfer_data.attributes.except('id', 'bank_transfer_datable_id'))
    end
    article.create_shipping_readiness(self.shipping_readiness.attributes.except('id'))
  end

  def set_template_name
    self.template_name = self.title
  end

  def check_if_pictures_have_images
    pictures.each do |picture|
      picture.destroy if !picture.image.file.exists?
    end
  end

end
