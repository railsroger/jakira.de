# This Model manages User accounts. It is coupled with devise and allows email confirmation,
# locking after too many unsuccessful login attempts, validates the user input and tracks some
# activity about the user like the login count.
class User < ActiveRecord::Base

  enum articles_view_type: [:vertical, :horizontal]
  enum role: [:guest, :regular, :admin, :art_association]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         # :lockable,
         :omniauthable,
         :confirmable,
         :timeoutable, timeout_in: 1.hours,
          authentication_keys: [:login]

  attr_accessor :account_type,
                :current_password,
                :accept_terms,
                :accept_privacy_policy,
                :login,
                :become_a_member

  before_create :add_membership_number,
                if: -> { regular? }

  # before_save :add_art_association_membership_number,
  #   if: -> { art_association.present? &&
  #            art_association_membership_number.nil?}

  after_commit :update_business_cards_thumbs, if: -> { has_shop? }

  belongs_to :address
  belongs_to :art_association
  has_many :articles
  has_many :shopping_baskets
  has_many :cart_articles,
             through: :shopping_baskets,
             source: :article
  has_many :favorites
  has_many :favorite_articles,
             through: :favorites,
             source: :article
  has_many :orders, foreign_key: 'seller_id'
  has_many :purchases
  has_many :articles_orders, through: :purchases
  has_many :sales,
             through: :orders,
             source: :articles_orders
  has_many :favorite_shops
  has_many :invoices
  has_many :order_messages
  
  has_many :uploaded_pictures,
           class_name: 'Image',
           foreign_key: :uploader_id

  has_many :pictures,
           -> { user_pictures },
           class_name: 'Image',
           foreign_key: :parent_id

  has_many :business_cards_orders
  # has_many :sent_messages, class_name: "Message", foreign_key: 'sender_id'
  # has_many :received_messages, class_name: "Message", foreign_key: 'recipient_id'

  has_many :received_ratings,
           class_name: 'Rating',
           foreign_key: "receiver_id"

  has_many :sent_ratings,
           class_name: 'Rating',
           foreign_key: "sender_id"

  has_one :shop

  accepts_nested_attributes_for :shop, :address

  validates_presence_of :art_association_membership_number,
                        :art_association,
                        if: -> { become_a_member }

  validates :art_association_membership_number,
            numericality: { greater_than: 0 },
            allow_nil: false,
            if: -> { become_a_member }

  validates :art_association_membership_number,
            numericality: { greater_than: 0 },
            allow_nil: true,
            if: -> { !become_a_member }

  validates_uniqueness_of :art_association_membership_number,
                            scope: :art_association_id,
                            if: -> { art_association.present? || become_a_member }

  validates :accept_terms,
            :accept_privacy_policy,
              acceptance: true

  # validates_presence_of :nickname,
  #                         unless: -> { art_association.present? }

  validates :nickname,
              uniqueness: true,
              format: {with: /^[a-zA-Z0-9_\.]*$/,
                       multiline: true},
              allow_blank: true,
                if: -> { !guest? }

  validates_presence_of :address,
                          if: -> { regular? || art_association? }


  scope :newsletter_receivers, -> { regular
                                   .where(monthly_letters_notification: true)}

  scope :sellers, -> { joins(:shop)
                      .where(shops: {deleted: false}) }

  scope :buyers, -> { regular
                     .where("not exists (
                             select null 
                             from shops
                             where users.id = shops.user_id 
                             and shops.deleted != 't')")}

  scope :search, lambda { |text|
    if text
       includes(:address)
      .where('LOWER(users.nickname) LIKE :search 
           OR LOWER(users.email) LIKE :search
           OR LOWER(addresses.company_name) LIKE :search
           OR LOWER(addresses.first_name) LIKE :search
           OR LOWER(addresses.last_name) LIKE :search',
           search: "%#{text.downcase}%")
      .references(:address)
    else
      all
    end
  }

  def rating_count(rating=nil)
    if (1..5).exclude?(rating)
      self.received_ratings.finished.count
    else
      self.received_ratings.finished.where(score: rating).count
    end
    
  end

  def average_rating
    self.received_ratings.finished.map(&:score).sum.to_f / self.rating_count if self.received_ratings.finished.present?
  end

  def float_rating
    ((average_rating - average_rating.floor) * 100).to_i if self.average_rating.present? 
  end

  def full_name
    address.full_name
  end

  def has_shop?
    !shop.blank? && !shop.new_record? && !shop.deleted?
  end

  alias seller? has_shop?

  def buyer?
    regular? && !has_shop?
  end

  def buyer_or_guest?
    buyer? || guest?
  end

  def regular_or_guest?
    regular? || guest?
  end

  def email_from
    full_name + ' <' + email + '>'
  end

  def remember_me
    true
  end

  def self.from_omniauth(auth)
    user = where(provider: auth.provider, uid: auth.uid).last
    if user.present? && !user.deleted
      user
    else
      u = User.new(provider: auth.provider,
                   uid: auth.uid,
                   nickname: auth.info.name,
                   email: "temporary_#{Time.now.to_i}#{rand(100)}@email.com")
      u.skip_confirmation!
      u.build_address
      u.save!(validate: false)
      u
    end
  end

  def has_unread_messages?
    Message.received(self).not_read.count > 0
  end

  def self.members_to_xls(options = {})
    CSV.generate(options) do |csv|
      user_columns = ["art_association_membership_number",
                      "nickname",
                      "email"]
      address_columns = ["first_name",
                         "last_name",
                         "street",
                         "house_number",
                         "postal_code",
                         "place",
                         "country"]
      csv << user_columns + address_columns
      all.each do |user|
        csv << user.attributes.values_at(*user_columns) + user.address.attributes.values_at(*address_columns)
      end
    end
  end

  def occupation
    has_shop? ? shop.address.occupation : nil
  end

  def full_name_occupation
    full_name + (occupation.present? ? " - #{occupation}" : '')
  end

  def address_and_contact_data_for_business_card
    address_for_business_card +
    "\n" +
    contact_data_for_business_card
  end

  def address_for_business_card
    "#{shop.address.street} #{shop.address.house_number}" +
    "\n#{shop.address.postal_code} #{shop.address.place}"
  end

  def contact_data_for_business_card
    (shop.address.phone.present? ?
      "#{I18n.t('user.address_for_business_card.phone')} #{shop.address.phone_number}\n" :
      '') +
    (shop.address.mobile.present? ?
      "#{I18n.t('user.address_for_business_card.mobile')} #{shop.address.mobile_number}\n" :
      '') +
    "#{I18n.t('user.address_for_business_card.email')} #{email}"
  end

  def use_or_generate_purchase
    last_purchase = purchases.last
    user_address = address.attributes.except('id')
    if last_purchase.nil? || last_purchase.confirmed?
      @purchase = purchases.create
    else
      @purchase = last_purchase
    end

    @articles = shopping_baskets.map(&:article)

    # remove removed articles_orders
    @purchase.articles_orders.each do |articles_order|
      if !@articles.include?(articles_order.original_article) ||
         !articles_order.original_article_not_changed?
        articles_order.delete_voucher_discount
        articles_order.destroy
      end
    end

    @articles.group_by(&:user_id).each do |user_id, articles|
      # Create an Order
      new_order = @purchase.orders.find_or_initialize_by(seller_id: user_id)
      new_order.seller_address = User.find(user_id).address.dup
      new_order.save!

      # Fill the Order with ArticlesOrders
      articles.each do |article|
        old_articles_order =
          new_order.articles_orders
                   .find_by(original_article_id: article.id)
        article_in_cart = article.shopping_baskets
                                 .find_by(user_id: id)
        if old_articles_order.present?
          articles_order = old_articles_order
        else
          articles_order = 
            new_order.articles_orders.new(
              original_article_id: article.id,
              article_id: article.create_ordered_article.id)
        end

        articles_order.message_text    = article_in_cart.message_text
        articles_order.message_sent_at = article_in_cart.message_sent_at
        articles_order.amount          = article_in_cart.article_amount

        if old_articles_order.present?
          articles_order.save!(validate: false)
        else
          articles_order.build_shipping_address user_address
          articles_order.build_payment_address user_address
          articles_order.save!(validate: false)
          if articles_order.shipping_address.all_filled?
            articles_order.use_shipping_address_for_payment_address = true
          end
        end
        unless articles_order.available_countries_except_forbidden
                             .include?(articles_order.shipping_address.country)
          articles_order.shipping_address.update_attribute('country', '')
        end
      end
      new_order.save!
    end
    @purchase
  end

  def picture_ids=(ids)
    super ids
    Image.where(id: ids).each do |picture|
      picture.update(kind: :user_pictures)
    end
  end

  private

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash)
     .where(["lower(nickname) = :value OR lower(email) = :value",
            { value: login.downcase }]).first
    elsif conditions.has_key?(:nickname) || conditions.has_key?(:email)
      where(conditions.to_hash).first
    end
  end

  def add_membership_number
    last_regular_user = User.regular.last
    last_membership_number = last_regular_user.nil? ? 1000000000 : last_regular_user.membership_number
    self.membership_number = last_membership_number + 1
  end

  # def add_art_association_membership_number
  #   max_number = art_association.members.map(&:art_association_membership_number).compact.max
  #   new_number = max_number.present? ? max_number + 1 : 1
  #   self.art_association_membership_number = new_number
  # end

  def self.monthly_invoices_generating
    User.sellers.each do |user|
      user.invoices.create(price: user.shop.monthly_charge, status: "check")
    end
  end

  def self.quarter_invoices_generating
    beginning_of_quarter = Time.now.yesterday.beginning_of_quarter
    end_of_quarter = Time.now.yesterday.end_of_quarter
      
    User.sellers.each do |user|
      sales = user.sales.where(confirmed_at: beginning_of_quarter..end_of_quarter)
      if sales.present?
        sales_ids = sales.map(&:id)
        sum_of_invoice = (sales.map(&:article).map(&:price).sum / 100 * 5)
        user.invoices.create(price: sum_of_invoice, articles_order_ids: sales_ids, status: "check")
      end
    end
  end

  def self.send_newsletters(newsletter)
    receivers = case newsletter.receivers
    when "everyone"
      User.newsletter_receivers
    when "sellers"
      User.sellers.newsletter_receivers
    when "buyers"
      User.buyers.newsletter_receivers
    end
    receivers.each do |user|
      UserMailer.send_newsletter(newsletter, user.email).deliver
    end
  end

  def self.find_for_authentication(conditions)
    super(conditions.merge(deleted: false))
  end

  def update_business_cards_thumbs
    new_thread = Thread.new do
      business_cards_templates_names = [*1..6].map { |n| n.to_s.rjust(2, '0') }
      business_cards_thumbs = 'public/business_cards_thumbs/'
      directory = business_cards_thumbs + id.to_s + '/'
      Dir.mkdir(business_cards_thumbs) unless File.exists?(business_cards_thumbs)
      Dir.mkdir(directory) unless File.exists?(directory)
      business_cards_templates_names.each do |file_name|
        path_to_temporary_pdf = directory + file_name + '.pdf'
        pdftk = PdfForms.new('/usr/bin/pdftk')
        pdftk.fill_form ('public/business_cards_templates/' + file_name + '.pdf' ),
                        path_to_temporary_pdf,
                        { "Shopname in Großbuchstaben" => shop.name.mb_chars.upcase.to_s,
                          "Name Kunde" => full_name_occupation,
                          "Adresse & Kontaktdaten" => address_and_contact_data_for_business_card,
                          "Adresse" => address_for_business_card,
                          "Kontaktdaten" => contact_data_for_business_card}
        img_path = directory + file_name + '_front.png'
        pdf = Grim.reap(path_to_temporary_pdf)
        pdf[0].save(img_path, {
          width: 420,
          colorspace: "sRGB",
          alpha: "remove"
        })
        img_path = directory + file_name + '_back.png'
        pdf = Grim.reap(path_to_temporary_pdf)
        pdf[1].save(img_path, {
          width: 420,
          colorspace: "sRGB",
          alpha: "remove"
        })
        File.delete path_to_temporary_pdf
      end
    end
    new_thread.join
  end

end
