class FavoriteShop < ActiveRecord::Base
  belongs_to :shop
  belongs_to :user

  validates :shop_id,
  			  uniqueness: true,
  			  	if: ->(o) { o.user.favorite_shops.map(&:shop_id).include?(o.shop_id) }
              
end
