class PersonalBankData < ActiveRecord::Base
  belongs_to :shop
  validates_presence_of :account_holder,
                        :iban,
                        :swft_bic
end