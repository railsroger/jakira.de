class MaterialTextile < ActiveRecord::Base
  belongs_to :article, touch: true

  validates :percent,
              numericality: { less_than: 101 },
              	if: -> { name.present?}

  validate :validate_percent

  validates_presence_of :name, if: -> { percent.present? }

  private

  def validate_percent
    if article.material_textiles.map(&:percent).map(&:to_i).sum > 100 && percent.present?
      errors.add(:percent, :more_than)
    end
  end
end
