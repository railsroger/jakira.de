class Message < ActiveRecord::Base
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :recipient, class_name: 'User', foreign_key: 'recipient_id'

  validates_presence_of :subject, :body

  mount_uploader :file, FileUploader

  scope :sent,
        ->(current_user) { where(sender_id: current_user.id)
                          .where(sender_status: nil)
                          .order('sent_at DESC')}

  scope :received,
        ->(current_user) { where(recipient_id: current_user.id)
                          .where(recipient_status: nil)
                          .order('sent_at DESC') }

  scope :drafts,
        ->(current_user) { where(sender_id: current_user.id)
                          .where(sender_status: "draft")
                          .order('created_at DESC') }

  scope :deleted,
        ->(current_user) { where("(sender_id = ? AND sender_status = ?) OR 
                           (recipient_id = ? AND recipient_status = ?)", 
                           current_user.id, 'deleted',
                           current_user.id, 'deleted') }

  scope :archived,
        ->(current_user) { where("(sender_id = ? AND sender_status = ?) OR
                           (recipient_id = ? AND recipient_status = ?)",
                           current_user.id, 'archived',
                           current_user.id, 'archived') }

  scope :not_read,
        -> { where(read_at: nil) }

  # Read message and if it is read by recepient then mark it is read
  def self.read_message(id, reader)
    message = find(id, :conditions => ['sender_id = ? OR recipient_id = ?', reader, reader])
    if message.read_at.nil? && (message.recipient.id==reader)
      message.read_at = Time.now
      message.save!
    end
    message
  end

  # Based on if a message has been read by it's recepient returns true or false.
  def read?
    self.read_at.nil? ? false : true
  end

  def system?
    self.sender_id == 0
  end

  # Create a user notification
  def self.notify(user, subject, body)
    Message.create!(sender_id: 0, recipient_id: user.id, subject: subject, body: body)
  end

end
