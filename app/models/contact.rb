class Contact

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :first_name,
                :last_name,
                :email,
                :theme,
                :message

  validates_presence_of :first_name,
                        :last_name,
                        :email,
                        :message
  
  validates :email, email: true

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

end