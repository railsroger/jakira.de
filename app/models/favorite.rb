class Favorite < ActiveRecord::Base
  after_create :increment_added_to_favorites_count_in_article
  after_save :remove_from_cart

  belongs_to :article
  belongs_to :user

  validates_presence_of :article, :user

  validates :article,
            uniqueness: { scope: :user,
                          message: 'You have already added this article' }

  validate :article_is_valid?

  private

  def article_is_valid?
    if !article.normal? ||
       article.deleted? ||
       article.completely_deleted? ||
       article.offline?
      errors.add(:article, 'Invalid Article')
    end
  end

  def increment_added_to_favorites_count_in_article
    ActiveRecord::Base.record_timestamps = false
    article.increment!('added_to_favorites_count', 1)
    ActiveRecord::Base.record_timestamps = true
  end

  def remove_from_cart
    shopping_basket = user.shopping_baskets.find_by(article_id: article_id)
    shopping_basket.destroy if shopping_basket.present?
  end
end
