class ShoppingBasket < ActiveRecord::Base
  before_save :add_message_sent_at_time
  after_create :increment_added_to_cart_count_in_article
  after_save :remove_from_favorites

  belongs_to :user
  belongs_to :article

  validates_presence_of :article, :user

  validates :article,
            uniqueness: { scope: :user,
                          message: 'You have already added this article' }

  validate :user_not_owner_of_article?
  validate :article_not_sold?
  validate :article_is_valid?

  def price_with_discount
    article.price
  end

  def total_price
    article.price
  end

  private

  def user_not_owner_of_article?
    return unless article.user_id == user_id
    errors.add(:article, :you_cant_buy_your_article)
  end

  def article_not_sold?
    return unless article.sold?
    errors.add(:article, :the_article_was_already_sold)
  end

  def article_is_valid?
    if !article.normal? ||
       article.deleted? ||
       article.completely_deleted? ||
       article.offline?
      errors.add(:article, 'Invalid Article')
    end
  end

  def add_message_sent_at_time
    self.message_sent_at = Time.now if message_text.present?
  end

  def increment_added_to_cart_count_in_article
    ActiveRecord::Base.record_timestamps = false
    article.increment!('added_to_cart_count', 1)
    ActiveRecord::Base.record_timestamps = true
  end

  def remove_from_favorites
    favorite = user.favorites.find_by(article_id: article_id)
    favorite.destroy if favorite.present?
  end
end
