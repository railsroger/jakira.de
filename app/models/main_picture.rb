class MainPicture < Picture
  # has_one :shop

  def shop
    Shop.find_by(main_picture_id: id)
  end
end