class BusinessCardsOrder < ActiveRecord::Base

  enum status: [:ordered, :in_processing, :completed]

  belongs_to :user
  belongs_to :buyer, class_name: 'User', foreign_key: :user_id

  validates_presence_of :value

end