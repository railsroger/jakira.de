class WithdrawalForm < ActiveRecord::Base
  belongs_to :shop
  mount_uploader :file, WithdrawalFormUploader
end