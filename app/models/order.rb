# An Order can be issued by an User to buy one or multiple Articles. The Article will be marked as sold when the
# seller confirms the Order. The Articles are associated to the order by the ArticlesOrders model.
class Order < ActiveRecord::Base
  has_many :articles_orders, inverse_of: :order
  has_many :articles, through: :articles_orders

  belongs_to :purchase

  belongs_to :seller_address,
      class_name: "Address",
      foreign_key: "seller_address_id"
  
  belongs_to :seller,
      class_name: "User",
      foreign_key: "seller_id"

end
