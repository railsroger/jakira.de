class SuitableFor < ActiveRecord::Base

  has_and_belongs_to_many :articles, touch: true

  translates :name

end
