# An Article can have multiple ShippingOptions. The buyer will select one.
class ShippingMethod < ActiveRecord::Base

  after_save :destroy_if_not_checked

  belongs_to :shipping_methodable, polymorphic: true, touch: true
  has_many :articles_orders

  monetize :price_cents

  validates_acceptance_of :checked, if: :shipping_methods_were_not_chosen?
  
  validates :price_cents,
              numericality: { greater_than_or_equal_to: 0 },
              if: -> { checked && name != "pickup" && name != "free_within_germany"}

  validate :validate_duration

  validates :name,
    inclusion: { in: %w{pickup free_within_germany within_germany within_eu worldwide} },
    presence: true,
    uniqueness: { scope: :shipping_methodable_id}

  def destroy_if_not_checked
  	if !self.checked
  	  self.destroy
  	end
  end

  def shipping_methods_were_not_chosen?
    (shipping_methodable.class.to_s == "Article" ||
      shipping_methodable.set_shipping == '1') &&
    !shipping_methodable.shipping_methods.map(&:checked).include?(true)
  end

  def duration
    if duration_from.nil? || duration_to.nil?
      I18n.t('shipping_method.duration.by_appointment')
    else
      duration_from.to_s + ' - ' + duration_to.to_s + I18n.t('shipping_method.duration.days')
    end
  end

  private

  def validate_duration
    if checked && name != "pickup"
      if duration_from.nil? ||
        duration_from < 0 ||
        duration_from == 0 ||
        (duration_to.present? && (duration_from >= duration_to && duration_to != 0))
        errors.add(:duration, I18n.t("activerecord.errors.models.shipping_method.attributes.validate_duration.valid") )
        errors.add(:duration_from, nil)
      end
      if duration_to.nil? ||
        duration_to <= 0 ||
        (duration_from.present? && duration_from >= duration_to)
        errors.add(:duration, I18n.t("activerecord.errors.models.shipping_method.attributes.validate_duration.valid") )
        errors.add(:duration_to, nil)
      end
    end
  end
end
