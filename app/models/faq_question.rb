class FaqQuestion < ActiveRecord::Base
  
  belongs_to :faq_section

  translates :question, :answer

  validates :question, length: {minimum: 3, maximum: 255}

  scope :search, lambda { |text|
    if text
      where('LOWER(question) LIKE :search
          OR LOWER(answer) LIKE :search',
          search: "%#{text.downcase}%")
    else
      all
    end
  }

end
