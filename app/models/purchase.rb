class Purchase < ActiveRecord::Base
  belongs_to :user
  belongs_to :buyer, class_name: 'User', foreign_key: :user_id

  has_many :orders, inverse_of: :purchase
  has_many :articles_orders, through: :orders

  validates :user, presence: true

  scope :confirmed,     -> { where(confirmed: true) }
  scope :not_confirmed, -> { where(confirmed: false) }

  def shipping_and_payment_methods_are_present?
    articles_orders.map do |ao|
      ao.shipping_method.present? && ao.payment_type.present?
    end.exclude?(false)
  end

  def confirm
    update(confirmed: true)
    articles_orders.each do |articles_order|
      articles_order.update(ordered_at: Time.now)
      case articles_order.article.order_confirmation
      when 'automatically'
        articles_order.confirmed!
      when 'with_exception'
        if !articles_order.article.customizable
          articles_order.confirmed!
        else
          articles_order.awaiting_confirmation!
        end
      else
        articles_order.awaiting_confirmation!
      end
    end
    user.shopping_baskets
        .where(article_id: articles_orders.map(&:original_article)
                                          .map(&:id))
        .delete_all
  end

  def actual_shipping_address
    shipping_address
  end

  def all_filled?
    articles_orders.map(&:filled?).exclude?(false)
  end

  def set_shipping_methods
    articles_orders.includes(:shipping_method)
                   .where('shipping_method_id IS ? OR name != ?',
                          nil, 'pickup')
                   .references(:shipping_methods)
                   .map(&:set_shipping_method)
  end

  def fixed_costs
    articles_orders.map(&:article).map(&:price).sum
  end

  def shipping_costs
    articles_orders.map(&:shipping_method_price).sum
  end

  def discounts_sum
    articles_orders.map(&:voucher_discount).sum
  end

  def total_costs
    articles_orders.map(&:total_price).sum
  end

end
