class Picture < Document
  belongs_to :article, touch: true
  belongs_to :shop
  belongs_to :user
  mount_uploader :image, ImageUploader

  attr_accessor :x, :y, :width, :height, :rotate
  after_update :crop_image, if: :cropping?

  def main?
    self.article.present? &&
    self.article.main_picture_id.present? &&
    self.article.main_picture_id == self.id
  end

  def zoomable?
    small_width = FastImage.size(self.image.middle.path)[0]
    small_height = FastImage.size(self.image.middle.path)[1]
    original_width = FastImage.size(self.image.path)[0]
    original_height = FastImage.size(self.image.path)[1]
    small_width < original_width &&
    small_height < original_height
  end

  def cropping?
    !x.blank? && !y.blank? && !width.blank? && !height.blank?
  end

  private

  def crop_image
    image.recreate_versions!
  end

end

