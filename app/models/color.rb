class Color < ActiveRecord::Base
  has_and_belongs_to_many :articles, touch: true

  translates :name

  scope :order_by_name, -> { with_translations(I18n.locale)
  							            .order('color_translations.name') }

  def self.available
    colors = []
    ["black", "brown", "beige", "white", "gold", "orange", "red", "pink",
     "purple", "blue", "green", "gray", "silver", "colorful"].each do |name|
      colors << Color.with_translations(:en).find_by(name: name)
    end
    colors
  end
end
