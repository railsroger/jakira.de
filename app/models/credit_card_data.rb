class CreditCardData
  include ActiveModel::Model
  include ActiveModel::Validations
  attr_accessor :type,
  				:number,
  				:expire_month,
  				:expire_year,
  				:first_name,
  				:last_name

  validates_presence_of :type,
  						:expire_month,
		  				:expire_year,
		  				:first_name,
		  				:last_name
		  				
  validates :number, credit_card_number: {brands: [:visa, :mastercard]} 
  validates_numericality_of :expire_year, greater_than_or_equal_to: Time.now.year
  validates_numericality_of :expire_month, greater_than: Time.now.month, if: :current_year?
  validates_numericality_of :expire_month, less_than_or_equal_to: 12

  def current_year?
    self.expire_year.to_i == Time.now.year
  end
  # def initialize(attributes = {})
  # 	attributes.each do |name, value|
  # 	  send("#{name}=", value)
  # 	end
  # end
end