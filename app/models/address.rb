class Address < ActiveRecord::Base
  after_commit :change_addresses_in_cart,
               if: -> {
                 user.present? &&
                   user.purchases.last.present? &&
                   !user.purchases.last.confirmed?
               }

  has_one :user
  has_one :art_association
  has_one :shop, inverse_of: :address
  has_one :voucher

  validates_presence_of :first_name,
                        :last_name

  validates_presence_of :street,
                        :house_number,
                        :postal_code,
                        :place,
                        :country,
                        if: -> {
                          shop.present? ||
                            (articles_order.present? &&
                              (!articles_order.pickup? ||
                               self == articles_order.payment_address ||
                               (self == articles_order.shipping_address &&
                                 articles_order.pickup?)))
                        }

  validates_presence_of :street,
                        :postal_code,
                        :place,
                        if: -> {
                          voucher.present? &&
                            voucher.bank_transfer?
                        }

  validates_presence_of :company_name,
                        :street,
                        :house_number,
                        :city,
                        :postal_code,
                        :phone,
                        if: -> { art_association.present? }

  validates_inclusion_of :country,
    in: ->(address) { address.articles_order.available_countries_except_forbidden || [] },
    if: ->(address) { address.articles_order.present? &&
                      address.articles_order.shipping_address == address &&
                      !address.articles_order.pickup? }

  def full_name
    if first_name.present? && last_name.present?
      first_name + " " + last_name
    end
  end

  def articles_order
    ArticlesOrder.where("shipping_address_id = ? OR payment_address_id = ?", id, id).first
  end

  def filled?
    (attributes.except('id', 'first_name', 'last_name').collect { |k, v| v.present? }).include?(true)
  end

  def all_filled?
    if valid?
      true
    else
      errors.clear
      false
    end
  end

  def default_address?
    attributes.except('id', 'first_name', 'last_name') == self.articles_order.buyer.address.attributes.except('id', 'first_name', 'last_name')
  end

  def change_addresses_in_cart
    user.purchases.last.articles_orders.each do |articles_order|
      shipping_address = articles_order.shipping_address
      shipping_address.assign_attributes(articles_order.buyer.address
                                                       .attributes.except('id'))
      shipping_address.save(validate: false)

      payment_address = articles_order.payment_address
      payment_address.assign_attributes(articles_order.buyer.address
                                                      .attributes.except('id'))
      payment_address.save(validate: false)
    end
  end

  def country_name
    if self.country.present?
      country = ISO3166::Country[self.country]
      country.translations[I18n.locale.to_s] || country.name
    end
  end

  def available_countries
    if articles_order.present? &&
       self == articles_order.shipping_address
      articles_order.available_countries_except_forbidden
    end
  end

  def priority_countries
    if country.present? &&
          available_countries.present? &&
          available_countries.include?(country) &&
          country != 'DE'
      ['DE', country]
    else
      ['DE']
    end
  end

  def payment_address?
    articles_order.present? && self == articles_order.payment_address
  end

  def phone_number
    area_code + ' ' + phone
  end
  
  def mobile_number
    area_code + ' ' + mobile
  end
end
