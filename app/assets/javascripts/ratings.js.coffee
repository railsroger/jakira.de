$.fn.updateLeftedCharactersInReviewTextarea = ->
  $(this).closest('.review-form-wrapper')
         .find('.characters_left b.characters')
         .text(300 - $(this).val().length)

@setRatingStar =(ratingStar) ->
  ratingStar.closest('.stars')
            .find('.gold_star')
            .show()
  ratingStar.nextAll()
            .find('.gold_star')
            .hide()

@clickOnStarToSetRating = ->
  $('.review-form-wrapper .stars .star').click ->
    setRatingStar($(this))
    $(this).closest('form')
           .find('input[name="rating[score]"]')
           .val $(this).index() + 1
    $(this).closest('form').submit()

@keyupOnReviewTextarea = ->
  $('.review-form-wrapper textarea').keyup ->
    $(this).updateLeftedCharactersInReviewTextarea()

$ ->
  keyupOnReviewTextarea()
  clickOnStarToSetRating()

$(document).on 'ready page:load', ->
  if $('.review-form-wrapper').length
    $('.review-form-wrapper textarea').each ->
      $(this).updateLeftedCharactersInReviewTextarea()

    $('input[name="rating[score]"]').each ->
      if $(this).val().length
        setRatingStar($(this).closest('.review-form-wrapper')
                             .find('.star')
                             .eq($(this).val() - 1))


