$ ->
  $("[id*=address_attributes_country]").change ->
    if $(this).val().length == 0
      $('[id*=attributes_area_code]').val('')
    else
      $(this).closest('form')
        .find('#load_country_code')
        .attr('href', '/addresses/load_country_code?code=' + $(this).val())
      $('#load_country_code').click()

  if $("[id*=address_attributes_country]").length &&
     $('[id*=attributes_area_code]').length
    if $("[id*=address_attributes_country]").val().length != 0 &&
       $('[id*=attributes_area_code]').val().length == 0
      $("[id*=address_attributes_country]").closest('form')
        .find('#load_country_code')
        .attr('href', '/addresses/load_country_code?code=' + $("[id*=address_attributes_country]").val())
      $('#load_country_code').click()

# $('[id*=address_attributes_phone],
#    [id*=address_attributes_mobile]').mask '000ZZZ / 00ZZ - 0000', translation: 'Z':
#   pattern: /[0-9]/
#   optional: true

# $('[id*=address_attributes_phone],
#    [id*=address_attributes_mobile]').mask '0#', translation: '0':
#    pattern: /[0-9]\s/