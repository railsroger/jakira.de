$ ->
  $('#user_message_received_notification,
     #user_rated_notification,
     #user_my_favorites_notification,
     #user_monthly_letters_notification').change ->
    $(this).submit()

  $('.account_settings .complete_cancel_check_box #complete_cancel_membership').change ->
    if $(this).prop('checked')
      $('.user_settings_button_highlight.cancel_membership').css('display', 'inline-block')
      $('.user_settings_button_disabled.cancel_membership').hide()
    else
      $('.user_settings_button_highlight.cancel_membership').hide()
      $('.user_settings_button_disabled.cancel_membership').css('display', 'inline-block')
