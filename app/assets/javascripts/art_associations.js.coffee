importXlsFileField = ->
  xlsForm = $('.art_association_index_member_page .import_xls_form')
  diplayedFileField = xlsForm.find('#file_field')
  hiddenFileField = xlsForm.find('#file')
  placeholderText = diplayedFileField.text()

  diplayedFileField.click ->
    hiddenFileField.click()

  hiddenFileField.change ->
    if $(this).val() == ''
      diplayedFileField.text placeholderText
    else
      arr = $(this).val().split('\\')
      diplayedFileField.text arr[arr.length-1]

$ ->
  if $('.art_association_index_member_page .import_xls_form').length
    importXlsFileField()

  $('table.art_association_members td.cell_with_arrow').click ->
    $(this).find('.arrow_right').toggle()
    $(this).find('.arrow_down').toggle()
    $(this).closest('tr').next().toggle()

  $('.cooperation_partners_page .section').click ->
    $(this).next('.lists').slideToggle()
    $(this).find('.arrow_to_right').toggle()
    $(this).find('.arrow_to_down').toggle()