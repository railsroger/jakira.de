$ ->
  $('.packages table input[type=checkbox]').click ->
    $('.packages table input[type=checkbox]').not($(this)).prop('checked', false)
    $('#shop_payment_authorization').prop('checked', false)

  $('#personal_bank_data input').keyup ->
    $('#shop_payment_authorization').prop('checked', false)

  $("#shop_terms_and_revocation_are_chosen").change ->
    if $("#shop_terms_and_revocation_are_chosen").prop("checked")
      $("#terms_and_revocation").slideDown()
    else
      $("#terms_and_revocation").slideUp()

  if $("#terms_and_revocation").find(".field_with_errors").length != 0
    $("#terms_and_revocation").show()

  if $("#terms_and_revocation textarea").first().val() != "" || $("#terms_and_revocation textarea").last().val() != ""
    $("#shop_terms_and_revocation_are_chosen").click()

  $("#shop-form #shop_set_shipping").change ->
    if $(this).prop("checked")
      $(".shipping_information").slideDown()
    else
      $(".shipping_information input:checked").click()
      $(".shipping_information").slideUp()

  if $("#shop-form #shop_set_shipping").prop('checked')
    $(".shipping_information").show()

  if $(".settings_shops .shipping_information .field_with_errors").length > 0
    $(".shipping_information").show().prev().hide()


  $("#shop-form #shop_set_payment_types").change ->
    if $(this).prop("checked")
      $("#payment_types").slideDown()
    else
      $("#payment_types input:checked").click()
      $("#payment_types").slideUp()
  
  if $('#shop-form #shop_set_payment_types').prop('checked')
    $("#payment_types").show()


  $('#remove_shop_irrevocably').change ->
    if $(this).prop('checked')
      $('.green_button.cancel_shop').css('display', 'inline-block')
      $('.gray_button.cancel_shop').hide()
    else
      $('.green_button.cancel_shop').hide()
      $('.gray_button.cancel_shop').css('display', 'inline-block')

  $('.left_shop_info_panel .address_header').click ->
    $(this).parent().find('.address_content').slideToggle()