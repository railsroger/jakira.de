$ ->
  $('.business_card_block img').click ->
    bigCard = $(this).next('.big_business_card')
    $('.business_cards .big_business_card').not(bigCard).hide()
    bigCard.toggle()

  $(".close_big_card").click ->
    $(this).closest('.big_business_card').hide()

  $('.admin.business_cards .green_button.edit_business_card_details').click ->
    $(this).hide()
    $('.admin.business_cards .business_card_details').hide()
    $('.admin.business_cards .business_card_details_form').show()