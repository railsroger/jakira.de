$ ->
  shippingReadinessDurationInputs = $('.shipping_readiness .from, .shipping_readiness .to')
  
  $('.shipping_readiness .immegiately').click ->
    shippingReadinessDurationInputs.prop('disabled', true)
  $('.shipping_readiness .in_some_days').click ->
    shippingReadinessDurationInputs.prop('disabled', false)
  if $('.shipping_readiness .immegiately').prop('checked')
    shippingReadinessDurationInputs.prop('disabled', true)