# Filter Box

$.fn.addFilterBox = ->
  filtersBoxes = $('.category_page .filters_boxes')
  createFilterBox = ->
    newFilterBox = $('.category_page
                      .filter_box_prototype').clone()
                                             .removeClass('filter_box_prototype')
                                             .addClass('filter_box')
    if filter.find('.button').length
      newFilterBox.find('.key').text(filter.find('.button').text() + ":")
    else
      newFilterBox.find('.key').text($('#search_in_category')
                                       .attr('placeholder') + ":")
    return newFilterBox

  updateOrCreateFilterBox =(className, value) ->
    currentFilterBox = filtersBoxes.find('.' + className)
    if currentFilterBox.length
      filterBox = currentFilterBox
    else
      filterBox = createFilterBox()
      filtersBoxes.append filterBox
      filterBox.addClass(className)
    filterBox.find('.value').text(value)
    filtersBoxes.show()

  defaultActionsForSelectTag = (selectTag, className) ->
    selectedOptions = selectTag.find('option:selected')
    if selectedOptions.length
      value = selectedOptions.map( ->
                $(this).text()
              ).get().join ', '
      updateOrCreateFilterBox(className, value)
    else
      $('.' + className).remove()
      if !filtersBoxes.find('.filter_box').length
        filtersBoxes.hide()

  filter = this.closest(".filter")
  if filter.find('.price_range').length
    value = $('#show_min_price').text() +
            '-' +
            $('#show_max_price').text() +
            $('.show_price_symbol:first').text()
    updateOrCreateFilterBox('price_filter_box', value)
  else if filter.find('#textile_sizes').length
    defaultActionsForSelectTag($('#textile_sizes'), 'textile_sizes_filter_box')
  else if filter.find('#color_ids').length
    defaultActionsForSelectTag($('#color_ids'), 'colors_filter_box')
  else if filter.find('#motive').length
    defaultActionsForSelectTag($('#motive'), 'motive_filter_box')
  else if filter.find('#artist').length
    defaultActionsForSelectTag($('#artist'), 'artist_filter_box')
  else if filter.find('#search_in_category').length
    if $('#search_in_category').val().length != 0
      value = $('#search_in_category').val()
      updateOrCreateFilterBox('search_in_category_filter_box', value)
    else
      $('.search_in_category_filter_box').remove()
      if !filtersBoxes.find('.filter_box').length
        filtersBoxes.hide()
  else if filter.find('#mobile_search_in_category').length
    if $('#mobile_search_in_category').val().length != 0
      value = $('#mobile_search_in_category').val()
      updateOrCreateFilterBox('search_in_category_filter_box', value)
    else
      $('.search_in_category_filter_box').remove()
      if !filtersBoxes.find('.filter_box').length
        filtersBoxes.hide()
  # Removing Filter Fox
  unselectOptions =(selectedOptions) ->
    selectedOptions.prop('selected', false)
        .closest('.filter').find('.element').removeClass('selected')
        .closest('form').submit()
  $('.category_page .filter_box .close_filter').click ->
    filterBox = $(this).closest('.filter_box')
    if filterBox.hasClass('price_filter_box')
      $('#min_price, #max_price')
        .val('').first()
        .closest('form').submit()
    else if filterBox.hasClass('textile_sizes_filter_box')
      unselectOptions $('#textile_sizes option:selected')
    else if filterBox.hasClass('colors_filter_box')
      unselectOptions $('#color_ids option:selected')
    else if filterBox.hasClass('motive_filter_box')
      unselectOptions $('#motive option:selected')
    else if filterBox.hasClass('artist_filter_box')
      unselectOptions $('#artist option:selected')
    else if filterBox.hasClass('search_in_category_filter_box')
      $('#search_in_category,
         #mobile_search_in_category').val('')
        .closest('form').submit()
    filterBox.remove()
    if !filtersBoxes.find('.filter_box').length
      filtersBoxes.hide()


$ ->
  $('#price_filter_slider').slider
    range: true
    min: parseInt($('#min_price').attr('peak'))
    max: parseInt($('#max_price').attr('peak'))
    values: [
      $('#show_min_price').text()
      $('#show_max_price').text()
    ]
    slide: (event, ui) ->
      $('#min_price').val ui.values[0]
      $('#max_price').val ui.values[1]
      $('#show_min_price').text ui.values[0]
      $('#show_max_price').text ui.values[1]
    change: (event, ui) ->
      $('.category_page .price_range').addFilterBox()
      $("#category_filters").submit()

  $('.category_page .price_range .apply').click ->
    $('#min_price').val $('#show_min_price').text()
    $('#max_price').val $('#show_max_price').text()
    $(this).addFilterBox()
    $(this).closest('form').submit()

  $('.category_page .filter .button').click ->
    dropDown = $(this).closest('.filter').find('.drop-down')
    button = $(this).closest('.filter').find('.button')
    button.toggleClass('pressed')
    dropDown.toggle()
    if dropDown.is(":visible")
      $('.category_page .filter .drop-down').not(dropDown).hide()
      $('.category_page .filter .button').not(button)
                                         .removeClass('pressed')

  $('.category_page .filter .drop-down.list .element').click ->
    option = $(this).closest('.filter')
                    .find("option[value='" + $(this).attr('value') + "']")
    if $(this).hasClass('selected')
      option.prop('selected', false)
      $(this).removeClass('selected')      
    else
      option.prop('selected', 'selected')
      $(this).addClass('selected')
    option.addFilterBox()
    $(this).closest('form').submit()

  $('.category_page .filter .hidden_fields select option:selected').each ->
    element = $(this).closest('.filter')
                     .find(".element[value='" + $(this).val() + "']")
    element.addClass('selected').addFilterBox()

  if ($('#min_price').length &&
      $('#max_price').length) &&
     ($('#min_price').val().length &&
      $('#max_price').val().length)
    $('#price_filter_slider').addFilterBox()

  if $('#search_in_category').length &&
     $('#search_in_category').val().length != 0
    $('#search_in_category').addFilterBox()

  if $('#mobile_search_in_category').length &&
     $('#mobile_search_in_category').val().length != 0
    $('#mobile_search_in_category').addFilterBox()

  $('#search_in_category').keyup ->
    $('#mobile_search_in_category').val("")
    $(this).addFilterBox()
    $(this).closest('form').submit()

  $('#mobile_search_in_category').keyup ->
    $('#search_in_category').val("")
    $(this).addFilterBox()
    $(this).closest('form').submit()

  $('.category_page .open_button').click ->
    openButton = $(this)
    menu = $(this).next()
    openButton.toggleClass('checked')
    menu.slideToggle ->
      if menu.css('display') == 'block'
        $('body').scrollTo(openButton, 500)


  $('.main-field').click ->
    if $('.dropdown-field').css('display') == 'block'
      $('.dropdown-field').hide()
      $('.materials_filters .dropdown-field').closest('form').submit()
    else
      $('.dropdown-field').show()

  $('.show_children').click ->
    $(this).hide().parent().find('.hide_children').show()
    $(this).parent().nextUntil(".parent").slideDown();

  $('.hide_children').click ->
    $(this).hide().parent().find('.show_children').show()
    $(this).parent().nextUntil(".parent").slideUp();

  $('.materials-select .all.material input').change ->
    if $(this).prop('checked')
      $(this).parent().nextAll(".material").find('input').prop("checked", true)
    else
      $(this).parent().nextAll(".material").find('input').prop("checked", false)

  $('.materials-select .parent input').change ->
    if $(this).prop('checked')
      $(this).parent().find('.show_children').hide()
             .parent().find('.hide_children').show()
      $(this).parent().nextUntil(".parent").slideDown().find('input').prop("checked", true)
    else
      $(this).parent().find('.hide_children').hide()
             .parent().find('.show_children').show()
      $(this).parent().nextUntil(".parent").slideUp().find('input').prop("checked", false)

  $('.materials-select .child input').change ->
    $(this).parent().prevAll('.parent').first()
           .find('input').prop('checked', true)


  $('.materials-select .child input:checked').each -> 
    $(this).parent().prevAll('.parent').first()
           .find('.show_children').hide()
           .parent().find('.hide_children').show()
           .parent().nextUntil(".parent").show()

  countOfAllMaterialsCheckboxes = $('.materials-select .parent input, .materials-select .child input').length
  if $('.materials-select .material input:checked').length == countOfAllMaterialsCheckboxes
    $('.materials-select .all.material input').prop('checked', true)
    $('.materials-select .main-field').text($('.materials-select .all.material').text())

  $('.materials-select .material input').change ->
    arrayOfMaterialsNames = $('.materials-select .parent input:checked,
                               .materials-select .child input:checked').next().map(->
      $(this).text()
    ).get()
    if arrayOfMaterialsNames.length == 1
      $('.materials-select .main-field').text(arrayOfMaterialsNames.join(''))
    else if arrayOfMaterialsNames.length == 2
      $('.materials-select .main-field').text(arrayOfMaterialsNames.join(', '))
    else if arrayOfMaterialsNames.length > 2 && arrayOfMaterialsNames.length < countOfAllMaterialsCheckboxes
      $('.materials-select .main-field').text(arrayOfMaterialsNames.slice(0, 2).join(', ') + ' ...')
    else if arrayOfMaterialsNames.length == 0
      $('.materials-select .main-field').text($('.materials_filters .materials-select .all.material').text())
    else
      $('.materials-select .main-field').text($('.materials-select .all.material').text())

  if $('.materials-select').length
    arrayOfMaterialsNames = $('.materials-select .parent input:checked,
                               .materials-select .child input:checked').next().map(->
      $(this).text()
    ).get()
    if arrayOfMaterialsNames.length == 1
      $('.materials-select .main-field').text(arrayOfMaterialsNames.join(''))
    else if arrayOfMaterialsNames.length == 2
      $('.materials-select .main-field').text(arrayOfMaterialsNames.join(', '))
    else if arrayOfMaterialsNames.length > 2 && arrayOfMaterialsNames.length < countOfAllMaterialsCheckboxes
      $('.materials-select .main-field').text(arrayOfMaterialsNames.slice(0, 2).join(', ') + ' ...')
    else if arrayOfMaterialsNames.length == 0
      $('.materials-select .main-field').text($('.materials_filters .materials-select .all.material').text())
    else
      $('.materials-select .main-field').text($('.materials-select .all.material').text())