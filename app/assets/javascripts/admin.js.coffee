$ ->
  $('.incoming_payments .glyphicon-pencil').click ->
    $(this).hide()
           .closest('.incoming_payments')
           .find('span')
           .hide()
           .closest('.incoming_payments')
           .find('.change_paid_on_form_wrapper')
           .css('display', 'inline-block')

  $('.admin.vouchers.change .edit.thin_brown_button').click ->
    $(this).hide()
    $(this).closest('.form_wrapper').find('.save.thin_brown_button').show()
    $(this).closest('.form_wrapper').find('.data_fields').show()

  $('.admin.vouchers.change .data_fields').each ->
    if $(this).find('.field_with_errors').length
      $(this).show()
      $(this).closest('.form_wrapper').find('.save.thin_brown_button').show()
      $(this).closest('.form_wrapper').find('.edit.thin_brown_button').hide()

  $('table.admin_art_associations td.cell_with_arrow').click ->
    $(this).find('.arrow_right').toggle()
    $(this).find('.arrow_down').toggle()
    $(this).closest('tr').next().toggle()

  if $('#faq_section_id').val() != ''
      $('#faq_section_id').css('color','black')
    else
      $('#faq_section_id').css('color','grey')

  if $('#faq_question_id').val() != ''
      $('#faq_question_id').css('color','black')
    else
      $('#faq_question_id').css('color','grey')

  $('#faq_section_id, #faq_question_id').change ->
    if $(this).val() != ''
      $(this).css('color','black')
    else
      $(this).css('color','grey')