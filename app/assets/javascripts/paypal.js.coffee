$ ->
  $('.payment_data .show_video_guide').click ->
    if $('.payment_data .paypal_video_guide').css('display') == "block"
      $('.paypal_video_guide video').get(0).pause()
    $('.payment_data .paypal_video_guide').slideToggle()

  $('.payment_data .show_pdf_guide').click ->
    $('.payment_data .paypal_pdf_guide').slideToggle()