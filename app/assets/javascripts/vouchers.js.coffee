$ -> 
  $(".new_voucher_form_wrapper .payment_types input[type=radio]").click ->
    if $(this).attr('id') == "voucher_payment_type_bank_transfer"
      $('.hidden_address_fields').slideDown()
    else
      $('.hidden_address_fields').slideUp()

  if $('#voucher_payment_type_bank_transfer').prop('checked')
    $('.hidden_address_fields').show()


  $('.admin.vouchers table td.open_more_info').click ->
    $(this).closest('tr').next('.more_info').toggle()
    $(this).find('.triangle_to_right').toggle()
    $(this).find('.triangle_to_down').toggle()

  $('.admin.vouchers table td.action .circle_arrows').click ->
    $(this).hide()
    $(this).closest('.action').find('.sent').hide()
    $(this).closest('.action').find('.voucher_action_wrapper').show()