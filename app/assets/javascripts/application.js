// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery.turbolinks
//= require jquery-ui
//= require jquery.ui.touch-punch
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery.multiple.select
//= require jquery.touchSwipe
//= require jquery.scrollTo
//= require jquery.mask
//= require cropper
//= require tinymce-jquery
//= require jquery.elevatezoom
//= require jquery.minicolors
//= require ckeditor/init
//= require select2
//= require spin
//= require jquery.spin
//= require bootstrap-select
//= require bxslider
//= require turbolinks
//= require_tree .

if (window.attachEvent) window.attachEvent("onload", sfHover);

var spinner_opts = {
      lines: 11 // The number of lines to draw
    , length: 17 // The length of each line
    , width: 14 // The line thickness
    , radius: 44 // The radius of the inner circle
    , scale: 0.5 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#000' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 0.9 // Rounds per second
    , trail: 100 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
}


$(document).on('ready page:load', function() {
    $('.bxslider').bxSlider({
        auto: true,
        pause: 5000
    });
    $('.search_form').submit(function() {
        $('body').spin();
        var form       = $(this);
        var action     = form.attr("action");
        var text_field = form.find('#search');
        var value      = text_field.val();
        Turbolinks.visit(action + value);
        return false;
    });

    $('[data-toggle="popover"]').popover()

    // select payment type
    $("#payment_type_selects select").change(function() {
        var $changedSelect = $(this)
        // remove the same option from other selects and slideDown next select
        if ($changedSelect.val() != "") {
            $('#payment_type_selects select').not($changedSelect).find("option").each(function() {
                if ($(this).val() == $changedSelect.val()) {
                    $(this).remove();
                };
            });
            $changedSelect.parent().next().slideDown();
        };

        // add option to other selects
        $("#payment_type_selects select").not($changedSelect).each(function() {
            var $anotherSelect = $(this);
            $changedSelect.find("option").not("option:first").not("option:selected").each(function() {
                $changedSelectOption = $(this);
                if ($anotherSelect.find("option[value=" + $changedSelectOption.val() + "]").length != true) {
                    $changedSelectOption.clone().removeAttr("selected").appendTo($anotherSelect);
                };
            });
        });
        if ($("#payment_type_selects option:selected:contains('Überweisung')").length == true) {
            $("#payment_type_selects option:selected:contains('Überweisung')").parent().parent().after($("#bank_transfer_data"))
            $("#bank_transfer_data").slideDown();
        } else {
            $("#bank_transfer_data").slideUp();
        };
        if ($("#payment_type_selects option:selected:contains('PayPal')").length == true) {
            $("#payment_type_selects option:selected:contains('PayPal')").parent().parent().after($("#paypal_data"))
            $("#paypal_data").slideDown();
        } else {
            $("#paypal_data").slideUp();
        };
    });

    // var mainContentHeightWhenLoad = $("#main-content").height() + "px"

    if ($("#article-form").length) {
        $("#main-content").height($("#article-form").height());
    }

    $(window).scroll(function() {
        if ($("#shop-form").css('display') == "block") {
            $("#main-content").height($("#shop-form").height());
        } else if ($("#article-form").css('display') == "block") {
            $("#main-content").height($("#article-form").height());
        } else {
            $("#main-content").css('height', '100%');
        };
    });

    if ($("#shop-form").css('display') == "block") {
        $("#main-content").height($("#shop-form").height());
    } else if ($("#article-form").css('display') == "block") {
        $("#main-content").height($("#article-form").height());
    } else {
        $("#main-content").css('height', '100%');
    };

    if (($("#shop-form #error_explanation").length) ||
        ((window.location.href.split("/").splice(3, 1) == "shops") &&
            (window.location.href.split("/").splice(5, 1) == "edit") &&
            (window.location.href.split("/").splice(3, 2).join("/") != "users/edit"))) {
        $("#shop-form").show().css("margin-top", "-10px");
        $("#main-content").css("min-height", "1000px");
    };


    if ($("#shop-form").length) {
        $('#shop-form #pictures_place .loading').before($("#hidden_pictures .picture"))
        $("#shop-form .edit_picture").remove()
        if ($("#shop-form #pictures_place .picture").length == 2) {
            $('#add_picture').hide();
        };
    };

    if ($("#article-form").length) {
        $('#article-form #pictures_place .loading').before($("#hidden_pictures .picture"))
        if ($("#article-form #pictures_place .picture").length == 10) {
            $('#add_picture').hide();
        };
    };

    if ( $("#article-form #error_explanation").length || window.location.href.split("/").splice(3, 1) + window.location.href.split("/").splice(5, 1) == "articlesedit" ) {
        $("#article-form").show();
        $("#main-content").css("min-height", "1000px");
        $('#article-pictures').append($("#hidden_pictures"));
    };

    $("#article_material_ids").multipleSelect();

    $("#article_colors, #article_gem_color_ids").multipleSelect();

    $("#article_suitable_for_ids").multipleSelect();


    $("#select-in-basket").click(function() {
        $(this).hide()
    });

    // adding pictures start

    $('#add_picture_button, #add_picture_text').click(function() {
        $("#picture_image").click();
    });

    // adding pictures finish

    $("#pictures_place").on("click", "[type=checkbox]", function() {
        $(this).click().click();
        $("#pictures_place [type=checkbox]").not(this).each(function() {
            if ($(this).prop("checked")) {
                $(this).click();
            };
        });
        if ($('.picture [type=checkbox]:checked').length == 0){
            $(this).prop('checked', true)
        };
    });

    if ($('.picture [type=checkbox]:checked').length == 0){
        $('.picture [type=checkbox]:first').prop('checked', true)
    };

    $(".show_AGB_green_button, #show_AGB_x").click(function(){
        $("#show_AGB, #show_AGB_x").toggle();
    });


    $("body").keydown(function(event) {
        if (event.which == 27 ) {
            $("#show_AGB, #show_AGB_x").hide();
        };
    });


    $(document).click(function(event) {

        if (!$(event.target).closest('.show_AGB_green_button').length) {
            if (!$(event.target).closest('#show_AGB').length){
                $('#show_AGB_x,#show_AGB').fadeOut();
            };
        };

        if (!$(event.target).closest('.popup_link').length) {
            $('.show_popup_window').fadeOut();
        };

        if (!$(event.target).closest('.materials-select').length) {
            if ($('.dropdown-field').css('display') == 'block'){
                $('.dropdown-field').hide()
                $('.materials_filters .dropdown-field').closest('form').submit();
            }
        };

        if ((!$(event.target).closest('.drop_left_menu').length)
            && (event.target.className != 'burger_menu_icon')) {
            $('.drop_left_menu').hide('slide')
        }

        if (!$(event.target).closest('.category_page .filter').length) {
            $('.category_page .filter .drop-down').hide();
            $('.category_page .filter .button').removeClass('pressed');
        };

        if (!$(event.target).closest('.header-profile-menu-wrapper').length) {
            $('.header-profile-menu').hide();
        };
    });

    function noticeHide() {
      $('#notice, #alert').fadeOut(50);
    }

    if ($('#notice, #alert').length && !$('#notice-area .cancel_button').length) {
      $('#notice, #alert')
        .css({'cursor': 'pointer'})
        .click(function() {
          noticeHide();
        });
      setTimeout(noticeHide, 10000);
    }

    $("#notice-area .cancel_button").click(function() {
       noticeHide();
    });

    $('#select2-drop-mask').css("position", "static")

    // preview image before upload
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#show_preview_image').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        };
    };
    $("#preview_image").change(function(){
        readURL(this);
        $("#show_preview_image").show();
    });

    $("#article_price, #article_shipping_methods_attributes_0_price").keyup(function(event) {
        if ($(this).val().split(".").length != 1) {
            if ($(this).val().split(".")[1].length > 2 ) {
                $(this).val(parseFloat($(this).val()).toFixed(2).toString().replace(".", ","));
            };
        };
        if ($(this).val().split(",").length != 1) {
            if ($(this).val().split(",")[1].length > 2 ) {
                $(this).val(parseFloat($(this).val()).toFixed(2).toString().replace(".", ","));
            };
        };
    });

    $('#address_show_to_public').change(function() {
        if($(this).is(":checked")) {
            var $confirmBox = confirm("Are you sure?");
            if ($confirmBox != true) {
                $(this).prop("checked", false);
            };
        };
    });

    // copy value from shop_name to subdomain
    $("#shop_name").keyup(function(){
        var re = /[^\W_]|[\-]/;
        var thisval = []
        shopnamearray = $(this).val().toLowerCase().split('')
        for (i = 0; i < shopnamearray.length; i++) {
            if (shopnamearray[i] == " ") {
                thisval.push('-')
            };
            if (shopnamearray[i] == "ä") {
                thisval.push('ae')
            };
            if (shopnamearray[i] == "ö") {
                thisval.push('oe')
            };
            if (shopnamearray[i] == "ü") {
                thisval.push('ue')
            };
            if (re.test(shopnamearray[i])) {
                thisval.push(shopnamearray[i])
            };
        };
        thisval = thisval.join('');
        $("#shop_subdomain").val(thisval);
    });

    $("#wire input").change(function(){
        if ($(this).prop("checked")) {
            $(this).parent().after($("#bank_transfer_data"));
            $("#bank_transfer_data").slideDown();
        } else {
            $("#bank_transfer_data").slideUp();
        };
    });
    if ($("#wire input").prop("checked")){
        $("#wire input").parent().after($("#bank_transfer_data"));
        $("#bank_transfer_data").show();
    };

    $("#paypal input").change(function(){
        // if ($(this).prop("checked")) {
        $(this).parent().after($("#paypal_data"));
        $("#paypal_data").slideToggle();
        // } else if ($("#credit_card input").prop("checked") == false){
        //     $("#paypal_data").slideUp();
        // };
    });

    if ($("#paypal input").prop("checked")){
        $("#paypal input").parent().after($("#paypal_data"));
        $("#paypal_data").show();
    };

    $(".glyphicon-info-sign").hover(function(){
        $(this).find(".bubble").fadeIn();
    }, function() {
        $(this).find(".bubble").fadeOut();
    });

    var freeWithinGermanyInputs = $("#free_within_germany input:text, #free_within_germany input:checkbox")
    var freeWithinGermanyLabels = $("#free_within_germany label")
    var withinGermanyInputs = $("#within_germany input:text, #within_germany input:checkbox")
    var withinGermanyLabels = $("#within_germany label")
    freeWithinGermanyInputs.change(function(){
        if (freeWithinGermanyInputs.is(":checked")) {
            withinGermanyInputs.attr('disabled', true);
            withinGermanyLabels.css("color", "rgba(128, 128, 128, 0.53)")

        } else {
            withinGermanyInputs.attr('disabled', false);
            withinGermanyLabels.css("color", "#333")
        };
    });
    withinGermanyInputs.change(function(){
        if (withinGermanyInputs.is(":checked")) {
            freeWithinGermanyInputs.attr('disabled', true);
            freeWithinGermanyLabels.css("color", "rgba(128, 128, 128, 0.53)")
        } else {
            freeWithinGermanyInputs.attr('disabled', false);
            freeWithinGermanyLabels.css("color", "#333")
        };
    });

    if (freeWithinGermanyInputs.is(":checked")) {
        withinGermanyInputs.attr('disabled', true);
        withinGermanyLabels.css("color", "rgba(128, 128, 128, 0.53)")
    };
    if (withinGermanyInputs.is(":checked")) {
        freeWithinGermanyInputs.attr('disabled', true);
        freeWithinGermanyLabels.css("color", "rgba(128, 128, 128, 0.53)")
    };

    $(".file_button").click(function(){
        $('#message_file').click();
    });

    var $fileButtonTextBefore = $(".file_button").text()
    var $fileButtonTextAfter = $(".file_button_text").text()
    $("#message_file").change(function(){
        if ($("#message_file").val() != "") {
            $(".file_button").text($fileButtonTextAfter)
        } else {
            $(".file_button").text($fileButtonTextBefore)
        };
    });

    $(".select_all").click(function(){
        $(".message_line_icons input").prop("checked", true)
    })

    $(".message_line").click(function(event){
        if ($(event.target).closest('.message_line_header').length && ! $(event.target).closest('.message_line_icons').length) {

            var that = $(this);

            if (that.hasClass('message_line_active')) {
              that.removeClass('message_line_active');
              that.find('.message_line_body').slideUp();
            } else {
              that.addClass('message_line_active');
              that.find('.message_line_body').slideDown();
            }

            $('.message_line_active').each(function() {
              if (! that.is($(this))) {
                $(this).removeClass('message_line_active');
                $(this).find('.message_line_body').slideUp();
              }
            });
        };
    });

    $('.rating_star').click(function(){
        $(this).css('color', 'gold');
        $(this).prevAll().css('color', 'gold');
        $(this).nextAll().css('color', 'gray');
        $(this).closest('.review_block').find("#articles_order_rating, #rating_score").val($(this).attr('id').slice(-1)).submit();
    });

    $(".edit_articles_order #articles_order_rating, #rating_score").each(function() {
        if ($(this).val()) {
            var $star = $(this).last().parent().parent().prev().prev().find('div[id*="' + $(this).val() + '"]')
            $star
            $star.css('color', 'gold');
            $star.prevAll().css('color', 'gold');
            $star.nextAll().css('color', 'gray');
        };
    });

    $(".edit_rating #rating_text").change(function(){
        $(this).submit();
    });

    if (!$('#user_settings_show_address').length) {
        $("#user_settings_edit_address").parent().show();
    };

    if ($('#user_settings_form_address .field_with_errors').length) {
        $('#user_settings_show_address').hide();
        $("#user_settings_edit_address").parent().show();
    }

    if ($('#user_settings_edit_email_and_password .field_with_errors').length) {
        $('#user_settings_show_email_and_password').hide();
        $("#user_settings_edit_email_and_password").parent().show();
    }

    $('.replaceable_button').click(function(){
        $(this).parent().hide();
        $(this).parent().next().show();
    });

    if ($(".about_me_shops").length) {
        $('.about_me_shops #pictures_place .loading').before($("#hidden_pictures .picture"))
        $("#shop-form .edit_picture").remove()
        if ($(".about_me_shops #pictures_place .picture").length == 2) {
            $('#add_picture').hide();
        };
    };

    $('#show_preview_image').load(function(){
        $('#about_me_shops_placeholder_image').attr("src", $('#show_preview_image').attr("src"));
        $('#add_image_button, #add_image_text').hide();
        $('#about_me_shops_placeholder_image').show();
        $('#about_me_shops_main_image').hide();
        $('.destroy_shop_image_shops').hide();
        $("#delete_main_image").val("false");
    });

    $('#preview_image:file').change(function (){
        if ($(this).val() == "") {
            $('#about_me_shops_placeholder_image').hide();
            if ($('#about_me_shops_main_image').length) {
                $('#about_me_shops_main_image').show();
                $('.destroy_shop_image_shops').show();
            } else {
                $('#add_image_button, #add_image_text').show();
            };
        };
    });

    $('#add_image_button, #add_image_text, #about_me_shops_placeholder_image, #about_me_shops_main_image').click(function() {
        $("#preview_image").click();
    });

    $('.destroy_shop_image_shops').click(function(){
        $('.destroy_shop_image_shops, #about_me_shops_main_image').hide();
        $('#add_image_button, #add_image_text').show();
        $("#delete_main_image").val("true");
    });

    if ($("#settings_shops_personal_bank_data .field_with_errors").length) {
        $("#settings_shops_personal_bank_data").parent().show().prev().hide();
    };

    if ($("#settings_shops_payment_types .field_with_errors").length) {
        $("#settings_shops_payment_types").parent().show().prev().hide();
    };

    if ($(".shop_edit #shipping-methods .field_with_errors").length) {
        $("#shipping-methods").parent().parent().show().prev().hide();
    };

    $("#copy_from_personal_bank_data").click(function(){
        if ($("#copy_from_personal_bank_data").prop("checked")) {
            $("#shop_bank_transfer_data_attributes_bank_name").val($("#shop_personal_bank_data_attributes_bank_name").val());
            $("#shop_bank_transfer_data_attributes_account_holder").val($("#shop_personal_bank_data_attributes_account_holder").val());
            $("#shop_bank_transfer_data_attributes_account_number").val($("#shop_personal_bank_data_attributes_account_number").val());
            $("#shop_bank_transfer_data_attributes_bank_code").val($("#shop_personal_bank_data_attributes_bank_code").val());
            $("#shop_bank_transfer_data_attributes_iban").val($("#shop_personal_bank_data_attributes_iban").val());
            $("#shop_bank_transfer_data_attributes_swft_bic").val($("#shop_personal_bank_data_attributes_swft_bic").val());

            $("#article_bank_transfer_data_attributes_bank_name").val($("#shop_personal_bank_data_attributes_bank_name").val());
            $("#article_bank_transfer_data_attributes_account_holder").val($("#shop_personal_bank_data_attributes_account_holder").val());
            $("#article_bank_transfer_data_attributes_account_number").val($("#shop_personal_bank_data_attributes_account_number").val());
            $("#article_bank_transfer_data_attributes_bank_code").val($("#shop_personal_bank_data_attributes_bank_code").val());
            $("#article_bank_transfer_data_attributes_iban").val($("#shop_personal_bank_data_attributes_iban").val());
            $("#article_bank_transfer_data_attributes_swft_bic").val($("#shop_personal_bank_data_attributes_swft_bic").val());
        };
    });

    if ($('#article_suitable_for_ids').length){
        $('#article_suitable_for_ids').next().find(".ms-select-all").show();
        $('#article_suitable_for_ids').next().find("li:not(.ms-select-all)").css('margin-left', '5px')
    }

    $('.terms').click(function(){
        $('.terms_content').slideDown();
        $('.imprint_content').slideUp();
        $('.revocation_content').slideUp();
    })
    $('.imprint').click(function(){
        $('.terms_content').slideUp();
        $('.imprint_content').slideDown();
        $('.revocation_content').slideUp();
    })
    $('.revocation').click(function(){
        $('.terms_content').slideUp();
        $('.imprint_content').slideUp();
        $('.revocation_content').slideDown();
    })

    $('.info_text_header').click(function() {
        $(this).parent().find('.info_text_content').slideToggle();
    });
    $('.statistics_mobile_header').click(function() {
        $(this).parent().find('.statistics_mobile_content').slideToggle();
    });

    $('.invoices .search_field .glyphicon.glyphicon-search').click(function(){
        $(this).parent().submit();
    })

    $('.newsletter #template_id').select2({
        theme: "bootstrap",
        minimumResultsForSearch: Infinity
    });

    $('.select2-selection').click(function(){
        $('.material_child').each(function(){
            $('.select2-results__options li:contains('+ $(this).text() +')').css('margin-left', "10px")
            $('.select2-results__options li:last-child').css('margin-left', "0px")
        });
    })

    $("#newsletter_template").click(function(){
        $(".template_options").slideToggle();
        $(".delivery_date_check").slideToggle();
        $("#newsletter_delivery_date_checked").prop("checked", false)
        $(".delivery_date").hide();
    })
    if ($("#newsletter_template").prop("checked")) {
        $(".template_options").show();
        $(".delivery_date_check").hide();
    }

    $("#newsletter_delivery_date_checked").click(function(){
        $(".delivery_date").slideToggle();
    })
    if ($("#newsletter_delivery_date_checked").prop("checked")) {
        $(".delivery_date").show();
    }

    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        changeYear: true,
        yearRange: "-100:+0",
        minDate: "-100Y",
        maxDate: "+0D"
    });

    $('.change_paid_on_field').datepicker({
        dateFormat: "dd.mm.yy",
        changeYear: true,
        yearRange: "-20:+2"
    });


    if (CKEDITOR.instances['newsletter_text']) {
        CKEDITOR.remove(CKEDITOR.instances['newsletter_text']);
    }

    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        if ($('.user_logged_in_head .arrow').length) {
            $('.user_logged_in_head .arrow').css('margin-top', '0px')
        }
    }

    if (!navigator.cookieEnabled) {
        alert($('.cookies_alert').text())
    }

    $('.about_me_shops_pictures, #shop-form #pictures_place').find('[type=checkbox]').remove()

    $('#search-bar .glyphicon-search').click(function(){
        $(this).closest('form').submit()
    });

    $('.burger_menu_icon').click(function(){
        $('.drop_left_menu').show('slide')
    })

    $(".drop_left_menu").swipe( {
        swipeLeft:function() {
            $(".drop_left_menu").hide('slide')
        },
    });

    $(".drop_left_menu .category").click(function(){

        subcategories = $('.subcategories').find('.' + $(this).attr('id')).closest('.subcategories')

        $('.drop_left_menu .categories').css('position', 'absolute').hide('slide', {direction: 'left'})

        subcategories.css('position', 'relative').show('slide', {direction: 'right'}, function(){
            $('body').scrollTo(".second_part", 500)
        })
    })

    $(".drop_left_menu .all_categories").click(function(){
        $(this).closest('.subcategories').css('position', 'absolute').hide('slide', {direction: 'right'})
        $('.drop_left_menu .categories').css('position', 'relative').show('slide', {direction: 'left'}, function(){
            $('body').scrollTo(".second_part", 500)
        })
    })

    $('.drop_left_menu .subcategory').click(function(){
        if ($(this).find('a').hasClass('with_children')) {
            all_subcategories = $('.drop_left_menu .subcategory').not($(this))
            all_subcategories.find('.arrow_to_bottom').show()
            all_subcategories.find('.arrow_to_top').hide()
            all_subcategories.find('.with_children').removeClass('clickable').addClass('nonclickable')
            all_subcategories.find('> a').css('border-bottom', '1px solid white')
            all_subcategories.next('.subsubcategories').slideUp()
            all_subcategories.next('.subsubcategories').next('.subcategory').find('> a').css('border-top', 'none')

            $(this).find('.arrow_to_bottom').hide()
            $(this).find('.arrow_to_top').show()
            $(this).find('a').removeClass('nonclickable').addClass('clickable')
            $(this).find('> a').css('border-bottom', 'none')

            $(this).next('.subsubcategories').next('.subcategory').find('> a').css('border-top', '1px solid white')
            $(this).next('.subsubcategories').slideDown(function(){
                $('body').scrollTo($(this).prev('.subcategory'), 500)
            })
        }
    })

    $('.main_head .more_categories').click(function(){
        $(this).hide()
        $(this).prevAll('.subsubcategory').show()
    })

    $('.mobile_pictures_slider .pictures.owl-carousel').owlCarousel({
        loop:true,
        items:1
    });

    $('.header-profile-menu-button').hover(function() {
        headerProfileMenu = $(this).closest('.header-profile-menu-wrapper').find('.header-profile-menu')
        headerProfileMenu.show()
    })

    $('.header-profile-menu-button').click(function() {
        headerProfileMenu = $(this).closest('.header-profile-menu-wrapper').find('.header-profile-menu')
        headerProfileMenu.show()
    })

    $('.header-profile-menu .close_menu').click(function() {
        $(this).closest('.header-profile-menu').hide()
    })

});
