$ ->
  $("#picture_image").change ->
    $("#pictures_place .loading").fadeIn()
    $(this).closest('form').submit()

  $("#main_picture_image").change ->
    $(this).closest('form').submit()


  $("#add_main_picture_button").click ->
    $("#main_picture_image").click()
  $("#main_picture_image").change ->
    $('#add_main_picture_button').hide()
    $('.main_picture_block .loading').fadeIn()

  if $('.main_picture_block .main_picture').length
    $('#add_main_picture_button').hide()