# Addresses and payment methods page

clickChangeAddressButton = ->
  $("#purchase-address-and-payment-method
     .change-address").click ->
    td = $(this).closest('td')
    td.find('.change-address').slideUp()
    td.find('.purchase-address').slideDown()
    td.find('.address-block').slideUp()

$ ->
  clickChangeAddressButton()

  $('.purchases').find('.show_more_options,
                        .hide_more_options').click ->
    thisPurchaseItem = $(this).closest('.purchase-item')
    if $(this).attr('class') == 'show_more_options'
      thisPurchaseItem.find('.mark_order_messages_as_read').click().remove()
    thisPurchaseItem.find('.more_options,
                           .hide_more_options,
                           .show_more_options').slideToggle()