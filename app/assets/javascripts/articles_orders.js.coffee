$ ->
  $(".purchases .update-status-form-wrapper select").change ->
    if $(this).val() == 'finished'
      $(this).closest('.purchase-item')
             .find('.close_order')
             .click()
    else
      if $(this).val() == 'review'
        showMoreOptions = 
          $(this).closest('.purchase-item')
                 .find('.show_more_options')
        if showMoreOptions.css('display') == 'block'
          showMoreOptions.click()
        $('html, body').animate {
          scrollTop:
            $(this).closest('.purchase-item')
                   .find('.review')
                   .offset()
                   .top - ($(window).height() / 2) + 100
        }, 2500
      else
        confirmationQuestion = 
          $(this).closest(".purchases")
                 .find(".confirmations_for_statuses #" + $(this).val())
                 .text()
        if confirm(confirmationQuestion)
          this.form.submit()
    this.selectedIndex = 0

  if $(".print").length
    window.print()
    window.onfocus = ->
      window.close()