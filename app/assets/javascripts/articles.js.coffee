$ ->
  $('.categories_list li').click ->
    $(this).toggleClass('background_light_green')
    
    if $(this).hasClass('background_light_green')
      $(this).find('input[type=checkbox]').prop('checked', true)
    else
      $(this).find('input[type=checkbox]').prop('checked', false)

    if !$(this).closest('#multiple_choices').length
      $(this).parent().find("li").not($(this))
             .removeClass('background_light_green')


    $(this).find('.white_triangle').toggleClass('light_green_triangle')

    $(this).parent().find('.white_triangle')
           .not($(this).find('.white_triangle'))
           .removeClass('light_green_triangle')

    if $(this).parent().attr("class") != 'sub categories_list' && $(this).parent().attr("class") != 'subsub categories_list'
      $(this).closest(".first_categories_list, .second_categories_list, .third_categories_list")
             .find('.sub.categories_list').hide()
             .find("li").removeClass('background_light_green')
             .find('.white_triangle')
             .removeClass('light_green_triangle')
             .closest('li')
             .find('input[type=checkbox]').prop('checked', false)
             

    if $(this).parent().attr("class") != 'subsub categories_list'
      $(this).closest(".first_categories_list, .second_categories_list, .third_categories_list")
             .find('.subsub.categories_list').hide()
             .find("li").removeClass('background_light_green')
             .find('.white_triangle')
             .removeClass('light_green_triangle')
             .closest('li')
             .find('input[type=checkbox]').prop('checked', false)

    $("#article_category_ids option").prop('selected', false)
    $(".first_categories_list, .second_categories_list, .third_categories_list").find('li.background_light_green').each ->
      $("#article_category_ids
          option[value=" + $(this).val() + "]").prop('selected','selected')

    $(this).parent().nextAll('.big_perpendicular_angle_bracket_to_right').css("display", "none")
    if $(this).parent().find('.background_light_green').length &&
       $(this).find('.small_perpendicular_angle_bracket_to_right').length
      $(this).parent()
             .nextAll('.big_perpendicular_angle_bracket_to_right')
             .first()
             .css("display", "inline-block")
      $(this).closest(".first_categories_list,
                       .second_categories_list,
                       .third_categories_list")
             .find('.categories_list[value=' + $(this).val() + ']')
             .css("display", "inline-block")
  
    $(".categories_list li").show()
    $(".categories_list li.background_light_green").each ->
      $(".categories_list li[value=" + $(this).val() + "]").not($(this))
        .not($(".categories_list li")
        .find(".small_perpendicular_angle_bracket_to_right")
        .parent()).hide()
  
  $(".show_next_categories_list").click ->
    $(this).hide()
           .next().show()
           .nextAll('.category_list_separator_line').first().show()
           .next('.show_next_categories_list').show()


  selected_options = $("#article_category_ids option:selected")
  $.each [
    '.first'
    '.second'
    '.third'
  ], (index, value) ->
    # hide .show_next_categories_list
    if selected_options.length
      $(value + "_categories_list").show()
        .prev('.show_next_categories_list').hide()

    $(value + "_categories_list .subsub.categories_list li").each ->
      selected_option = selected_options.filter("#article_category_ids option[value=" + $(this).val() + "]")
      if selected_option.length
        selected_options = selected_options.not(selected_option)
        $(this).addClass('background_light_green')
        $(this).parent().css("display", "inline-block")
                        .prevAll('.big_perpendicular_angle_bracket_to_right')
                        .css("display", "inline-block")
        
        parentCategoryId = $(this).parent().attr('value')
        parentCategory = $(value + '_categories_list .sub.categories_list li[value=' + parentCategoryId + ']')
        selected_options = selected_options.not($("#article_category_ids option[value=" + parentCategoryId + "]"))
        parentCategory.addClass('background_light_green')
                      .find('.white_triangle')
                      .addClass('light_green_triangle')
        parentCategory.parent().css("display", "inline-block")
          
        parentOfParentCategoryId = parentCategory.parent().attr('value')
        parentOfParentCategory = $(value + '_categories_list .main.categories_list li[value=' + parentOfParentCategoryId + ']')
        selected_options = selected_options.not($("#article_category_ids option[value=" + parentOfParentCategoryId + "]"))
        parentOfParentCategory.addClass('background_light_green')
                              .find('.white_triangle')
                              .addClass('light_green_triangle')
        return false

    $(value + "_categories_list .sub.categories_list li").each ->
      selected_option = selected_options.filter("#article_category_ids option[value=" + $(this).val() + "]")
      if (selected_option.length)
        if $(this).closest('#multiple_choices').length

          parentCategoryId = $(this).parent().attr('value')
          parentCategory = $(value + '_categories_list .main.categories_list li[value=' + parentCategoryId + ']')

          if parentCategory.closest('ul').find('.background_light_green').length
            return false

          selected_options = selected_options.not(selected_option)
          $(this).addClass('background_light_green')
                 .find('.white_triangle')
                 .addClass('light_green_triangle')

          if $(this).find('.white_triangle').length
            $(this).parent().nextAll('.big_perpendicular_angle_bracket_to_right').css('display', 'inline-block')
            $(value + '_categories_list .subsub.categories_list[value=' + $(this).val() + ']').css('display', 'inline-block')

          $(this).parent().css("display", "inline-block")
                          .prevAll('.big_perpendicular_angle_bracket_to_right')
                          .css("display", "inline-block")

          lastElement = true
          $(this).closest('ul').find('li').each ->
            if selected_options.filter("option[value=" + $(this).val() + "]").length
              lastElement = false
              return false
          if lastElement
            selected_options = selected_options.not($("#article_category_ids option[value=" + parentCategoryId + "]"))
            parentCategory.addClass('background_light_green')
                          .find('.white_triangle')
                          .addClass('light_green_triangle')
            parentCategory.parent().css("display", "inline-block")
            return false
        else if !$(value + "_categories_list .sub.categories_list li.background_light_green").length
          selected_options = selected_options.not(selected_option)
          $(this).addClass('background_light_green')
                 .find('.white_triangle')
                 .addClass('light_green_triangle')

          if $(this).find('.white_triangle').length
            $(this).parent().nextAll('.big_perpendicular_angle_bracket_to_right').css('display', 'inline-block')
            $(value + '_categories_list .subsub.categories_list[value=' + $(this).val() + ']').css('display', 'inline-block')

          $(this).parent().css("display", "inline-block")
                          .prevAll('.big_perpendicular_angle_bracket_to_right')
                          .css("display", "inline-block")
          
          parentCategoryId = $(this).parent().attr('value')
          parentCategory = $(value + '_categories_list .main.categories_list li[value=' + parentCategoryId + ']')
          selected_options = selected_options.not($("#article_category_ids option[value=" + parentCategoryId + "]"))
          parentCategory.addClass('background_light_green')
                        .find('.white_triangle')
                        .addClass('light_green_triangle')
          parentCategory.parent().css("display", "inline-block")
          return false


    $(value + "_categories_list .main.categories_list li").each ->
      selected_option = selected_options.filter("#article_category_ids option[value=" + $(this).val() + "]")
      if (selected_option.length) && !$(value + "_categories_list .main.categories_list li.background_light_green").length
        selected_options = selected_options.not(selected_option)
        $(this).addClass('background_light_green')
               .find('.white_triangle')
               .addClass('light_green_triangle')

        if $(this).find('.white_triangle').length
          $(this).parent().nextAll('.big_perpendicular_angle_bracket_to_right').first().css('display', 'inline-block')
          $(value + '_categories_list .sub.categories_list[value=' + $(this).val() + ']').css('display', 'inline-block')

        $(this).parent().css("display", "inline-block")
                        .prevAll('.big_perpendicular_angle_bracket_to_right')
                        .css("display", "inline-block")
        return false
    if $(value + "_categories_list").is(":visible")
      $(value + "_categories_list")
        .next('.category_list_separator_line').show()
        .next('.show_next_categories_list').show()


  $(".categories_list li.background_light_green").each ->
    $(this).find('input[type=checkbox]').prop('checked', true)
    $(".categories_list li[value=" + $(this).val() + "]").not($(this))
      .not($(".categories_list li")
      .find(".small_perpendicular_angle_bracket_to_right")
      .parent()).hide()

  changePercents = () ->
    sumOfPercents = 0
    $(".material_textiles_block_form input[type=text]").each ->
      if !isNaN(parseInt($(this).val()))
        sumOfPercents += parseInt $(this).val()
    $overallPercent = $(".material_textiles_block_form .overall_percent")
    $overallPercent.text(sumOfPercents + "%")
    if sumOfPercents > 100
      $overallPercent.css("color", "#bf0018")
    else
      $overallPercent.css("color", "#333")
  
  $(".material_textiles_block_form input[type=text]").keyup ->
    changePercents()
  if $(".material_textiles_block_form input[type=text]").length
    changePercents()

  $('#article_material_ids,
     #article_colors,
     #article_gem_color_ids,
     #article_suitable_for_ids').change ->
    if $(this).find('option:selected').length == $(this).find('option').length
      $(this).next().find(".ms-choice span").text($("#all-label").text())
    else
      arrayOfSelectedValues = $(this).find('option:selected').map( -> $(this).text()).get()
      if arrayOfSelectedValues.length > 2
        $(this).next().find(".ms-choice span").text(arrayOfSelectedValues.slice(0, 2).join(", ") + " ...")
      else
        $(this).next().find(".ms-choice span").text(arrayOfSelectedValues.join(", "))

  $('#article_customizable').change ->
    $(".customizable_comment").slideToggle()
  if $('#article_customizable').prop("checked")
    $(".customizable_comment").slideDown()


  if $('#article_save_as_template').prop('checked')
    $('#article_template_name').show()


  $('#article_save_as_template').change ->
    if $(this).prop('checked')
      $('#article_template_name').slideDown()
    else
      $('#article_template_name').slideUp()

  $('#article_artist').click ->
    if !$(this).val().length
      $(this).val($(this).attr('placeholder'))

  $(".article_show .desktop_pictures img:not(.no_image)").click ->
    mainPicture = $(".article_show .main_picture").attr('class', "usual_picture")
    usualPicture = $(this).after(mainPicture)
    $(this).attr('class', "main_picture")
           .appendTo($(".article_show .main_wrapper"))
    $('.zoomContainer').remove()
    $('.article_show .main_picture').elevateZoom()
    mainPicture.show()
  $('.article_show .main_picture').elevateZoom();

  $('.article_show .usual_pictures img:lt(5)').show()

  if $('.article_show .usual_picture').length <= 5
    $('.article_show .triangle_right span').css("border-left", "50px solid #dcdcdc")

  $('.article_show .triangle_left').click ->
    if $('.usual_picture:visible').length >= 5 &&
       $('.usual_picture:visible:first').prev('.usual_picture').length
      $('.usual_picture:visible:last').hide()
      $('.usual_picture:visible:first').prev('.usual_picture').show()
      $('.article_show .triangle_right span').css("border-left", "50px solid #5383a0")
      if !$('.usual_picture:visible:first').prev('.usual_picture').length
        $('.article_show .triangle_left span').css("border-right", "50px solid #dcdcdc")
      

  $('.article_show .triangle_right').click ->
    if $('.usual_picture:visible').length >= 5 &&
       $('.usual_picture:visible:last').next('.usual_picture').length
      $('.usual_picture:visible:first').hide()
      $('.usual_picture:visible:last').next('.usual_picture').show()
      $('.article_show .triangle_left span').css("border-right", "50px solid #5383a0")
      if !$('.usual_picture:visible:last').next('.usual_picture').length
        $('.article_show .triangle_right span').css("border-left", "50px solid #dcdcdc")


  setTimeout (->
    $('#article_material_ids,
       #article_colors,
       #article_gem_color_ids,
       #article_suitable_for_ids').each ->
      if $(this).find('option:selected').length == $(this).find('option').length
        $(this).next().find(".ms-choice span").text($("#all-label").text())
      else
        arrayOfSelectedValues = $(this).find('option:selected').map( -> $(this).text()).get()
        if arrayOfSelectedValues.length > 2
          $(this).next().find(".ms-choice span").text(arrayOfSelectedValues.slice(0, 2).join(", ") + " ...")
        else
          $(this).next().find(".ms-choice span").text(arrayOfSelectedValues.join(", "))
  ), 1

  $('#article-form #template').change ->
    $(this).closest('form').submit()
    $('body').spin()

  $('#article_made_in').selectpicker
    width: 160
    noneSelectedText: ''
    size: 10

  $('#article_year').select2
    tags: true,
    theme: "bootstrap"

  $('.shop_head .show_info_button').click ->
    $('.shop_mini_data').slideToggle()
    $('.shop_head .arrow_to_bottom').toggle()
    $('.shop_head .arrow_to_top').toggle()


  $('.article_show .share_buttons a:not(.share_by_email)').click ->
    w = 580
    h = 470

    dualScreenLeft = if window.screenLeft != undefined then window.screenLeft else screen.left
    dualScreenTop = if window.screenTop != undefined then window.screenTop else screen.top
    width = if window.innerWidth then window.innerWidth else if document.documentElement.clientWidth then document.documentElement.clientWidth else screen.width
    height = if window.innerHeight then window.innerHeight else if document.documentElement.clientHeight then document.documentElement.clientHeight else screen.height
    left = width / 2 - (w / 2) + dualScreenLeft
    top = height / 3 - (h / 3) + dualScreenTop

    newWindow = window.open($(this).attr('href'), 'share', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left)

    if newWindow && newWindow.focus
      newWindow.focus()