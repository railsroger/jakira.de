$ ->
  $("#withdrawal_form_file").change ->
    $('#add_file_button').hide()
    $(".withdrawal_form_file_place .loading").fadeIn()
    $("#new_withdrawal_form").trigger('submit.rails')

  $('#add_file_button').click ->
    $('#withdrawal_form_file').click()