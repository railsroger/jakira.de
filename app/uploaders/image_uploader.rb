class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  # storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end
  # version :original, if: :is_small_width?
  process resize_to_limit: [1200, 1200]
  process :crop

  def fix_exif_rotation
    img = MiniMagick::Image.new(model.image.path)
    img.auto_orient
  end

  process :fix_exif_rotation

  # Create different versions of your uploaded files:

  version :for_mobile do
    process resize_to_fill: [500, 400]
  end

  version :middle do 
    process resize_to_fill: [240, 180]
  end

  version :small do
    process resize_to_fit: [nil, 110]
  end

  version :purchase_thumb do
    process resize_to_fill: [155, 115]
  end

  def crop
    if model.class.name != 'Shop' && model.cropping?
      img = MiniMagick::Image.new(model.image.path) do |b|
        b.rotate model.rotate
        b.crop "#{model.width.to_i}x#{model.height.to_i}+#{model.x.to_i}+#{model.y.to_i}+"
      end
    end
  end


  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
  private

  # def is_small_width? picture
  #   image = MiniMagick::Image.open(picture.path)
  #   if image[:width] > 2200 && model.new_record?
  #     raise CarrierWave::IntegrityError, I18n.t(:"errors.messages.big_width_error")
  #   end
  # end

end
